package dialogs;

/*
 * Copyright 2000-2016 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import application.settings.AppSettingsConfigurable;
import application.settings.AppSettingsState;
import com.intellij.CommonBundle;
import com.intellij.codeInsight.CodeInsightBundle;
import com.intellij.ide.util.PropertiesComponent;
import com.intellij.java.JavaBundle;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.CustomShortcutSet;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.editor.event.DocumentEvent;
import com.intellij.openapi.editor.event.DocumentListener;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.options.ShowSettingsUtil;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ModuleRootManager;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.util.Comparing;
import com.intellij.openapi.vfs.VfsUtilCore;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiMember;
import com.intellij.psi.PsiNameHelper;
import com.intellij.psi.PsiPackage;
import com.intellij.psi.codeStyle.JavaCodeStyleSettings;
import com.intellij.refactoring.PackageWrapper;
import com.intellij.refactoring.ui.MemberSelectionTable;
import com.intellij.refactoring.ui.PackageNameReferenceEditorCombo;
import com.intellij.refactoring.util.RefactoringMessageUtil;
import com.intellij.refactoring.util.RefactoringUtil;
import com.intellij.refactoring.util.classMembers.MemberInfo;
import com.intellij.testIntegration.TestFramework;
import com.intellij.testIntegration.TestIntegrationUtils;
import com.intellij.ui.EditorTextField;
import com.intellij.ui.JBColor;
import com.intellij.ui.RecentsManager;
import com.intellij.ui.ReferenceEditorComboWithBrowseButton;
import com.intellij.ui.ScrollPaneFactory;
import com.intellij.util.IncorrectOperationException;
import com.intellij.util.SmartList;
import com.intellij.util.ui.JBUI;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.jps.model.java.JavaSourceRootType;
import util.IDEAUtil;
import util.SettingsUtil;

public class CreateTestsDialog extends DialogWrapper {

  private static final String RECENTS_KEY = "CreateTestDialog.RecentsKey";
  private static final String RECENT_SUPERS_KEY = "CreateTestDialog.Recents.Supers";
  private static final String DEFAULT_LIBRARY_NAME_PROPERTY =
      CreateTestsDialog.class.getName() + ".defaultLibrary";
  private static final String DEFAULT_LIBRARY_SUPERCLASS_NAME_PROPERTY =
      CreateTestsDialog.class.getName() + ".defaultLibrarySuperClass";
  private static final String SHOW_INHERITED_MEMBERS_PROPERTY =
      CreateTestsDialog.class.getName() + ".includeInheritedMembers";

  private final Project myProject;
  private final PsiClass myTargetClass;
  private final PsiPackage myTargetPackage;
  private final Module myTargetModule;

  protected PsiDirectory myTargetDirectory;

  private EditorTextField myTargetClassNameField;
  private ReferenceEditorComboWithBrowseButton myTargetPackageField;
  private final JCheckBox myGenerateBeforeBox = new JCheckBox(
      JavaBundle.message("intention.create.test.dialog.setUp"));
  private final JCheckBox myGenerateAfterBox = new JCheckBox(
      JavaBundle.message("intention.create.test.dialog.tearDown"));
  private final JCheckBox myUseSpy = new JCheckBox("Use spy");
  private final JCheckBox myUseStaticMethodMock = new JCheckBox("Mock static methods");
  private final JCheckBox myShowInheritedMethodsBox = new JCheckBox(
      JavaBundle.message("intention.create.test.dialog.show.inherited"));
  private final MemberSelectionTable myMethodsTable = new MemberSelectionTable(
      Collections.emptyList(), null);

  public CreateTestsDialog(@NotNull Project project,
      @NotNull String title,
      PsiClass targetClass,
      PsiPackage targetPackage,
      Module targetModule) {
    super(project, true);
    myProject = project;

    myTargetClass = targetClass;
    myTargetPackage = targetPackage;
    myTargetModule = targetModule;

    setTitle(title);
    init();
  }

  protected String suggestTestClassName(PsiClass targetClass) {
    JavaCodeStyleSettings customSettings = JavaCodeStyleSettings
        .getInstance(targetClass.getContainingFile());
    String prefix = customSettings.TEST_NAME_PREFIX;
    String suffix = customSettings.TEST_NAME_SUFFIX;
    return prefix + targetClass.getName() + suffix;
  }

  private void updateMethodsTable() {
    List<MemberInfo> methods = TestIntegrationUtils.extractClassMethods(
        myTargetClass, myShowInheritedMethodsBox.isSelected());

    Set<PsiMember> selectedMethods = new HashSet<>();
    for (MemberInfo each : myMethodsTable.getSelectedMemberInfos()) {
      selectedMethods.add(each.getMember());
    }
    for (MemberInfo each : methods) {
      each.setChecked(selectedMethods.contains(each.getMember()));
    }

    myMethodsTable.setMemberInfos(methods);

    //TODO here add additional parameters based on chosen framework
//    TableColumn column = new TableColumn();
//    column.setHeaderValue("Additional column");
//    myMethodsTable.addColumn(column);
  }

  private String getDefaultLibraryName() {
    return getProperties().getValue(DEFAULT_LIBRARY_NAME_PROPERTY, "JUnit5");
  }

  private String getLastSelectedSuperClassName(TestFramework framework) {
    return getProperties().getValue(getDefaultSuperClassPropertyName(framework));
  }

  private static String getDefaultSuperClassPropertyName(TestFramework framework) {
    return DEFAULT_LIBRARY_SUPERCLASS_NAME_PROPERTY + "." + framework.getName();
  }

  private void restoreShowInheritedMembersStatus() {
    myShowInheritedMethodsBox
        .setSelected(getProperties().getBoolean(SHOW_INHERITED_MEMBERS_PROPERTY));
  }

  private void saveShowInheritedMembersStatus() {
    getProperties()
        .setValue(SHOW_INHERITED_MEMBERS_PROPERTY, myShowInheritedMethodsBox.isSelected());
  }

  private PropertiesComponent getProperties() {
    return PropertiesComponent.getInstance(myProject);
  }

  @Override
  protected String getDimensionServiceKey() {
    return getClass().getName();
  }

  @Override
  protected String getHelpId() {
    return "reference.dialogs.createTest";
  }

  @Override
  public JComponent getPreferredFocusedComponent() {
    return myTargetClassNameField;
  }

  @Override
  protected JComponent createCenterPanel() {
    JPanel panel = new JPanel(new GridBagLayout());

    GridBagConstraints constr = new GridBagConstraints();

    constr.fill = GridBagConstraints.HORIZONTAL;
    constr.anchor = GridBagConstraints.WEST;

    int gridy = 1;

    constr.insets = insets(4);
    constr.gridy = gridy++;
    constr.gridx = 0;
    constr.weightx = 0;
    final JLabel settingsLabel = new JLabel("Choose testing framework in application settings");
    settingsLabel.setForeground(JBColor.BLUE.darker());
    settingsLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
    settingsLabel.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        Project dataAsync = (Project) IDEAUtil
            .getDataAsync(CommonDataKeys.PROJECT.getName());
        ShowSettingsUtil
            .getInstance().showSettingsDialog(dataAsync, AppSettingsConfigurable.class);
      }
    });
    panel.add(settingsLabel);

    constr.gridheight = 1;

    constr.insets = insets(6);
    constr.gridy = gridy++;
    constr.gridx = 0;
    constr.weightx = 0;
    constr.gridwidth = 1;
    panel.add(new JLabel(JavaBundle.message("intention.create.test.dialog.class.name")), constr);

    myTargetClassNameField = new EditorTextField(suggestTestClassName(myTargetClass));
    myTargetClassNameField.getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void documentChanged(@NotNull DocumentEvent e) {
        getOKAction().setEnabled(PsiNameHelper.getInstance(myProject).isIdentifier(getClassName()));
      }
    });

    constr.gridx = 1;
    constr.weightx = 1;
    panel.add(myTargetClassNameField, constr);

    constr.insets = insets(1);
    constr.gridy = gridy++;
    constr.gridx = 0;
    constr.weightx = 0;
    panel.add(new JLabel(JavaBundle.message("dialog.create.class.destination.package.label")),
        constr);

    constr.gridx = 1;
    constr.weightx = 1;

    String targetPackageName = myTargetPackage != null ? myTargetPackage.getQualifiedName() : "";
    myTargetPackageField = new PackageNameReferenceEditorCombo(targetPackageName, myProject,
        RECENTS_KEY, JavaBundle.message("dialog.create.class.package.chooser.title"));

    new AnAction() {
      @Override
      public void actionPerformed(@NotNull AnActionEvent e) {
        myTargetPackageField.getButton().doClick();
      }
    }.registerCustomShortcutSet(new CustomShortcutSet(
            KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, InputEvent.SHIFT_DOWN_MASK)),
        myTargetPackageField.getChildComponent());
    JPanel targetPackagePanel = new JPanel(new BorderLayout());
    targetPackagePanel.add(myTargetPackageField, BorderLayout.CENTER);
    panel.add(targetPackagePanel, constr);

    constr.insets = insets(6);
    constr.gridy = gridy++;
    constr.gridx = 0;
    constr.weightx = 0;
    panel.add(new JLabel(JavaBundle.message("intention.create.test.dialog.generate")), constr);

    constr.gridx = 1;
    constr.weightx = 1;
    myGenerateBeforeBox.setEnabled(false);
    panel.add(myGenerateBeforeBox, constr);

    constr.insets = insets(1);
    constr.gridy = gridy++;
    myGenerateAfterBox.setEnabled(false);
    panel.add(myGenerateAfterBox, constr);

    constr.insets = insets(2);
    constr.gridy = gridy++;
    myUseSpy.setToolTipText("Force engine to wrap with spy constructor creating object under test");
    panel.add(myUseSpy, constr);

    constr.insets = insets(3);
    constr.gridy = gridy++;
    myUseSpy.setToolTipText("Mock calls to static methods");
    panel.add(myUseStaticMethodMock, constr);

    constr.insets = insets(6);
    constr.gridy = gridy++;
    constr.gridx = 0;
    constr.weightx = 0;
    final JLabel membersLabel = new JLabel(
        JavaBundle.message("intention.create.test.dialog.select.methods"));
    membersLabel.setLabelFor(myMethodsTable);
    panel.add(membersLabel, constr);

    constr.gridx = 1;
    constr.weightx = 1;
    panel.add(myShowInheritedMethodsBox, constr);

    constr.insets = insets(1, 8);
    constr.gridy = gridy++;
    constr.gridx = 0;
    constr.gridwidth = GridBagConstraints.REMAINDER;
    constr.fill = GridBagConstraints.BOTH;
    constr.weighty = 1;
    panel.add(ScrollPaneFactory.createScrollPane(myMethodsTable), constr);

    myShowInheritedMethodsBox.addActionListener(e -> updateMethodsTable());
    restoreShowInheritedMembersStatus();
    updateMethodsTable();
    return panel;
  }

  private static Insets insets(int top) {
    return insets(top, 0);
  }

  private static Insets insets(int top, int bottom) {
    return JBUI.insets(top, 8, bottom, 8);
  }

  public String getClassName() {
    return myTargetClassNameField.getText();
  }

  public PsiClass getTargetClass() {
    return myTargetClass;
  }


  public PsiDirectory getTargetDirectory() {
    return myTargetDirectory;
  }

  public Collection<MemberInfo> getSelectedMethods() {
    return myMethodsTable.getSelectedMemberInfos();
  }

  public boolean shouldGeneratedAfter() {
    return myGenerateAfterBox.isSelected();
  }

  public boolean shouldGeneratedBefore() {
    return myGenerateBeforeBox.isSelected();
  }

  public TestFramework getSelectedTestFrameworkDescriptor() {
    TestFramework testFramework = SettingsUtil
        .getTestFramework(AppSettingsState.getInstance().framework);
    final List<TestFramework> descriptors = new SmartList<>(
        TestFramework.EXTENSION_NAME.getExtensionList());
    descriptors.sort((d1, d2) -> Comparing.compare(d1.getName(), d2.getName()));
    for (final TestFramework framework : descriptors) {
      if (framework.equals(testFramework)) {
        return framework;
      }
    }
    return null;
  }

  public boolean useSpy() {
    return myUseSpy.isSelected();
  }

  public boolean mockStatic() {
    return myUseStaticMethodMock.isSelected();
  }

  @Override
  protected void doOKAction() {
    RecentsManager.getInstance(myProject)
        .registerRecentEntry(RECENTS_KEY, myTargetPackageField.getText());


    String errorMessage = null;
    try {
      myTargetDirectory = selectTargetDirectory();
      if (myTargetDirectory == null) {
        Messages.showInfoMessage(
            "Couldn't find test package in the project : " + myTargetPackageField.getText(),
            "Incorrect Target Directory");
        return;
      }
    } catch (IncorrectOperationException e) {
      errorMessage = e.getMessage();
    }

    if (errorMessage == null) {
      try {
        errorMessage = checkCanCreateClass();
      } catch (IncorrectOperationException e) {
        errorMessage = e.getMessage();
      }
    }

    if (errorMessage != null) {
      final int result = Messages
          .showOkCancelDialog(myProject,
              JavaBundle.message("dialog.message.0.update.existing.class", errorMessage),
              CommonBundle.getErrorTitle(), Messages.getErrorIcon());
      if (result == Messages.CANCEL) {
        return;
      }
    }

    saveShowInheritedMembersStatus();
    super.doOKAction();
  }

  protected String checkCanCreateClass() {
    return RefactoringMessageUtil.checkCanCreateClass(myTargetDirectory, getClassName());
  }

  @Nullable
  private PsiDirectory selectTargetDirectory() throws IncorrectOperationException {
    final String packageName = getPackageName();
    final PackageWrapper targetPackage = new PackageWrapper(PsiManager.getInstance(myProject),
        packageName);

//        final VirtualFile selectedRoot = ReadAction.compute(() -> {
//            final List<VirtualFile> testFolders = TestDirectoryCollector.computeTestRoots(myTargetModule);
//            List<VirtualFile> roots;
//            if (testFolders.isEmpty()) {
//                roots = new ArrayList<>();
//                List<String> urls = TestDirectoryCollector.computeSuitableTestRootUrls(myTargetModule);
//                for (String url : urls) {
//                    try {
//                        ContainerUtil.addIfNotNull(roots, VfsUtil.createDirectories(VfsUtilCore.urlToPath(url)));
//                    } catch (IOException e) {
//                        throw new RuntimeException(e);
//                    }
//                }
//                if (roots.isEmpty()) {
//                    JavaProjectRootsUtil.collectSuitableDestinationSourceRoots(myTargetModule, roots);
//                }
//                if (roots.isEmpty()) return null;
//            } else {
//                roots = new ArrayList<>(testFolders);
//            }
//
//            if (roots.size() == 1) {
//                return roots.get(0);
//            } else {
//                PsiDirectory defaultDir = chooseDefaultDirectory(targetPackage.getDirectories(), roots);
//                return MoveClassesOrPackagesUtil.chooseSourceRoot(targetPackage, roots, defaultDir);
//            }
//        });

    VirtualFile selectedRoot = SettingsUtil
        .getTestDirectory(myProject, ServiceManager.getService(AppSettingsState.class).testRoot);

    if (selectedRoot == null) {
      Messages.showInfoMessage(
          "Please go to settings of Enjoyable Testing plugin and set target directory",
          "Invalid Target Directory");
      return null;
    }

    return WriteCommandAction.writeCommandAction(myProject)
        .withName(CodeInsightBundle.message("create.directory.command"))
        .compute(
            () -> RefactoringUtil.createPackageDirectoryInSourceRoot(targetPackage, selectedRoot));
  }

  @Nullable
  private PsiDirectory chooseDefaultDirectory(PsiDirectory[] directories, List<VirtualFile> roots) {
    List<PsiDirectory> dirs = new ArrayList<>();
    PsiManager psiManager = PsiManager.getInstance(myProject);
    for (VirtualFile file : ModuleRootManager.getInstance(myTargetModule)
        .getSourceRoots(JavaSourceRootType.TEST_SOURCE)) {
      final PsiDirectory dir = psiManager.findDirectory(file);
      if (dir != null) {
        dirs.add(dir);
      }
    }
    if (!dirs.isEmpty()) {
      for (PsiDirectory dir : dirs) {
        final String dirName = dir.getVirtualFile().getPath();
        if (dirName.contains("generated")) {
          continue;
        }
        return dir;
      }
      return dirs.get(0);
    }
    for (PsiDirectory dir : directories) {
      final VirtualFile file = dir.getVirtualFile();
      for (VirtualFile root : roots) {
        if (VfsUtilCore.isAncestor(root, file, false)) {
          final PsiDirectory rootDir = psiManager.findDirectory(root);
          if (rootDir != null) {
            return rootDir;
          }
        }
      }
    }
    return ModuleManager.getInstance(myProject)
        .getModuleDependentModules(myTargetModule)
        .stream().flatMap(module -> ModuleRootManager.getInstance(module)
            .getSourceRoots(JavaSourceRootType.TEST_SOURCE).stream())
        .map(psiManager::findDirectory).findFirst().orElse(null);
  }

  private String getPackageName() {
    String name = myTargetPackageField.getText();
    return name != null ? name.trim() : "";
  }

}
