package application.settings;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.util.xmlb.XmlSerializerUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@State(
    name = "enjoyableTesting.settings",
    storages = {@Storage("enjoyableTestingSettings.xml")}
)
public class AppSettingsState implements PersistentStateComponent<AppSettingsState> {

  public String framework = "spock";

  public String testRoot = null;

  public String superClassName = null;

  public static AppSettingsState getInstance() {
    return ServiceManager.getService(AppSettingsState.class);
  }

  @Override
  public @Nullable AppSettingsState getState() {
    return this;
  }

  @Override
  public void loadState(@NotNull AppSettingsState state) {
    XmlSerializerUtil.copyBean(state, this);
  }
}
