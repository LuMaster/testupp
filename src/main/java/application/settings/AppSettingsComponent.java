package application.settings;

import com.intellij.icons.AllIcons;
import com.intellij.ide.util.PropertiesComponent;
import com.intellij.ide.util.TreeClassChooser;
import com.intellij.ide.util.TreeClassChooserFactory;
import com.intellij.java.JavaBundle;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.DumbService;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectFileIndex;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.roots.SourceFolder;
import com.intellij.openapi.ui.ComboBox;
import com.intellij.openapi.util.Comparing;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.JavaCodeFragment;
import com.intellij.psi.PsiClass;
import com.intellij.testIntegration.JavaTestFramework;
import com.intellij.testIntegration.TestFramework;
import com.intellij.ui.ReferenceEditorComboWithBrowseButton;
import com.intellij.ui.SimpleListCellRenderer;
import com.intellij.util.SmartList;
import com.intellij.util.ui.JBUI;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.plugins.groovy.ext.spock.SpockTestFramework;
import org.jetbrains.plugins.groovy.testIntegration.GroovyTestFramework;
import util.SettingsUtil;

public class AppSettingsComponent {

  private static final String DEFAULT_LIBRARY_NAME_PROPERTY =
      AppSettingsComponent.class.getName() + ".defaultLibrary";
  private static final String DEFAULT_LIBRARY_SUPERCLASS_NAME_PROPERTY =
      AppSettingsComponent.class.getName() + ".defaultLibrarySuperClass";
  private static final String RECENTS_KEY = "AppSettingsComponent.RecentsKey";
  public static final String RECENT_SUPERS_KEY = "AppSettingsComponent.Recents.Supers";
  private static final String RECENT_TESTDIR_KEY = "AppSettingsComponent.Recents.testDir";

  private JPanel myMainPanel;

  private ComboBox<TestFramework> myLibrariesCombo;

  private final Project myProject;

  private TestFramework descriptor;

  private ReferenceEditorComboWithBrowseButton mySuperClassField;

  private ComboBox<VirtualFile> myTestTargetDirectory;

  private JLabel myFixLibraryLabel;
  private JPanel myFixLibraryPanel;
  private final JButton myFixLibraryButton = new JButton(
      JavaBundle.message("intention.create.test.dialog.fix.library"));

  private final Map<String, String> keysMap = new HashMap<>();

  public AppSettingsComponent(Project myProject) {
    this.myProject = myProject;
    myMainPanel = new JPanel(new GridBagLayout());

    if (myProject == null) {
      return;
    }

    GridBagConstraints constr = new GridBagConstraints();
    constr.fill = GridBagConstraints.HORIZONTAL;
    constr.anchor = GridBagConstraints.WEST;

    int gridy = 1;

    constr.gridy = gridy++;
    constr.gridx = 0;
    constr.weightx = 0;

    myLibrariesCombo = new ComboBox<>(
        new DefaultComboBoxModel<>());

    final JLabel libLabel = new JLabel(
        JavaBundle.message("intention.create.test.dialog.testing.library"));
    libLabel.setLabelFor(myLibrariesCombo);
    myMainPanel.add(libLabel, constr);

    constr.gridx = 1;
    constr.weightx = 1;
    constr.gridwidth = GridBagConstraints.REMAINDER;
    myMainPanel.add(myLibrariesCombo, constr);

    constr.gridx = 1;
    constr.weightx = 1;
    constr.gridwidth = GridBagConstraints.REMAINDER;
    myMainPanel.add(myLibrariesCombo, constr);

    myFixLibraryPanel = new JPanel(new BorderLayout());
    myFixLibraryLabel = new JLabel();
    myFixLibraryLabel.setIcon(AllIcons.Actions.IntentionBulb);
    myFixLibraryPanel.add(myFixLibraryLabel, BorderLayout.CENTER);
    myFixLibraryPanel.add(myFixLibraryButton, BorderLayout.EAST);

    constr.insets = insets(1);
    constr.gridy = gridy++;
    constr.gridx = 0;
    myMainPanel.add(myFixLibraryPanel, constr);

    constr.insets = insets(1);
    constr.gridy = gridy++;
    constr.gridx = 0;
    constr.weightx = 0;
    myMainPanel
        .add(new JLabel(JavaBundle.message("intention.create.test.dialog.super.class")),
            constr);
    mySuperClassField = new ReferenceEditorComboWithBrowseButton(
        new MyChooseSuperClassAction(), null, myProject, true,
        JavaCodeFragment.VisibilityChecker.EVERYTHING_VISIBLE, RECENT_SUPERS_KEY);
    mySuperClassField.setMinimumSize(mySuperClassField.getPreferredSize());
    constr.gridx = 1;
    constr.weightx = 1;
    myMainPanel.add(mySuperClassField, constr);

    constr.insets = insets(1);
    constr.gridy = gridy++;
    constr.gridx = 0;
    constr.weightx = 0;
    myMainPanel
        .add(new JLabel("Test directory:"),
            constr);
    myTestTargetDirectory = new ComboBox<>(
        new DefaultComboBoxModel<>());
    constr.gridx = 1;
    constr.weightx = 1;
    myMainPanel.add(myTestTargetDirectory, constr);

    myTestTargetDirectory.setRenderer(SimpleListCellRenderer.create((label, value, index) -> {
      if (value != null) {
        label.setText(value.getPath());
      }
    }));

    myLibrariesCombo.setRenderer(SimpleListCellRenderer.create((label, value, index) -> {
      if (value != null) {
        label.setText(value.getName());
        label.setIcon(value.getIcon());
      }
    }));

    final String defaultLibrary = getDefaultLibraryName();
    final DefaultComboBoxModel<TestFramework> model = (DefaultComboBoxModel<TestFramework>) myLibrariesCombo
        .getModel();

    SettingsUtil.actionOnPositiveFramework((fr) -> {
      model.addElement(fr);
      if (Objects.equals(defaultLibrary, fr.getName().toLowerCase(Locale.ROOT))) {
        descriptor = fr;
      }
    });

    myLibrariesCombo.addActionListener(e -> {
      descriptor = (TestFramework) myLibrariesCombo.getSelectedItem();
    });

    myLibrariesCombo.addActionListener(e -> {
      final Object selectedItem = myLibrariesCombo.getSelectedItem();
      if (selectedItem != null) {
        final DumbService dumbService = DumbService.getInstance(myProject);
        dumbService.runWithAlternativeResolveEnabled(() ->
            onLibrarySelected((TestFramework) selectedItem));
      }
    });

    myLibrariesCombo.setSelectedItem(descriptor);

    for (VirtualFile root : ProjectRootManager.getInstance(myProject)
        .getContentSourceRoots()) {
      DefaultComboBoxModel<VirtualFile> testTargetDirectoryModel = (DefaultComboBoxModel<VirtualFile>) myTestTargetDirectory
          .getModel();

      SourceFolder sourceFolder = ProjectFileIndex.getInstance(myProject).getSourceFolder(root);
      if (sourceFolder != null) {
        if (sourceFolder.isTestSource()) {
          testTargetDirectoryModel.addElement(root);
          if (descriptor instanceof SpockTestFramework
              || descriptor instanceof GroovyTestFramework) {
            if (root.getPath().contains("groovy")) {
              myTestTargetDirectory.setSelectedItem(root);
            }
          }
        }
      }
    }

    // If user first time use application set
    if (getTestRootFromAppState() == null) {
      VirtualFile selectedItem = (VirtualFile) myTestTargetDirectory
          .getSelectedItem();
      if (selectedItem != null) {
        ServiceManager
            .getService(AppSettingsState.class).testRoot = selectedItem.getPath();
      }
    }
  }

  private String getTestRootFromAppState() {
    return ServiceManager.getService(AppSettingsState.class).testRoot;
  }

  public JComponent getPanel() {
    return myMainPanel;
  }

  private static Insets insets(int top) {
    return insets(top, 0);
  }

  private static Insets insets(int top, int bottom) {
    return JBUI.insets(top, 8, bottom, 8);
  }

  private PropertiesComponent getProperties() {
    return PropertiesComponent.getInstance(myProject);
  }

  private String getDefaultLibraryName() {
    return getProperties().getValue(DEFAULT_LIBRARY_NAME_PROPERTY, "spock");
  }

  private String getLastSelectedSuperClassName(TestFramework framework) {
    return getProperties().getValue(getDefaultSuperClassPropertyName(framework));
  }

  public void saveDefaultLibraryNameAndSuperClass() {
    getProperties()
        .setValue(getDefaultSuperClassPropertyName(descriptor), mySuperClassField.getText());
  }

  private static String getDefaultSuperClassPropertyName(TestFramework framework) {
    return DEFAULT_LIBRARY_SUPERCLASS_NAME_PROPERTY + "." + framework.getName();
  }

  @Nullable
  public String getSuperClassName() {
    String result = mySuperClassField.getText().trim();
    if (result.length() == 0) {
      return null;
    }
    return result;
  }

  public ReferenceEditorComboWithBrowseButton getMySuperClassField() {
    return mySuperClassField;
  }

  public TestFramework getTestFramework() {
    return descriptor;
  }

  public String getTestRoot() {
    return ((VirtualFile) myTestTargetDirectory.getSelectedItem()).getPath();
  }

  public void setTestFramework(TestFramework testFramework) {
    final List<TestFramework> descriptors = new SmartList<>(
        TestFramework.EXTENSION_NAME.getExtensionList());
    descriptors.sort((d1, d2) -> Comparing.compare(d1.getName(), d2.getName()));

    for (final TestFramework framework : descriptors) {
      if (!framework.equals(testFramework)) {
        continue;
      }
      myLibrariesCombo.setSelectedItem(framework);
      descriptor = framework;
    }
  }

  private void onLibrarySelected(TestFramework testFramework) {
    myFixLibraryPanel.setVisible(true);
    String text = JavaBundle
        .message("intention.create.test.dialog.library.not.found", testFramework.getName());
    myFixLibraryLabel.setText(text);
    myFixLibraryButton.setVisible(testFramework instanceof JavaTestFramework
        && ((JavaTestFramework) testFramework).getFrameworkLibraryDescriptor() != null
        || testFramework.getLibraryPath() != null);

    String libraryDefaultSuperClass = testFramework.getDefaultSuperClass();
    String lastSelectedSuperClass = getLastSelectedSuperClassName(testFramework);
    String superClass =
        lastSelectedSuperClass != null ? lastSelectedSuperClass : libraryDefaultSuperClass;

    if (isSuperclassSelectedManually()) {
      if (superClass != null) {
        String currentSuperClass = mySuperClassField.getText();
        mySuperClassField.appendItem(superClass);
        mySuperClassField.setText(currentSuperClass);
      }
    } else {
      mySuperClassField.appendItem(StringUtil.notNullize(superClass));
      mySuperClassField.getChildComponent().setSelectedItem(superClass == null ? "" : superClass);
    }

    descriptor = testFramework;
  }

  private boolean isSuperclassSelectedManually() {
    String superClass = mySuperClassField.getText();
    if (StringUtil.isEmptyOrSpaces(superClass)) {
      return false;
    }

    for (TestFramework framework : TestFramework.EXTENSION_NAME.getExtensions()) {
      if (superClass.equals(framework.getDefaultSuperClass())) {
        return false;
      }
      if (superClass.equals(getLastSelectedSuperClassName(framework))) {
        return false;
      }
    }

    return true;
  }

  public void setTestRoot(String testRoot) {
    if (testRoot == null) {
      return;
    }
    VirtualFile testDirectory = SettingsUtil.getTestDirectory(myProject, testRoot);
    myTestTargetDirectory.setSelectedItem(testDirectory);
  }

  public Project getMyProject() {
    return myProject;
  }


  class MyChooseSuperClassAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      TreeClassChooserFactory f = TreeClassChooserFactory.getInstance(myProject);
      TreeClassChooser dialog =
          f.createAllProjectScopeChooser(
              JavaBundle.message("intention.create.test.dialog.choose.super.class"));
      dialog.showDialog();
      PsiClass aClass = dialog.getSelected();
      if (aClass != null) {
        String superClass = aClass.getQualifiedName();

        mySuperClassField.appendItem(superClass);
        mySuperClassField.getChildComponent().setSelectedItem(superClass);
      }
    }
  }
}
