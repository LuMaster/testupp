package application.settings;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.options.ShowSettingsUtil;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;
import util.IDEAUtil;

public class ShowSettings extends AnAction {

  @Override
  public void actionPerformed(@NotNull AnActionEvent e) {
    Project dataAsync = (Project) IDEAUtil
        .getDataAsync(CommonDataKeys.PROJECT.getName());
    ShowSettingsUtil.getInstance().showSettingsDialog(dataAsync,AppSettingsConfigurable.class);
  }
}
