package application.settings;

import com.intellij.openapi.Disposable;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Disposer;
import com.intellij.ui.RecentsManager;
import java.util.Locale;
import java.util.Objects;
import javax.swing.JComponent;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import util.IDEAUtil;
import util.SettingsUtil;

public class AppSettingsConfigurable implements Configurable, @NotNull Disposable {

  private AppSettingsComponent mySettingsComponent;

  // A default constructor with no arguments is required because this implementation
  // is registered as an applicationConfigurable EP

  @Nls(capitalization = Nls.Capitalization.Title)
  @Override
  public String getDisplayName() {
    return "Enjoyable Testing";
  }

  @Override
  public JComponent getPreferredFocusedComponent() {
    return mySettingsComponent.getPanel();
  }

  @SneakyThrows
  @Nullable
  @Override
  public JComponent createComponent() {
    Project dataAsync = IDEAUtil.getActiveProject();
    if (dataAsync != null || mySettingsComponent == null) {
      mySettingsComponent = new AppSettingsComponent(dataAsync);
    }
    return mySettingsComponent.getPanel();
  }

  @Override
  public boolean isModified() {
    AppSettingsState settings = AppSettingsState.getInstance();
    boolean modified = !mySettingsComponent.getTestFramework()
        .equals(SettingsUtil.getTestFramework(settings.framework));
    modified = modified || !mySettingsComponent.getTestRoot().equals(settings.testRoot);
    modified = modified || !Objects
        .equals(mySettingsComponent.getSuperClassName(), settings.superClassName);
    return modified;
  }

  @Override
  public void apply() {
    AppSettingsState settings = AppSettingsState.getInstance();

    RecentsManager.getInstance(mySettingsComponent.getMyProject())
        .registerRecentEntry(
            AppSettingsComponent.RECENT_SUPERS_KEY,
            mySettingsComponent.getMySuperClassField().getText());

    settings.framework = mySettingsComponent.getTestFramework().getName().toLowerCase(Locale.ROOT);
    settings.testRoot = mySettingsComponent.getTestRoot();
    settings.superClassName = mySettingsComponent.getSuperClassName();

    mySettingsComponent.saveDefaultLibraryNameAndSuperClass();
  }

  @Override
  public void reset() {
    AppSettingsState settings = AppSettingsState.getInstance();
    mySettingsComponent.setTestFramework(SettingsUtil.getTestFramework(settings.framework));
    mySettingsComponent.setTestRoot(settings.testRoot);
    if (settings.superClassName != null) {
      mySettingsComponent.getMySuperClassField().setText(settings.superClassName);
    }

  }

  @Override
  public void disposeUIResources() {
    mySettingsComponent = null;
  }

  @Override
  public void dispose() {
    Disposer.dispose(this);
  }
}
