package application;

import api.ISelectionProvider;
import api.ITestGenerator;
import com.intellij.codeInsight.CodeInsightBundle;
import com.intellij.codeInsight.CodeInsightUtil;
import com.intellij.codeInsight.FileModificationService;
import com.intellij.codeInsight.TestFrameworks;
import com.intellij.codeInsight.hint.HintManager;
import com.intellij.ide.util.PackageChooserDialog;
import com.intellij.ide.util.PsiClassListCellRenderer;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.command.CommandProcessor;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleUtilCore;
import com.intellij.openapi.project.DumbService;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.psi.JavaDirectoryService;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiJavaFile;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiPackage;
import com.intellij.refactoring.util.classMembers.MemberInfo;
import com.intellij.testIntegration.TestFinderHelper;
import com.intellij.testIntegration.TestFramework;
import com.intellij.testIntegration.createTest.CreateTestAction;
import com.intellij.ui.components.JBList;
import com.intellij.util.containers.ContainerUtil;
import data.collectors.TestDirectoryCollector;
import data.configs.TestConfiguration;
import data.providers.TestCreatorProvider;
import dialogs.CreateTestsDialog;
import dialogs.MissedTestsDialog;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.jetbrains.annotations.NotNull;

public class GenerateTestsAction extends AnAction {

    @Override
    public void actionPerformed(@NotNull AnActionEvent anActionEvent) {
        ISelectionProvider service = ServiceManager.getService(ISelectionProvider.class);

        PsiElement lastSelectedElement = service.getLastSelectedElement();
        if (lastSelectedElement == null) {
            Messages.showInfoMessage("Not supported object for generating tests.", "Not Supported Object");
            return;
        }
        PsiClass srcClass = getPsiClass(lastSelectedElement);
        if (srcClass == null) {
            Messages.showInfoMessage(anActionEvent.getProject(), "Unsupported source language in this version.\nThis version of plugin supports only Java language.\nSupport me on patreon to help me develop better plugin for creating tests", "Unsupported Source Language");
            return;
        }

        if (!CreateTestAction.isAvailableForElement(srcClass)) {
            return;
        }
        Project project = srcClass.getProject();

        Collection<PsiElement> testClasses = TestFinderHelper.findTestsForClass(srcClass);
        testClasses = filterClassesByTestFramework(testClasses);
        // Generate tests for non existing class section
        if (testClasses.isEmpty()) {
            // Find proper module if other that src module and create source test folder if there is no such in current module
            Module moduleForSrc = ModuleUtilCore.findModuleForPsiElement(srcClass);
            if (moduleForSrc == null) {
                Messages.showErrorDialog("There is no source module for this file.", "No Source Module");
                return;
            }
            Module moduleForTests = CreateTestAction.suggestModuleForTests(project, moduleForSrc);
            PsiPackage srcPackage = JavaDirectoryService.getInstance().getPackage(srcClass.getContainingFile().getContainingDirectory());
            if (srcPackage == null) {
                return;
            }
            if (TestDirectoryCollector.computeSuitableTestRootUrls(moduleForTests).isEmpty()) {
                // 1. Call generating new test source
//                TestSourceFolderCreator sourceFolderCreator = new TestSourceFolderCreator(project);
//                sourceFolderCreator.createTestsSourceFolder(moduleForSrc, moduleForTests, srcPackage);
//                PsiPackage createdPackage = sourceFolderCreator.getCreatedPackage();
//                if (createdPackage == null) {
//                    return;
//                }
                Messages.showInfoMessage(project, "No suitable test roots. Please create some test modules/roots and repeat operation.\nAutomated generation of test roots will be added in future versions.", "No Suitable Roots");
                PackageChooserDialog packageChooserDialog = new PackageChooserDialog(
                    "Choose Package in Which Tests Will Be Created", project);
                boolean isOk = packageChooserDialog.showAndGet();
                if (isOk) {
                    PsiPackage selectedPackage = packageChooserDialog.getSelectedPackage();
                    generateNewTest(project, srcClass, selectedPackage, moduleForTests);
                }
            } else {
                generateNewTest(project, srcClass, srcPackage, moduleForTests);
            }
            return;
        }
        Editor editor = anActionEvent.getData(PlatformDataKeys.EDITOR);
        if (editor == null) {
            return;
        }
        if (testClasses.size() == 1) {
            generateMissedTests((PsiClass) ContainerUtil.getFirstItem(testClasses), srcClass, editor);
            return;
        }

        // Generate tests for multiple existing class section
        final JBList list = new JBList(testClasses);
        list.setCellRenderer(new PsiClassListCellRenderer());
        final PsiClass finalSrcClass = srcClass;
        JBPopupFactory.getInstance().createListPopupBuilder(list)
                .setItemChoosenCallback(() -> generateMissedTests((PsiClass) list.getSelectedValue(), finalSrcClass, editor))
                .setTitle("Choose Test")
                .createPopup().showInBestPositionFor(editor);
    }

    private Collection<PsiElement> filterClassesByTestFramework(Collection<PsiElement> classes) {
        List<PsiElement> result = new ArrayList<>();
        for (PsiElement aClass : classes) {
            // TODO in a future I need to separate choosing test framework before this action to know which files we can write to
            if (aClass instanceof PsiClass && !(aClass.getContainingFile() instanceof PsiJavaFile)) {
                result.add(aClass);
            }
        }
        return result;
    }

    private PsiClass getPsiClass(PsiElement lastSelectedElement) {
        if (lastSelectedElement instanceof PsiMethod) {
            return ((PsiMethod) lastSelectedElement).getContainingClass();
        }
        if (lastSelectedElement instanceof PsiClass) {
            return ((PsiClass) lastSelectedElement);
        }
        if (lastSelectedElement instanceof PsiJavaFile) {
            //TODO: Need better way ?
            return ((PsiJavaFile) lastSelectedElement).getClasses()[0];
        }
        return null;
    }

    public void generateNewTest(Project project, PsiClass srcClass, PsiPackage srcPackage, Module moduleForTests) {
        DumbService.getInstance(project).runWhenSmart(() -> {
            final CreateTestsDialog d = new CreateTestsDialog(project, "Create Advanced Tests", srcClass, srcPackage, moduleForTests);
            if (!d.showAndGet()) {
                return;
            }
            CommandProcessor.getInstance().
                    executeCommand(
                            project,
                            () -> {
                              TestFramework framework = d.getSelectedTestFrameworkDescriptor();
                              TestConfiguration.Builder builder = new TestConfiguration.Builder();
                              builder.useSpy(d.useSpy());
                              builder.mockStatic(d.mockStatic());
                              builder.framework(framework);
                              builder.project(project);
                              final ITestGenerator generator = TestCreatorProvider
                                  .forLanguage(srcClass, framework, builder.build());
                              DumbService.getInstance(project).withAlternativeResolveEnabled(() -> {
                                PsiElement psiElement = generator.generateTest(project, d);
                              });
                            },
                            CodeInsightBundle.message("intention.create.test"),
                            this);
        });
    }

    private void generateMissedTests(PsiClass testClass, PsiClass srcClass, Editor srcEditor) {
        if (testClass != null) {
            final TestFramework framework = TestFrameworks.detectFramework(testClass);
            if (framework != null) {
                final Project project = testClass.getProject();
                final Editor editor = CodeInsightUtil.positionCursorAtLBrace(project, testClass.getContainingFile(), testClass);
                if (!FileModificationService.getInstance().preparePsiElementsForWrite(testClass)) return;
                final MissedTestsDialog dialog = new MissedTestsDialog(project, srcClass, testClass, framework);
                if (dialog.showAndGet()) {
                    TestConfiguration.Builder builder = new TestConfiguration.Builder();
                    builder.useSpy(dialog.useSpy());
                    final ITestGenerator generator = TestCreatorProvider.forLanguage(srcClass, framework, builder.build());
                    Collection<MemberInfo> selectedMethods = dialog.getSelectedMethods();
                    WriteCommandAction.runWriteCommandAction(project, () -> generator.addTestMethods(editor, testClass, framework, selectedMethods));
                }
            } else {
                HintManager.getInstance().showErrorHint(srcEditor, "Failed to detect test framework for " + testClass.getQualifiedName());
            }
        }
    }
}
