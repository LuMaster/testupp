package application.services;

import com.intellij.ide.browsers.BrowserLauncher;
import com.intellij.notification.Notification;
import com.intellij.notification.NotificationDisplayType;
import com.intellij.notification.NotificationGroup;
import com.intellij.notification.NotificationType;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.IconLoader;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class PluginNotification {

  private static final NotificationGroup NOTIFICATION_GROUP =
      new NotificationGroup("Enjoyable Testing Notifications",
          NotificationDisplayType.BALLOON, true);

  public static void notify(@Nullable Project project) {
    String content = "If you find this plugin helpful or you want to support me to make this plugin even better, please donate :)";
//        Notification notification = NotificationGroupManager.getInstance()
//            .getNotificationGroup("Enjoyable Testing Notifications")
//            .createNotification("Enjoyable testing", "Donation", content,
//                NotificationType.INFORMATION);
    Notification notification = NOTIFICATION_GROUP.createNotification(
        "Enjoyable testing", "Donation", content,
        NotificationType.INFORMATION);
    AnAction donateAction = new AnAction("Donate", "",
        IconLoader.getIcon("/icons/paypal/paypal_13x13.png", PluginNotification.class)) {
      @Override
      public void actionPerformed(@NotNull AnActionEvent e) {
        BrowserLauncher.getInstance().open("http://paypal.me/lukasbasiura");
      }
    };
    notification.addAction(donateAction);
    notification
        .setIcon(IconLoader.getIcon("/icons/pluginIcon13x13.png", PluginNotification.class));
        notification.notify(project);
    }

}
