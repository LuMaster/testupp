package application.services;

import io.sentry.Sentry;
import io.sentry.SentryEvent;

public class MySentryClient {

  static {
    Sentry.init(options -> {
      options.setDsn("https://74b3514b48014229bd25490b2f716386@o557742.ingest.sentry.io/5690354");
    });
  }

  public void sendEvent(SentryEvent event) {
    Sentry.captureEvent(event);
  }
}
