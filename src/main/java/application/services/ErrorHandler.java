package application.services;

import com.intellij.diagnostic.AbstractMessage;
import com.intellij.diagnostic.IdeaReportingEvent;
import com.intellij.ide.DataManager;
import com.intellij.ide.plugins.IdeaPluginDescriptor;
import com.intellij.idea.IdeaLogger;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.diagnostic.ErrorReportSubmitter;
import com.intellij.openapi.diagnostic.IdeaLoggingEvent;
import com.intellij.openapi.diagnostic.SubmittedReportInfo;
import com.intellij.openapi.diagnostic.SubmittedReportInfo.SubmissionStatus;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.Task.Backgroundable;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.util.Consumer;
import io.sentry.SentryEvent;
import io.sentry.SentryLevel;
import io.sentry.protocol.Message;
import java.awt.Component;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import util.IDEAUtil;

public class ErrorHandler extends ErrorReportSubmitter {

  @Override
  public boolean submit(IdeaLoggingEvent @NotNull [] events,
      @Nullable String additionalInfo,
      @NotNull Component parentComponent,
      @NotNull Consumer<SubmittedReportInfo> consumer) {

    DataContext context = DataManager.getInstance().getDataContext(parentComponent);
    Project project = CommonDataKeys.PROJECT.getData(context);

    new Backgroundable(project, "Sending error report") {
      @Override
      public void run(@NotNull ProgressIndicator indicator) {
        StringBuilder envBuilder = new StringBuilder();
        if (getPluginDescriptor() instanceof IdeaPluginDescriptor) {
          envBuilder.append("VERSION: ").append(getPluginDescriptor().getVersion()).append("\n");
        }
        envBuilder.append("IDEA VERSION: ").append(IDEAUtil.applicationVersion()).append("\n");
        envBuilder.append("OS NAME: ").append(System.getProperty("os.name")).append("\n");

        // now, attach all exceptions to the message
        Map<String, Throwable> errors = new HashMap(events.length);
        for (IdeaLoggingEvent ideaEvent : events) {
          // this is the tricky part
          // ideaEvent.throwable is a com.intellij.diagnostic.IdeaReportingEvent.TextBasedThrowable
          // This is a wrapper and is only providing the original stacktrace via 'printStackTrace(...)',
          // but not via 'getStackTrace()'.
          //
          // Sentry accesses Throwable.getStackTrace(),
          // So, we workaround this by retrieving the original exception from the data property
          if (ideaEvent instanceof IdeaReportingEvent && ideaEvent
              .getData() instanceof AbstractMessage) {
            AbstractMessage data = (AbstractMessage) ideaEvent.getData();
            String info = data.getAdditionalInfo();
            long mostSignificantBits = UUID.randomUUID().getMostSignificantBits();
            String prefix = "[" + mostSignificantBits + "] ";
            errors.put(info == null ? prefix : prefix + info, data.getThrowable());
          } else {
            // ignoring this ideaEvent, you might not want to do this
          }
        }

        MySentryClient sentry = new MySentryClient();

        for (Entry<String, Throwable> entry : errors.entrySet()) {
          SentryEvent event = new SentryEvent(entry.getValue());
          Message sentryMessage = new Message();
          sentryMessage.setMessage(entry.getKey());
          event.setMessage(sentryMessage);
          event.setLevel(SentryLevel.ERROR);
          // set server name to empty to avoid tracking personal data
          event.setServerName("");
          event.setExtra("Environment", envBuilder.toString());
          event.setExtra("last_action", IdeaLogger.ourLastActionId);
          event.setEnvironment("production");
          sentry.sendEvent(event);
        }

        ApplicationManager.getApplication().invokeLater(() -> {
          // we're a bit lazy here.
          // Alternatively, we could add a listener to the sentry client
          // to be notified if the message was successfully send
          Messages.showInfoMessage(parentComponent, "Thank you for submitting your report!",
              "Error Report");
          consumer.consume(new SubmittedReportInfo(SubmissionStatus.NEW_ISSUE));
        });
      }
    }.queue();
    return true;
  }

  @Override
  public @NotNull String getReportActionText() {
    return "Report to Enjoyable Testing";
  }
}
