package application.services;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.startup.StartupActivity;
import org.jetbrains.annotations.NotNull;

public class StartupListener implements StartupActivity {

    @Override
    public void runActivity(@NotNull Project project) {
        PluginNotification.notify(project);
    }
}
