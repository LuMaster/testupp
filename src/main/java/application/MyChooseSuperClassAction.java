package application;

import com.intellij.ide.util.TreeClassChooser;
import com.intellij.ide.util.TreeClassChooserFactory;
import com.intellij.java.JavaBundle;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiClass;
import com.intellij.ui.ReferenceEditorComboWithBrowseButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyChooseSuperClassAction implements ActionListener {

  private final Project myProject;
  private final ReferenceEditorComboWithBrowseButton mySuperClassField;

  public MyChooseSuperClassAction(
      Project myProject,
      ReferenceEditorComboWithBrowseButton mySuperClassField) {
    this.myProject = myProject;
    this.mySuperClassField = mySuperClassField;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    TreeClassChooserFactory f = TreeClassChooserFactory.getInstance(myProject);
    TreeClassChooser dialog =
        f.createAllProjectScopeChooser(JavaBundle.message("intention.create.test.dialog.choose.super.class"));
    dialog.showDialog();
    PsiClass aClass = dialog.getSelected();
    if (aClass != null) {
      String superClass = aClass.getQualifiedName();

      mySuperClassField.appendItem(superClass);
      mySuperClassField.getChildComponent().setSelectedItem(superClass);
    }
  }
}