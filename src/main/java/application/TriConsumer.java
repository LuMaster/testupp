package application;

@FunctionalInterface
public interface TriConsumer<T, U, P> {
    void accept(T t, U u, P p);
}