package application;

import api.ISelectionProvider;
import com.intellij.openapi.application.Application;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiJavaFile;
import com.intellij.psi.PsiMethod;
import org.jetbrains.plugins.groovy.lang.psi.GroovyFile;

import static com.intellij.openapi.application.ApplicationManager.getApplication;
import static util.IDEAUtil.getActivePsiFile;
import static util.IDEAUtil.getCurrentProjectViewPane;
import static util.IDEAUtil.getProjectToolWindow;

public class SelectionProvider implements ISelectionProvider
{
    public PsiElement getLastSelectedElement()
    {
        Application application = getApplication();
        final PsiElement[] lastSelectedElement = new PsiElement[1];
        application.invokeAndWait(() -> getApplication().runReadAction(() -> {
            if (getProjectToolWindow().isActive()) {
                lastSelectedElement[0] = getCurrentProjectViewPane().getSelectedPSIElements()[0];
            } else {
                lastSelectedElement[0] = getActivePsiFile();
            }
        }));
        return lastSelectedElement[0];
    }

    private PsiElement getProperParent(PsiElement lastSelectedElement)
    {
        if (lastSelectedElement instanceof PsiMethod || lastSelectedElement instanceof PsiFile || lastSelectedElement instanceof PsiClass) {
            return lastSelectedElement;
        }
        PsiElement parent = lastSelectedElement.getParent();
        while (parent != null) {
            if (parent instanceof PsiMethod) {
                return parent;
            }
            if (parent instanceof PsiClass) {
                return parent;
            }
            if (parent instanceof PsiJavaFile || parent instanceof GroovyFile) {
                return parent;
            }
            parent = parent.getParent();
        }
        return null;
    }
}
