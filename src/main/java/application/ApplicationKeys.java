package application;

import com.intellij.openapi.util.Key;
import data.structure.FunctionInCode;
import generators.TestSectionLabel;

public class ApplicationKeys
{

    public static Key<TestSectionLabel> SECTION_TYPE_KEY = Key.create("testupp.section.type");

    public static Key<FunctionInCode> FUNCTION_IN_CODE = Key.create("testupp.type");

}
