package application.filetemplates;

import com.intellij.ide.fileTemplates.FileTemplateDescriptor;
import com.intellij.ide.fileTemplates.FileTemplateGroupDescriptor;
import com.intellij.ide.fileTemplates.FileTemplateGroupDescriptorFactory;
import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.util.IconLoader;

public class EnjoyableTestingFileTemplatesProvider implements FileTemplateGroupDescriptorFactory {

  @Override
  public FileTemplateGroupDescriptor getFileTemplatesDescriptor() {
    final FileTemplateGroupDescriptor group = new FileTemplateGroupDescriptor("Enjoyable Testing",
        IconLoader.findIcon("/icons/pluginIcon13x13.png"));
    group.addTemplate(
        new FileTemplateDescriptor("TestNGClass.java", JavaFileType.INSTANCE.getIcon()));
    return group;
  }
}
