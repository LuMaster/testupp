package application;

import com.intellij.openapi.diagnostic.Logger;
import org.apache.log4j.Level;

public class TestUppLogger {

    private TestUppLogger() {
    }

    private static class SingletonHelper {
        private static final Logger LOGGER = Logger.getInstance(TestUppLogger.class);
    }

    public static Logger getLogger() {
        SingletonHelper.LOGGER.setLevel(Level.DEBUG);
        return SingletonHelper.LOGGER;
    }

//    static {
//        FileHandler fh = null;
//        SimpleDateFormat format = new SimpleDateFormat("M-d_HHmmss");
//        try {
//            fh = new FileHandler("C:/temp/test/MyLogFile_"
//                    + format.format(Calendar.getInstance().getTime()) + ".log");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        fh.setFormatter(new SimpleFormatter());
//        LOGGER.addHandler(fh);
//    }
}
