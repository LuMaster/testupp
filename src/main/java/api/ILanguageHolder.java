package api;

import com.intellij.testIntegration.TestFramework;

public interface ILanguageHolder {

  TestFramework getTestFramework();
}
