package api;

import com.intellij.psi.PsiDirectory;

public interface ITestLanguage {

  /**
   * @param types      def / public void
   * @param methodName method name
   * @param arguments  arguments
   * @param body       method body
   * @param directory
   */
  String getMethodTemplate(String types, String methodName, String arguments, String body,
      PsiDirectory directory);

  String getMethodTemplateTypes();

  String getStaticClassInitializer(String cfqName);

  String getNewObjectInitializer(String type, String args);

  String getNewExpression(String type, String arguments);

  String getSpyInitializerWithNewObject(String type, String arguments);

  String getSimpleMockStatement(String type, String object);

    String getSpecificMockStatement(String type, String object);

    /**
     * @param type
     * @param variable
     * @param object
     * @return declaration by assignment eg. String a = b
     */
    String getDeclarationAssignment(String type, String variable, String object);

    String getCollectionInitializer(String collectionType);

    /**
     * @param type
     * @param variable
     * @param object
     * @param methodCall
     * @return declaration of variable initialized with method call eg. obj.getA()
     */
    String getVariableDeclarationCallByMethod(String type, String variable , String object, String methodCall);

    /**
     * @param object - variable name
     * @param method - method with parameters
     */
    String getVoidCall(String object, String method);

    String joinMockWithReturn(String mock, String result);

    String getThenReturn();

    String getGivenSectionName();

    String getAndSectionName();

    String getWhenSectionName();

    String getThenSectionName();

    String getWhereSectionName();

    String getExpectSectionName();

    default String getLabelBasedOnName(String name) {
        switch (name) {
            case "GIVEN":
                return getGivenSectionName();
            case "AND":
                return getAndSectionName();
            case "WHEN":
                return getWhenSectionName();
            case "THEN":
                return getThenSectionName();
            case "WHERE":
                return getWhereSectionName();
            case "EXPECT":
                return getExpectSectionName();
        }
        return null;
    }
}
