package api;

import application.ApplicationKeys;
import com.intellij.psi.PsiElement;
import com.intellij.testIntegration.TestFramework;
import data.structure.FunctionInCode;
import generators.TestSectionLabel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractFlowCoverBranch {

    /**
     * I want to store only mocks for PsiVariables or PsiMethodCallExpressions for now !!
     */
    protected Map<PsiElement, PsiElement> mapOfMocks = new LinkedHashMap<>();

    private Set<String> restrictedNames = new HashSet<>();

    private final TestFramework framework;

    protected AbstractFlowCoverBranch(TestFramework language) {
        this.framework = language;
    }

    public TestFramework getTestFramework() {
        return framework;
    }

    public Set<String> getRestrictedNames() {
        return restrictedNames;
    }

    public void updateRestrictedNames(Collection<String> restrictedNames) {
        this.restrictedNames.addAll(restrictedNames);
    }


    /**
     * TODO replace 'SpockLabels' with something more common!
     * @param data
     * @return
     */
    @Nullable
    public TestSectionLabel getSectionType(PsiElement data) {
        return data.getUserData(ApplicationKeys.SECTION_TYPE_KEY);
    }

    @Nullable
    public FunctionInCode getFunctionInCode(PsiElement value) {
        return value.getUserData(ApplicationKeys.FUNCTION_IN_CODE);
    }

    public List<PsiElement> getOriginalElements(){
        return new ArrayList<>(mapOfMocks.keySet());
    }

    public abstract void addMock(PsiElement variable, PsiElement mock, TestSectionLabel label);
}
