package api;

import com.intellij.psi.PsiElement;

public interface ISelectionProvider {
    PsiElement getLastSelectedElement();
}
