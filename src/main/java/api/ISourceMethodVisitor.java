package api;

import com.intellij.psi.PsiMethod;

public interface ISourceMethodVisitor
{
    void collect(PsiMethod method);
}
