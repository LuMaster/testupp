package api;

import com.intellij.codeInsight.FileModificationService;
import com.intellij.codeInsight.actions.OptimizeImportsProcessor;
import com.intellij.ide.fileTemplates.FileTemplate;
import com.intellij.ide.fileTemplates.FileTemplateDescriptor;
import com.intellij.ide.fileTemplates.FileTemplateManager;
import com.intellij.ide.fileTemplates.FileTemplateUtil;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.psi.JavaDirectoryService;
import com.intellij.psi.JavaPsiFacade;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementFactory;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiJavaCodeReferenceElement;
import com.intellij.psi.PsiJavaFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiPackage;
import com.intellij.psi.PsiReferenceList;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.search.GlobalSearchScopesCore;
import com.intellij.refactoring.util.classMembers.MemberInfo;
import com.intellij.testIntegration.TestFramework;
import com.intellij.testIntegration.TestIntegrationUtils;
import com.intellij.util.IncorrectOperationException;
import com.siyeh.ig.psiutils.ImportUtils;
import data.providers.SourceMethodVisitorProvider;
import data.structure.FlowCoverContainer;
import data.structure.FlowCoverSection;
import data.structure.tests.SpockTest;
import dialogs.CreateTestsDialog;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.plugins.groovy.actions.GroovyTemplates;
import org.jetbrains.plugins.groovy.annotator.intentions.CreateClassActionBase;
import org.jetbrains.plugins.groovy.lang.psi.GroovyFileBase;
import org.jetbrains.plugins.groovy.lang.psi.GroovyPsiElementFactory;
import org.jetbrains.plugins.groovy.lang.psi.api.statements.typedef.GrExtendsClause;
import org.jetbrains.plugins.groovy.lang.psi.api.statements.typedef.GrTypeDefinition;
import org.jetbrains.plugins.groovy.lang.psi.api.types.GrCodeReferenceElement;

public interface ITestGenerator {

    default void addToDocument(FlowCoverContainer flowCoverContainer,
        ILanguageGenerator mockGenerator, Editor editor, PsiClass targetClass) {
        Project project = targetClass.getProject();
        PsiDocumentManager.getInstance(project).commitDocument(editor.getDocument());
        List<FlowCoverSection> flowCoverSections = flowCoverContainer.getFlowCoverSections();
        PsiDirectory directory = targetClass.getContainingFile().getContainingDirectory();
        for (FlowCoverSection flowCoverSection : flowCoverSections) {
            SpockTest spockTest = flowCoverSection.convertToTest();
            PsiMethod methodFromText = mockGenerator.getJVMElementFactory()
                .createMethodFromText(spockTest.createTestFromText(directory), targetClass);
            mockGenerator.addAnnotations(methodFromText);
            targetClass.add(methodFromText);
        }
        for (PsiClass aClass : flowCoverContainer.getClassesToImport().stream()
            .filter(Objects::nonNull)
            .collect(Collectors.toSet())) {
            addImport(aClass, targetClass);
        }
        OptimizeImportsProcessor processor = new OptimizeImportsProcessor(project,
            targetClass.getContainingFile());
        processor.runWithoutProgress();
    }

    default ISourceMethodVisitor getSourceMethodVisitor() {
        ILanguageGenerator languageGenerator = getLanguageGenerator();
        if (languageGenerator == null) {
            return null;
        }
        languageGenerator.updateExistingNames(getExistingNames());
        return SourceMethodVisitorProvider
            .getNewInstance(getFlowCoverContainer(), languageGenerator,
                getSourceClass().getLanguage(), getConfig());
    }

    default FlowCoverContainer initFlowCoverContainer() {
        ISourceClassVisitor sourceClassVisitor = getSourceClassVisitor();
        if (sourceClassVisitor != null) {
            sourceClassVisitor.visitSourceClass(getSourceClass());
            return sourceClassVisitor.getFlowCoverContainer();
        }
        return null;
    }

    @Nullable
    static PsiClass findClass(Project project, String fqName) {
        GlobalSearchScope scope = GlobalSearchScope.allScope(project);
        return JavaPsiFacade.getInstance(project).findClass(fqName, scope);
    }

    default void addImport(PsiClass aClass, PsiClass targetClass) {
        PsiFile containingFile = targetClass.getContainingFile();
        if (containingFile instanceof GroovyFileBase) {
            ((GroovyFileBase) containingFile).addImportForClass(aClass);
        } else if (containingFile instanceof PsiJavaFile) {
            ImportUtils.addImportIfNeeded(aClass, containingFile);
        }
    }

    default void showErrorLater(final Project project, final String targetClassName) {
        ApplicationManager.getApplication()
            .invokeLater(() -> Messages.showErrorDialog(project,
                String.format("Cannot Create Class %s", targetClassName),
                "Failed to Create Class"));
    }

    default PsiClass createTestClass(CreateTestsDialog d,TestFramework framework,Project project,String superClassName){
        String languageName = framework.getLanguage().getID().toLowerCase(Locale.ROOT);
        if(languageName.equals("groovy")){
            GrTypeDefinition targetClass = createTestClassForGroovy(project,d);
            if (targetClass == null) {
                return null;
            }
            markClassAsTest(targetClass,project,superClassName);
            return targetClass;
        } else if(languageName.equals("java")){
            PsiClass targetClass = createTestClassForJava(d,framework);
            if (targetClass == null) {
                return null;
            }
            markClassAsTest(targetClass,project,superClassName);
            return targetClass;
        }
        return null;
    }

    @Nullable
    static PsiClass createTestClassForJava(CreateTestsDialog d, TestFramework testFrameworkDescriptor) {
        final FileTemplateDescriptor fileTemplateDescriptor = TestIntegrationUtils.MethodKind.TEST_CLASS.getFileTemplateDescriptor(testFrameworkDescriptor);
        final PsiDirectory targetDirectory = d.getTargetDirectory();

        final PsiPackage aPackage = JavaDirectoryService.getInstance().getPackage(targetDirectory);
        if (aPackage != null) {
            final GlobalSearchScope scope = GlobalSearchScopesCore.directoryScope(targetDirectory, false);
            final PsiClass[] classes = aPackage.findClassByShortName(d.getClassName(), scope);
            if (classes.length > 0) {
                if (!FileModificationService.getInstance().preparePsiElementForWrite(classes[0])) {
                    return null;
                }
                return classes[0];
            }
        }

        if (fileTemplateDescriptor != null) {
            final PsiClass classFromTemplate = createTestClassFromCodeTemplate(d, fileTemplateDescriptor, targetDirectory);
            if (classFromTemplate != null) {
                return classFromTemplate;
            }
        }

        return JavaDirectoryService.getInstance().createClass(targetDirectory, d.getClassName());
    }

    static PsiClass createTestClassFromCodeTemplate(final CreateTestsDialog d,
        final FileTemplateDescriptor fileTemplateDescriptor,
        final PsiDirectory targetDirectory) {
        final String templateName = fileTemplateDescriptor.getFileName();
        final FileTemplate fileTemplate = FileTemplateManager.getInstance(targetDirectory.getProject()).getCodeTemplate(templateName);
        final Properties defaultProperties = FileTemplateManager.getInstance(targetDirectory.getProject()).getDefaultProperties();
        Properties properties = new Properties(defaultProperties);
        properties.setProperty(FileTemplate.ATTRIBUTE_NAME, d.getClassName());
        final PsiClass targetClass = d.getTargetClass();
        if (targetClass != null && targetClass.isValid()) {
            properties.setProperty(FileTemplate.ATTRIBUTE_CLASS_NAME, targetClass.getQualifiedName());
        }
        try {
            final PsiElement psiElement = FileTemplateUtil
                .createFromTemplate(fileTemplate, templateName, properties, targetDirectory);
            if (psiElement instanceof PsiClass) {
                return (PsiClass)psiElement;
            }
            return null;
        }
        catch (Exception e) {
            return null;
        }
    }

    @Nullable
    static GrTypeDefinition createTestClassForGroovy(Project project, CreateTestsDialog d) {
        GrTypeDefinition targetClass = CreateClassActionBase.createClassByType(d.getTargetDirectory(),
            d.getClassName(),
            PsiManager.getInstance(project),
            null,
            GroovyTemplates.GROOVY_CLASS,
            true);
        return targetClass;
    }

    static void markClassAsTest(@NotNull GrTypeDefinition targetClass, @NotNull Project project, @Nullable String superClassName)
        throws IncorrectOperationException {
        if (superClassName == null) {
            return;
        }

        GroovyPsiElementFactory factory = GroovyPsiElementFactory.getInstance(project);

        PsiClass superClass = ITestGenerator.findClass(project, superClassName);
        GrCodeReferenceElement superClassRef;
        if (superClass != null) {
            superClassRef = factory.createCodeReferenceElementFromClass(superClass);
        } else {
            superClassRef = factory.createCodeReference(superClassName);
        }
        GrExtendsClause extendsClause = targetClass.getExtendsClause();
        if (extendsClause == null) {
            extendsClause = (GrExtendsClause) targetClass.addAfter(factory.createExtendsClause(), targetClass.getNameIdentifierGroovy());
        }

        extendsClause.add(superClassRef);
    }

    static void markClassAsTest(PsiClass targetClass, Project project, String superClassName) throws IncorrectOperationException {
        if (superClassName == null) return;
        final PsiReferenceList extendsList = targetClass.getExtendsList();
        if (extendsList == null) return;

        PsiElementFactory ef = JavaPsiFacade.getElementFactory(project);
        PsiJavaCodeReferenceElement superClassRef;

        PsiClass superClass = findClass(project, superClassName);
        if (superClass != null) {
            superClassRef = ef.createClassReferenceElement(superClass);
        }
        else {
            superClassRef = ef.createFQClassNameReferenceElement(superClassName, GlobalSearchScope.allScope(project));
        }
        final PsiJavaCodeReferenceElement[] referenceElements = extendsList.getReferenceElements();
        if (referenceElements.length == 0) {
            extendsList.add(superClassRef);
        } else {
            referenceElements[0].replace(superClassRef);
        }
    }

    @Nullable
    PsiElement generateTest(Project var1, CreateTestsDialog var2);

    void addTestMethods(Editor editor,
                        PsiClass targetClass,
                        TestFramework descriptor,
                        Collection<MemberInfo> methods);

    PsiClass getSourceClass();

    ILanguageGenerator getLanguageGenerator();

    ISourceClassVisitor getSourceClassVisitor();

    ITestConfiguration getConfig();

    Set<String> getExistingNames();

    TestFramework getTestFramework();

    FlowCoverContainer getFlowCoverContainer();
}
