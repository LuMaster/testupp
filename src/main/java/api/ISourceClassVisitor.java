package api;

import com.intellij.psi.PsiClass;
import data.structure.FlowCoverContainer;

import java.util.Set;

public interface ISourceClassVisitor
{
    void visitSourceClass(PsiClass psiClass);

    FlowCoverContainer getFlowCoverContainer();

    void updateExistingNames(Set<String> existingNames);

    Set<String> getRestrictedNames();
}
