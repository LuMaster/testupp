package api;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;

public interface IFlowCoverBranch extends ILanguageHolder{

    PsiElement getMock(PsiElement element);

    void addImport(PsiClass importClass);
}
