package api;

import com.intellij.openapi.project.Project;
import com.intellij.testIntegration.TestFramework;

public interface ITestConfiguration {

  boolean useSpy();

  boolean mockStatic();

  TestFramework getTestFramework();

  Project getProject();
}
