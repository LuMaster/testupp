package api;

import com.intellij.psi.JVMElementFactory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiType;
import com.intellij.psi.PsiVariable;
import java.util.Collection;
import java.util.List;

public interface ILanguageGenerator extends ILanguageHolder {

  //    ExtensionPointName<IMockGenerator> EP_NAME =
//            ExtensionPointName.create("com.enjoyable.testing.mockGenerator");
  JVMElementFactory getJVMElementFactory();

  PsiMethod addAnnotations(PsiMethod method);

  PsiElement staticMethodCall(String className, String methodName);

  PsiElement staticClassDeclaration(PsiType type);

  PsiElement staticClass(PsiType type);

  PsiElement createStatementFromPureText(String text);

  PsiElement initializeSpy(PsiType type, List<PsiElement> arguments);

  PsiElement initializeEmptyConstructor(PsiType type, String[] modifiers);

  PsiElement initializeMock(PsiVariable varName);

  PsiElement initializeMock(PsiType type);

  PsiElement initializeMockOfFinal(PsiVariable varName);

  PsiElement initializeMockOfFinal(PsiType type);

  PsiElement initializeSimpleValue(PsiType type, PsiVariable variable, String defaultValue);

  PsiElement initializeCollectionType(PsiVariable variable);

  PsiElement initializeSimpleValue(PsiType type, String defaultValue);

  PsiElement initializeMethodCall(PsiMethod methodToCall, String variableName,
      PsiElement... arguments);

  PsiElement createMockOutputCall(String initializer, String returnValue);

  PsiElement createNewExpression(PsiType type, List<PsiElement> arguments);

  PsiElement createAnyTypeArgument(PsiType type);

  PsiElement createAnyTypeArgument();

  PsiElement createSingleAnyNonNullArgument();

  void updateExistingNames(Collection<String> collection);

  Collection<String> getRestrictedNames();
}
