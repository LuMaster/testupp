package data.providers;

import api.ITestConfiguration;
import api.ITestGenerator;
import com.intellij.psi.PsiClass;
import com.intellij.testIntegration.TestFramework;
import generators.JavaMainTestGenerator;
import generators.spock.SpockTestGenerator;
import generators.testNG.TestNGTestGenerator;

public class TestCreatorProvider {

    public static synchronized ITestGenerator forLanguage(PsiClass srcClass, TestFramework framework, ITestConfiguration configuration) {
        String frameworkName = framework.getName().toLowerCase();
        switch (frameworkName) {
            case "spock":
                return new SpockTestGenerator(srcClass, framework,configuration);
            case "testng":
                return new TestNGTestGenerator(srcClass, framework,configuration);
            default:
                return new JavaMainTestGenerator();
        }
    }

}
