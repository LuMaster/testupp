package data.providers;

import api.ILanguageGenerator;
import api.ISourceMethodVisitor;
import api.ITestConfiguration;
import com.intellij.lang.Language;
import data.structure.FlowCoverContainer;
import generators.sourcevisitors.java.JavaMethodVisitor;

public class SourceMethodVisitorProvider
{

    public static synchronized ISourceMethodVisitor getNewInstance(FlowCoverContainer flowCoverContainer, ILanguageGenerator generator, Language language, ITestConfiguration config) {
        switch (language.getID().toLowerCase()) {
            case "java":
                return new JavaMethodVisitor(generator, flowCoverContainer,config);
            default:
                return null;
        }
    }

}
