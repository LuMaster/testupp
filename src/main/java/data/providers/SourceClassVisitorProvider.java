package data.providers;

import api.ILanguageGenerator;
import api.ISourceClassVisitor;
import api.ITestGenerator;
import com.intellij.psi.PsiClass;
import generators.sourcevisitors.java.JavaClassVisitor;

public class SourceClassVisitorProvider
{

    public static synchronized ISourceClassVisitor getInstance(ITestGenerator testGenerator,PsiClass psiClass, ILanguageGenerator languageGenerator) {
        switch (psiClass.getLanguage().getID().toLowerCase()) {
            case "java":
                return new JavaClassVisitor(languageGenerator,testGenerator);
            case "groovy":
                return null;
//                return new GroovyClassVisitor();
            default:
                return null;
        }
    }
}
