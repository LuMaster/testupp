package data.providers;

import api.ITestLanguage;
import com.intellij.openapi.project.Project;
import com.intellij.testIntegration.TestFramework;
import generators.spock.SpockTestLanguage;
import generators.testNG.TestNGTestLanguage;

public class TestLanguageDictionaryProvider {

  public static synchronized ITestLanguage getInstance(Project project, TestFramework framework) {
    switch (framework.getName().toLowerCase()) {
      case "spock":
        return new SpockTestLanguage(project, framework);
      case "testng":
        return new TestNGTestLanguage(project, framework);
      default:
        return null;
    }
  }
}
