package data.providers;

import api.ILanguageGenerator;
import com.intellij.openapi.project.Project;
import com.intellij.testIntegration.TestFramework;
import generators.spock.SpockTestLanguage;
import generators.spock.generators.SpockLanguageGenerator;
import generators.testNG.TestNGLanguageGenerator;
import generators.testNG.TestNGTestLanguage;

public class MockGeneratorProvider {

    public static synchronized ILanguageGenerator getInstance(Project project, TestFramework framework) {
        switch (framework.getName().toLowerCase()) {
            case "spock":
                return new SpockLanguageGenerator(project, framework,
                    new SpockTestLanguage(project, framework));
            case "testng":
              return new TestNGLanguageGenerator(project, framework,
                  new TestNGTestLanguage(project, framework));
            default:
                return null;
        }
    }


}
