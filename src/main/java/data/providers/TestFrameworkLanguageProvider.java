package data.providers;

import api.ITestLanguage;
import com.intellij.openapi.project.Project;
import com.intellij.testIntegration.TestFramework;
import generators.spock.SpockTestLanguage;

/**
 * TODO make as a service ? ??
 */
public class TestFrameworkLanguageProvider {

  public static synchronized ITestLanguage getInstance(
      Project project, TestFramework framework) {
    switch (framework.getName().toLowerCase()) {
      case "spock":
        return new SpockTestLanguage(project, framework);
    }
    return null;
  }

}
