package data.providers;

import com.intellij.jarRepository.JarRepositoryManager;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.*;
import com.intellij.openapi.roots.impl.libraries.LibraryEx;
import com.intellij.openapi.roots.libraries.Library;
import com.intellij.openapi.roots.libraries.LibraryTable;
import com.intellij.openapi.roots.libraries.ui.OrderRoot;
import com.intellij.openapi.roots.ui.configuration.ProjectStructureConfigurable;
import com.intellij.openapi.roots.ui.configuration.projectRoot.GlobalLibrariesConfigurable;
import com.intellij.openapi.roots.ui.configuration.projectRoot.ProjectLibrariesConfigurable;
import com.intellij.openapi.roots.ui.configuration.projectRoot.daemon.LibraryProjectStructureElement;
import com.intellij.openapi.roots.ui.configuration.projectRoot.daemon.ProjectStructureElement;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.idea.maven.utils.library.RepositoryLibraryProperties;
import org.jetbrains.idea.maven.utils.library.RepositoryUtils;
import org.jetbrains.jps.model.library.JpsMavenRepositoryLibraryDescriptor;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Stream;

public class LibraryProvider {

    private static String groupId = "org.spockframework";
    private static String artefactId = "spock-core";
    private static String version = "1.2-groovy-2.5";
    private static JpsMavenRepositoryLibraryDescriptor jpsMavenRepositoryLibraryDescriptor;
    private static RepositoryLibraryProperties libraryProperties;

    static {
        jpsMavenRepositoryLibraryDescriptor = new JpsMavenRepositoryLibraryDescriptor(groupId, artefactId, version, true);
//        libraryProperties = new RepositoryLibraryProperties(
//                jpsMavenRepositoryLibraryDescriptor.getGroupId(),
//                jpsMavenRepositoryLibraryDescriptor.getArtifactId(),
//                jpsMavenRepositoryLibraryDescriptor.getVersion(),
//                jpsMavenRepositoryLibraryDescriptor.isIncludeTransitiveDependencies());
    }

    public static void downloadLiblaryAndReplace(Project project) {
        ProjectStructureElement selectedElement = ProjectLibrariesConfigurable.getInstance(project).getSelectedElement();
        ProjectStructureElement selectedElement2 = GlobalLibrariesConfigurable.getInstance(project).getSelectedElement();
        if (!(selectedElement instanceof LibraryProjectStructureElement)) {
            return;
        }
        LibraryEx library = (LibraryEx) ((LibraryProjectStructureElement) selectedElement).getLibrary();
        boolean hasSources = RepositoryUtils.libraryHasSources(library);
        boolean hasJavadoc = RepositoryUtils.libraryHasJavaDocs(library);
        Collection<OrderRoot> orderRoots = JarRepositoryManager.loadDependenciesModal(project, libraryProperties, hasSources, hasJavadoc, null, null);
        Stream<File> downloadedFiles = orderRoots
                .stream()
                .filter(orderRoot -> orderRoot.getType() == OrderRootType.CLASSES)
                .map(it -> VfsUtilCore.virtualToIoFile(it.getFile()));
        if (downloadedFiles.toArray().length == 0) {
            if (Messages.showYesNoDialog("No files were downloaded. Do you want to try different coordinates?", "Failed to Download Library",
                    null) != Messages.YES) {
                return;
            }
            changeCoordinatesAndRetry(jpsMavenRepositoryLibraryDescriptor, library);
            return;
        }
        Stream<File> libraryFiles = Arrays.stream(library.getFiles(OrderRootType.CLASSES)).map(VfsUtilCore::virtualToIoFile);
        String[] annotationUrls = library.getUrls(AnnotationOrderRootType.getInstance());
        ProjectStructureConfigurable.getInstance(project);
        String name = library.getName();
    }

    private static void changeCoordinatesAndRetry(JpsMavenRepositoryLibraryDescriptor mavenCoordinates, LibraryEx library) {
    }


    private void attachDirBasedLibrary(@NotNull final Module module,
                                       @NotNull final String libName,
                                       @NotNull final String dir) {
        ApplicationManager.getApplication().runWriteAction(() ->
        {
            final ModuleRootManager rootManager = ModuleRootManager.getInstance(module);
            final String urlString = VirtualFileManager.constructUrl(LocalFileSystem.PROTOCOL, dir);
            final VirtualFile dirVirtualFile = VirtualFileManager.getInstance().findFileByUrl(urlString);
            if (dirVirtualFile != null) {
                final ModifiableRootModel modifiableModel = rootManager.getModifiableModel();
                final Library newLib = createDirLib(libName, dirVirtualFile, modifiableModel.getModuleLibraryTable());
                modifiableModel.commit();
            }
        });
    }


    private Library createDirLib(@NotNull final String libName,
                                 @NotNull final VirtualFile dirVirtualFile,
                                 @NotNull final LibraryTable table) {
        Library library = table.getLibraryByName(libName);
        if (library == null) {
            library = table.createLibrary(libName);

            Library.ModifiableModel libraryModel = library.getModifiableModel();
            libraryModel.addJarDirectory(dirVirtualFile, true, OrderRootType.CLASSES);
            libraryModel.addJarDirectory(dirVirtualFile, true, OrderRootType.SOURCES);
            libraryModel.addJarDirectory(dirVirtualFile, true, JavadocOrderRootType.getInstance());
            libraryModel.commit();

        }
        return library;
    }

    private void attachJarLibrary(@NotNull final Module module,
                                  @NotNull final String libName,
                                  @NotNull final String jarFilePath,
                                  @NotNull final String srcFilePath,
                                  @NotNull final String docFilePath) {
        ApplicationManager.getApplication().runWriteAction(() ->
        {
            final ModuleRootManager rootManager = ModuleRootManager.getInstance(module);

            final String clzUrlString = VirtualFileManager.constructUrl(JarFileSystem.PROTOCOL, jarFilePath) + JarFileSystem.JAR_SEPARATOR;
            final String srcUrlString = VirtualFileManager.constructUrl(JarFileSystem.PROTOCOL, srcFilePath) + JarFileSystem.JAR_SEPARATOR;
            final String docUrlString = VirtualFileManager.constructUrl(JarFileSystem.PROTOCOL, docFilePath) + JarFileSystem.JAR_SEPARATOR;

            final VirtualFile jarVirtualFile = VirtualFileManager.getInstance().findFileByUrl(clzUrlString);
            final VirtualFile srcVirtualFile = VirtualFileManager.getInstance().findFileByUrl(srcUrlString);
            final VirtualFile docVirtualFile = VirtualFileManager.getInstance().findFileByUrl(docUrlString);

            final ModifiableRootModel modifiableModel = rootManager.getModifiableModel();
            final Library newLib = createJarLib(libName, modifiableModel.getModuleLibraryTable(), jarVirtualFile, srcVirtualFile, docVirtualFile);
            modifiableModel.commit();
        });
    }


    private Library createJarLib(@NotNull final String libName,
                                 @NotNull final LibraryTable table,
                                 @Nullable final VirtualFile jarVirtualFile,
                                 @Nullable final VirtualFile srcVirtualFile,
                                 @Nullable final VirtualFile docVirtualFile) {
        Library library = table.getLibraryByName(libName);
        if (library == null) {
            library = table.createLibrary(libName);
            Library.ModifiableModel libraryModel = library.getModifiableModel();

            if (jarVirtualFile != null) {
                libraryModel.addRoot(jarVirtualFile, OrderRootType.CLASSES);
            }

            if (srcVirtualFile != null) {
                libraryModel.addRoot(srcVirtualFile, OrderRootType.SOURCES);
            }

            if (docVirtualFile != null) {
                libraryModel.addRoot(docVirtualFile, JavadocOrderRootType.getInstance());
            }

            libraryModel.commit();
        }
        return library;
    }

}
