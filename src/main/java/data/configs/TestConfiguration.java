package data.configs;

import api.ITestConfiguration;
import com.intellij.openapi.project.Project;
import com.intellij.testIntegration.TestFramework;

public class TestConfiguration implements ITestConfiguration {

    private final boolean useSpy;
    private final boolean mockStatic;
    private final TestFramework framework;
    private final Project project;

    private TestConfiguration(boolean useSpy, boolean mockStatic, TestFramework framework,
        Project project) {
        this.useSpy = useSpy;
        this.mockStatic = mockStatic;
        this.framework = framework;
        this.project = project;
    }

    @Override
    public boolean useSpy() {
        return useSpy;
    }

    @Override
    public boolean mockStatic() {
        return mockStatic;
    }

    @Override
    public TestFramework getTestFramework() {
        return framework;
    }

    @Override
    public Project getProject() {
        return project;
    }


    public static class Builder {

        private boolean useSpy;
        private boolean mockStatic;
        private TestFramework framework;
        private Project project;

        public Builder useSpy(boolean useSpy) {
            this.useSpy = useSpy;
            return this;
        }

        public Builder mockStatic(boolean mockStatic) {
            this.mockStatic = mockStatic;
            return this;
        }

        public Builder framework(TestFramework testFramework) {
            this.framework = testFramework;
            return this;
        }

        public Builder project(Project project) {
            this.project = project;
            return this;
        }

        public TestConfiguration build() {
            return new TestConfiguration(useSpy, mockStatic, framework, project);
        }

    }
}
