package data.structure.tests;

import api.ITestLanguage;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiElement;
import com.intellij.testIntegration.TestFramework;
import generators.TestSectionLabel;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

public class AbstractTest {

    protected Project project;

    protected String testName;

    protected TestFramework language;

    protected ITestLanguage dictionary;

    public Set<PsiElement> arguments;

    public Set<PsiElement> given;

    public Set<PsiElement> and;

    public Set<PsiElement> when;

    public Set<PsiElement> then;

    public Set<PsiElement> where;

    public Set<PsiElement> expect;


    protected AbstractTest() {
        testName = "";
        arguments = new LinkedHashSet<>();
        given = new LinkedHashSet<>();
        and = new LinkedHashSet<>();
        when = new LinkedHashSet<>();
        then = new LinkedHashSet<>();
        where = new LinkedHashSet<>();
    }

    public void addElementByLabel(PsiElement element, TestSectionLabel label) {
        if (label == null) {
            return;
        }
        switch (label) {
            case GIVEN:
                given.add(element);
                break;
            case AND:
                and.add(element);
                break;
            case WHEN:
                when.add(element);
                break;
            case THEN:
                then.add(element);
                break;
            case WHERE:
                where.add(element);
                break;
            default:
                break;
        }
    }

    protected String getBody(TestSectionLabel label, TestFramework language,
        Collection<PsiElement> partElements) {
        if (partElements == null || partElements.isEmpty()) {
            return "\n";
        }
        String lineSeparator = "\n";
        StringBuilder builder = new StringBuilder().append(lineSeparator);
        String labelName = label.getLabel(project, language);
        if (labelName != null) {
            builder.append(labelName);
        }
        for (PsiElement bodyElement : partElements) {
            builder.append(bodyElement.getText());
            builder.append(lineSeparator);
        }
        return builder.toString();
    }

    protected String processArguments() {
        StringBuilder result = new StringBuilder();
        for (PsiElement argument : arguments) {
            result.append(String.join(",", argument.getText()));
        }
        return result.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SpockTest)) {
            return false;
        }
        SpockTest spockTest = (SpockTest) o;
        return Objects.equals(testName, spockTest.testName)
            && Objects.equals(arguments, spockTest.arguments)
            && Objects.equals(given,
            spockTest.given)
            && Objects.equals(and, spockTest.and)
            && Objects.equals(when, spockTest.when)
            && Objects.equals(then, spockTest.then)
            && Objects.equals(where, spockTest.where);
    }

    public String getSectionBody(TestSectionLabel label) {
        switch (label) {
            case GIVEN:
                return getBody(label, language, given);
            case AND:
                return getBody(label, language, and);
            case WHEN:
                return getBody(label, language, when);
            case THEN:
                return getBody(label, language, then);
            case WHERE:
                return getBody(label, language, where);
            case EXPECT:
                return getBody(label, language, expect);
            default:
                return "";
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(testName, arguments, given, and, when, then);
    }

    public String createTestFromText(PsiDirectory directory) {
        String body = getSectionBody(TestSectionLabel.GIVEN)
            + getSectionBody(TestSectionLabel.AND)
            + getSectionBody(TestSectionLabel.WHEN)
            + getSectionBody(TestSectionLabel.WHERE)
            + getSectionBody(TestSectionLabel.THEN);
        return dictionary
            .getMethodTemplate(dictionary.getMethodTemplateTypes(), testName, processArguments(),
                body, directory);
    }
}
