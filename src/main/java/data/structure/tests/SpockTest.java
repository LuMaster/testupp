package data.structure.tests;

import api.ITestLanguage;
import com.intellij.openapi.project.Project;
import com.intellij.testIntegration.TestFramework;
import generators.TestSectionLabel;
import lombok.Getter;

@Getter
public class SpockTest extends AbstractTest {
    public SpockTest() {
        super();
    }

    public void withTestName(String name) {
        this.testName = name;
    }

    public void withFramework(TestFramework language) {
        this.language = language;
    }

    public void withDictionary(ITestLanguage dictionary) {
        this.dictionary = dictionary;
    }

    public void withProject(Project project) {
        this.project = project;
    }

    @Override
    public String toString() {
        String body = getSectionBody(TestSectionLabel.GIVEN)
            + getSectionBody(TestSectionLabel.AND)
            + getSectionBody(TestSectionLabel.WHEN)
            + getSectionBody(TestSectionLabel.WHERE)
            + getSectionBody(TestSectionLabel.THEN);
        return body;
    }
}
