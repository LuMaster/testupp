package data.structure;

import api.AbstractFlowCoverBranch;
import api.IFlowCoverBranch;
import application.ApplicationKeys;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiParameter;
import com.intellij.testIntegration.TestFramework;
import data.structure.elements.PsiConstructorCall;
import data.structure.elements.UnworkableElement;
import generators.TestSectionLabel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import util.ImportHelper;

@Getter
public class FlowCoverContainer extends AbstractFlowCoverBranch implements IFlowCoverBranch {

  private final Map<PsiElement, PsiElement> parameterToField = new HashMap<>();

  private final Set<PsiClass> classesToImport = new HashSet<>();

  /**
   * Constructor and map of mocked parameters
   */
  private final List<PsiConstructorCall> constructors = new ArrayList<>();

  private final Map<PsiElement, Set<PsiElement>> fieldsUsedInConstructor = new HashMap<>();

  private final List<FlowCoverSection> flowCoverSections;

  public FlowCoverContainer(TestFramework language) {
    super(language);
    flowCoverSections = new ArrayList<>();
  }

  public List<PsiElement> getMockedConstructorParameters(PsiElement constructor) {
    for (PsiConstructorCall psiConstructorCall : constructors) {
      if (psiConstructorCall.getConstructorCall().equals(constructor)) {
        return psiConstructorCall.getMockedParameters();
      }
    }
    return new ArrayList<>();
  }

  public void bindFieldWithConstructor(PsiElement constructor, PsiElement field) {
    Set<PsiElement> fieldsSet = this.fieldsUsedInConstructor.get(constructor);
    if (fieldsSet == null) {
      this.fieldsUsedInConstructor.put(constructor, new HashSet<>(Arrays.asList(field)));
    } else {
      fieldsSet.add(field);
    }
  }

  @Override
  public void addMock(PsiElement element, PsiElement mock, TestSectionLabel label) {
    if (mock != null && !(mock instanceof UnworkableElement)) {
      mock.putUserData(ApplicationKeys.SECTION_TYPE_KEY, label);
      classesToImport.addAll(ImportHelper.allImportsForElement(element));
      this.mapOfMocks.put(element, mock);
    }
  }

  public void addConstructor(PsiElement constructorCall, Map<PsiParameter, PsiElement> arguments) {
    if (constructorCall == null) {
      return;
    }
    constructorCall.putUserData(ApplicationKeys.SECTION_TYPE_KEY, TestSectionLabel.WHEN);
    PsiConstructorCall psiConstructorCall = new PsiConstructorCall();
    psiConstructorCall.addConstructorCall(constructorCall);
    psiConstructorCall.addParameters(arguments);
//        Map<PsiElement, PsiElement> psiElementPsiElementMap = constructors.get(constructorCall);
//        if (psiElementPsiElementMap == null) {
//            psiElementPsiElementMap = new HashMap<>();
//        }
//        psiElementPsiElementMap.putAll(arguments);
    constructors.add(psiConstructorCall);
  }

  /**
   * If constructors map is not empty it means we have processed constructor
   *
   * @return
   */
  public boolean isThereNoConstructor() {
    return constructors.isEmpty();
  }

  public List<PsiElement> getConstructorInitializers() {
    return constructors.stream().map(PsiConstructorCall::getConstructorCall)
        .filter(Objects::nonNull).collect(Collectors.toList());
  }

  /**
   * TODO refactor names!
   *
   * @param method
   */
  public void addMethod(FlowCoverSection method) {
    method.setParentContainer(this);
    flowCoverSections.add(method);
  }

  public void addCorrelationBetweenParameterAndField(PsiParameter parameter, PsiField field) {
    parameterToField.putIfAbsent(parameter, field);
  }

  public List<FlowCoverSection> getFlowCoverSections() {
    return flowCoverSections;
  }

  public PsiElement getMock(PsiElement key) {
    return mapOfMocks.get(key);
  }

  @Override
  public void addImport(PsiClass importClass) {
    classesToImport.add(importClass);
  }

  public List<PsiElement> getMockedFields() {
    return new ArrayList<>(mapOfMocks.values());
  }

  public List<PsiElement> getOriginalFields() {
    return new ArrayList<>(mapOfMocks.keySet());
  }
}
