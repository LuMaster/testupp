package data.structure;

import api.AbstractFlowCoverBranch;
import api.IFlowCoverBranch;
import api.ITestConfiguration;
import application.ApplicationKeys;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiVariable;
import data.providers.TestLanguageDictionaryProvider;
import data.structure.elements.UnworkableElement;
import data.structure.tests.SpockTest;
import generators.TestSectionLabel;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import util.ImportHelper;
import util.JavaUtil;
import util.VariableNameResolver;

@Getter
@Setter
public class FlowCoverSection extends AbstractFlowCoverBranch implements IFlowCoverBranch {

  /**
   * Mapped tested method Arguments List of mocks as values
   */
  private Set<PsiElement> statementsList = new LinkedHashSet<>();

  private Set<PsiField> usedFields = new HashSet<>();

  /**
   * Map of changed value of variable Reassignment Example: String a = "a"; a = "b"; a = "c"; At
   * least a is equal 'c' not 'a'
   */
  private Map<PsiElement, PsiElement> assignmentChangeMap = new LinkedHashMap<>();

  /**
   * Tested method to execute
   */
  private PsiElement methodCall;

  private PsiElement bestConstructorMatch;

  private FlowCoverContainer parentContainer;
  private ITestConfiguration configuration;

  public FlowCoverSection(ITestConfiguration configuration) {
    super(configuration.getTestFramework());
    this.configuration = configuration;
  }

  public void reassign(PsiElement original, PsiElement assignment) {
    if (original == null || assignment == null) {
      return;
    }
    assignmentChangeMap.put(original, assignment);
  }

  /**
   * If null then no change of reference of original variable
   *
   * @param original
   * @return
   */
  public PsiElement getChangedAssignment(PsiElement original) {
    return assignmentChangeMap.get(original);
  }

  public Map<PsiElement, PsiElement> getArguments() {
    return mapOfMocks;
  }

  public PsiElement getMethodCall() {
    return methodCall;
  }

  public FlowCoverContainer getParentContainer() {
    return parentContainer;
  }

  public void setMethodCall(PsiElement methodCall) {
    this.methodCall = methodCall;
  }

  public void addArgument(PsiVariable newArgument, PsiElement mockedEquivalent,
      TestSectionLabel label) {
    if (mockedEquivalent == null || mockedEquivalent instanceof UnworkableElement) {
      return;
    }
    mockedEquivalent.putUserData(ApplicationKeys.FUNCTION_IN_CODE, FunctionInCode.ARGUMENT);
    addMockWithImport(newArgument, mockedEquivalent, label);
  }

  public void addMock(PsiElement variable, PsiElement mock, TestSectionLabel label) {
    if (mock == null || variable == null || mock instanceof UnworkableElement) {
      return;
    }
    addMockWithImport(variable, mock, label);
  }

  public void addCall(PsiElement call, TestSectionLabel data) {
    if (JavaUtil.isAnyNull(call)) {
      return;
    }
    call.putUserData(ApplicationKeys.SECTION_TYPE_KEY, data);
    this.statementsList.add(call);
  }

  public void addStaticInit(PsiElement call, PsiClassType classType,
      TestSectionLabel data) {
    if (JavaUtil.isAnyNull(call, classType, data)) {
      return;
    }
    call.putUserData(ApplicationKeys.SECTION_TYPE_KEY, data);
    call.putUserData(ApplicationKeys.FUNCTION_IN_CODE, FunctionInCode.STATIC_INIT);
    addImport(classType.resolve());
    this.statementsList.add(call);
  }

  private void addMockWithImport(PsiElement newArgument, PsiElement mockedEquivalent,
      TestSectionLabel label) {
    mockedEquivalent.putUserData(ApplicationKeys.SECTION_TYPE_KEY, label);
    Set<PsiClass> psiClasses = ImportHelper.allImportsForElement(newArgument);
    for (PsiClass psiClass : psiClasses) {
      addImport(psiClass);
    }

    this.mapOfMocks.putIfAbsent(newArgument, mockedEquivalent);
  }

  public PsiElement getMock(PsiElement key) {
    if (key == null) {
      return null;
    }

    PsiElement changedAssignment = getChangedAssignment(key);
    if (changedAssignment != null) {
      key = changedAssignment;
      return getElementForChangedAssignment(key);
    }

    // if neither assignment nor declaration we should return field
    PsiElement mockFor = parentContainer.getMock(key);
    if (mockFor == null) {
      return this.mapOfMocks.get(key);
    }
    return mockFor;
  }

  @Override
  public void addImport(PsiClass importClass) {
    parentContainer.addImport(importClass);
  }

  @Nullable
  private PsiElement getElementForChangedAssignment(PsiElement key) {
    PsiElement mockFor = parentContainer.getMock(key);
    if (mockFor == null) {
      PsiElement mock = this.mapOfMocks.get(key);
      if (mock == null) {
        return null;
      }
      return mock;
    }
    return mockFor;
  }


  @Nullable
  public PsiElement getFirstConstructorInitializer() {
    if (parentContainer.getConstructorInitializers().size() < 1) {
      return null;
    }
    return parentContainer.getConstructorInitializers().get(0);
  }

  public Set<PsiElement> getArgumentMocks() {
    Set<PsiElement> result = new LinkedHashSet<>();
    for (PsiElement value : mapOfMocks.values()) {
      FunctionInCode functionInCode = getFunctionInCode(value);
      if (functionInCode != null && functionInCode.equals(FunctionInCode.ARGUMENT)) {
        result.add(value);
      }
    }
    return result;
  }

  public void addUsedField(PsiField variable) {
    usedFields.add(variable);
  }

  public void setParentContainer(FlowCoverContainer parentTest) {
    this.parentContainer = parentTest;
  }

  public Set<PsiElement> reduceAndGetStatements() {
//    Set<String> elementText = new HashSet<>();
//    for (Iterator<PsiElement> iterator = statementsList.iterator(); iterator.hasNext(); ) {
//      PsiElement psiElement = iterator.next();
//      if (elementText.contains(psiElement.getText())) {
//        iterator.remove();
//      } else {
//        elementText.add(psiElement.getText());
//      }
//    }
    return statementsList.stream().filter(JavaUtil.distinctByKey(PsiElement::getText)).collect(
        Collectors.toSet());
  }

  /**
   * TODO rewrite this to delivering test in language this section is made
   *
   * @return
   */
  public SpockTest convertToTest() {
    SpockTest spockTest = new SpockTest();
    List<PsiElement> mockedConstructorParameters = parentContainer
        .getMockedConstructorParameters(bestConstructorMatch);
    for (PsiElement mockedConstructorParameter : mockedConstructorParameters) {
      spockTest.addElementByLabel(mockedConstructorParameter,
          getSectionType(mockedConstructorParameter));
    }
    spockTest.withTestName(
        VariableNameResolver.suggestName(methodCall, getParentContainer().getRestrictedNames()));
    spockTest.withFramework(getTestFramework());
    spockTest.withDictionary(TestLanguageDictionaryProvider.getInstance(
        configuration.getProject(), getTestFramework()));
    spockTest.withProject(configuration.getProject());
    //TODO uncomment when setters will be done
//        for (PsiElement field : parentContainer.getMockedFields()) {
//            spockTest.addElementByLabel(field, getSectionType(field));
//        }
    if (bestConstructorMatch != null) {
      spockTest.addElementByLabel(bestConstructorMatch, getSectionType(bestConstructorMatch));
    }
    for (PsiElement value : new LinkedHashSet<>(mapOfMocks.values())) {
      spockTest.addElementByLabel(value, getSectionType(value));
    }
    for (PsiElement value : reduceAndGetStatements()) {
      spockTest.addElementByLabel(value, getSectionType(value));
    }
    if (methodCall != null) {
      spockTest.addElementByLabel(methodCall, getSectionType(methodCall));
    }
    return spockTest;
  }

  public PsiElement getBestConstructorMatch() {
    Map<PsiElement, Boolean> usageMap = new HashMap<>();

    long numberOfCorrect = 0;

    resetState(usageMap);

    Map<PsiElement, Set<PsiElement>> constructors = parentContainer.getFieldsUsedInConstructor();
    for (Map.Entry<PsiElement, Set<PsiElement>> mapEntry : constructors.entrySet()) {
      for (PsiElement psiElement : mapEntry.getValue()) {
        if (usedFields.contains(psiElement)) {
          usageMap.put(psiElement, true);
        }
      }
      if (numberOfCorrect < usageMap.values().stream().filter(t -> t).count()) {
        bestConstructorMatch = mapEntry.getKey();
        numberOfCorrect = usageMap.values().stream().filter(t -> t).count();
      }
      resetState(usageMap);
    }
    if (bestConstructorMatch == null) {
      bestConstructorMatch = getFirstConstructorInitializer();
      return bestConstructorMatch;
    }
    return bestConstructorMatch;
  }

  private void resetState(Map<PsiElement, Boolean> usageMap) {
    for (PsiField usedField : usedFields) {
      usageMap.put(usedField, false);
    }
  }
}
