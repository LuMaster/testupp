package data.structure.elements;

import application.ApplicationKeys;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiParameter;
import generators.TestSectionLabel;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.Getter;

@Getter
public class PsiConstructorCall extends AbstractPsiElement {

    private final Map<PsiParameter, PsiElement> parametersMocked = new LinkedHashMap<>();

    private PsiElement constructorCall;

    public PsiConstructorCall() {

    }

    public void addParameter(PsiParameter parameter, PsiElement mock) {
        mock.putUserData(ApplicationKeys.SECTION_TYPE_KEY, TestSectionLabel.GIVEN);
        parametersMocked.putIfAbsent(parameter, mock);
    }

    public void addParameters(Map<PsiParameter, PsiElement> arguments) {
        parametersMocked.putAll(arguments);
    }

    public List<PsiElement> getMockedParameters() {
        return parametersMocked.values().stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    public List<PsiElement> getOriginalParameters() {
        return new ArrayList<>(parametersMocked.keySet());
    }

    public void addConstructorCall(PsiElement call) {
        constructorCall = call;
    }
}
