package data.structure.elements;

import api.ILanguageGenerator;
import application.ApplicationKeys;
import com.intellij.psi.PsiElement;
import data.structure.FunctionInCode;
import generators.TestSectionLabel;
import java.util.HashSet;
import java.util.Set;
import org.jetbrains.annotations.Nullable;

public class Spy extends PsiConstructorCall {

  private final ILanguageGenerator generator;
  private final Set<PsiElement> methodCalls;
  private final PsiElement constructorCall;

  private Spy(ILanguageGenerator generator, PsiElement constructorCall,
      Set<PsiElement> methodCalls) {
    this.generator = generator;
    this.constructorCall = constructorCall;
    this.methodCalls = methodCalls;
  }

  @Nullable
  public PsiElement toElement() {
    if (constructorCall == null) {
      return null;
    }
    PsiElement statementFromPureText = generator.createStatementFromPureText(toString());
    TestSectionLabel sectionLabel = constructorCall.getUserData(ApplicationKeys.SECTION_TYPE_KEY);
    FunctionInCode functionInCode = constructorCall.getUserData(ApplicationKeys.FUNCTION_IN_CODE);
    statementFromPureText.putUserData(ApplicationKeys.SECTION_TYPE_KEY, sectionLabel);
    statementFromPureText.putUserData(ApplicationKeys.FUNCTION_IN_CODE, functionInCode);
    return statementFromPureText;
  }

  @Override
  public String toString() {
    String callText = constructorCall.getText();
    if (methodCalls.isEmpty()) {
      return callText;
    }
    StringBuilder strBuilder = new StringBuilder();
    for (PsiElement methodCall : methodCalls) {
      strBuilder.append(methodCall.getText()).append("\n");
    }
    return callText + "{\n" + strBuilder.toString() + "\n}";
  }

  public static class Builder {

    private final Set<PsiElement> methodCalls = new HashSet<>();
    private PsiElement constructorCall;
    private ILanguageGenerator generator;

    public Builder addGenerator(ILanguageGenerator generator) {
      this.generator = generator;
      return this;
    }

    public Builder addCall(PsiElement element) {
      methodCalls.add(element);
      return this;
    }

    public Builder addConstructor(PsiElement constructorCall) {
      this.constructorCall = constructorCall;
      return this;
    }

    public Spy build() {
      return new Spy(generator, constructorCall, methodCalls);
    }

  }
}
