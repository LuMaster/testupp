package data.structure;

public enum FunctionInCode {

    ARGUMENT, LOCAL_VARIABLE, FIELD, CONSTRUCTOR_ARGUMENT,SPY_METHOD_CALL, STATIC_INIT

}
