package util;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiType;
import com.intellij.psi.PsiVariable;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import com.intellij.psi.codeStyle.SuggestedNameInfo;
import com.intellij.psi.codeStyle.VariableKind;
import com.intellij.psi.util.PsiTypesUtil;
import com.intellij.refactoring.rename.NameSuggestionProvider;
import java.util.Set;

/**
 * Generate name for scope and given psi element
 */
public class VariableNameResolver {

    public static String suggestName(PsiElement methodToCall, Set<String> restrictedNames) {
        String varName = null;
        SuggestedNameInfo suggestedNames = null;
        if (methodToCall == null) {
            varName = iterateThrough(restrictedNames, "test");
            return varName;
        }
        for (NameSuggestionProvider nameSuggestionProvider : NameSuggestionProvider.EP_NAME
            .getExtensions()) {
            suggestedNames = nameSuggestionProvider
                .getSuggestedNames(methodToCall, methodToCall.getContext(), restrictedNames);
        }
        if (suggestedNames != null) {
            varName = suggestedNames.names[0];
        }
        if (varName == null || varName.isEmpty()) {
            // TODO get create name based on methodToCall name
            varName = iterateThrough(restrictedNames, "test");
        }
        return varName;
    }

    public static String resolveTypeName(PsiType type, Set<String> restrictedNames) {
        PsiClass psiClass = PsiTypesUtil.getPsiClass(type);
        if (psiClass != null) {
            JavaCodeStyleManager codeStyleManager = JavaCodeStyleManager.getInstance(psiClass.getProject());
            String suggestedNameInfo = codeStyleManager.suggestCompiledParameterName(type);
            return iterateThrough(restrictedNames, suggestedNameInfo);
        }
        return iterateThrough(restrictedNames, type.getPresentableText());
    }

    public static String resolveVariableName(PsiVariable variable, Set<String> restrictedNames) {
        JavaCodeStyleManager codeStyleManager = JavaCodeStyleManager.getInstance(variable.getProject());
        VariableKind variableKind = codeStyleManager.getVariableKind(variable);
        final SuggestedNameInfo nameInfo = codeStyleManager.suggestVariableName(variableKind, null, variable.getInitializer(), variable.getType());

        for (String name : nameInfo.names) {
            return iterateThrough(restrictedNames, name);
        }

        return iterateThrough(restrictedNames, variable.getName());
    }

    private static String iterateThrough(Set<String> restrictedNames, String name) {
        if (!restrictedNames.contains(name)) {
            restrictedNames.add(name);
            return name;
        }
        int i = 1;
        //noinspection StatementWithEmptyBody
        for (; restrictedNames.contains(name + i); i++) ;
        String finalName = name + i;
        restrictedNames.add(finalName);
        return finalName;
    }

}
