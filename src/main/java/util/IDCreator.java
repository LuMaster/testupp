package util;

import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiUtil;

public class IDCreator {

    public static String createID(PsiElement element) {
        return element.getProject().getName() + "#" + PsiUtil.getTypeByPsiElement(element) + "#" + element.getText().trim();
    }

}
