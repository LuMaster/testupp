package util;

import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiModifier;

public class PsiUtil {

  public static boolean isStatic(PsiMethod method) {
    return method.getModifierList().hasModifierProperty(PsiModifier.STATIC);
  }



}
