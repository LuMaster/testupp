package util;

import application.AvailableTestingFrameworks;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.util.Comparing;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.testIntegration.TestFramework;
import com.intellij.util.SmartList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import org.jetbrains.annotations.Nullable;

public class SettingsUtil {


  public static VirtualFile getTestDirectory(Project myProject, String dir) {
    for (VirtualFile root : ProjectRootManager.getInstance(myProject)
        .getContentSourceRoots()) {
      if (root.getPath().equals(dir)) {
        return root;
      }
    }
    return null;
  }

  @Nullable
  public static TestFramework getTestFramework(String name) {
    final List<TestFramework> descriptors = new SmartList<>(
        TestFramework.EXTENSION_NAME.getExtensionList());
    descriptors.sort((d1, d2) -> Comparing.compare(d1.getName(), d2.getName()));

    for (final TestFramework framework : descriptors) {
      if (!framework.getName().equalsIgnoreCase(name)) {
        continue;
      }
      return framework;
    }
    return null;
  }

  public static void actionOnPositiveFramework(Consumer<TestFramework> consumer) {
    final List<TestFramework> descriptors = new SmartList<>(
        TestFramework.EXTENSION_NAME.getExtensionList());
    descriptors.sort((d1, d2) -> Comparing.compare(d1.getName(), d2.getName()));
    List<String> availableFrameworks = Arrays.stream(AvailableTestingFrameworks.values())
        .map(value -> value.name().toLowerCase(
            Locale.ROOT))
        .collect(Collectors.toList());

    for (final TestFramework framework : descriptors) {
      if (!availableFrameworks.contains(framework.getName().toLowerCase(Locale.ROOT))) {
        continue;
      }
      consumer.accept(framework);
    }
  }

}
