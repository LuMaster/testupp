package util;

import application.TestUppLogger;
import com.intellij.ide.DataManager;
import com.intellij.ide.projectView.ProjectView;
import com.intellij.ide.projectView.impl.AbstractProjectViewPane;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.application.ApplicationInfo;
import com.intellij.openapi.editor.Caret;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.roots.FileIndexFacade;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowId;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.openapi.wm.WindowManager;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiReference;
import com.intellij.psi.search.PsiSearchHelper;
import com.intellij.psi.search.SearchScope;
import com.intellij.psi.search.searches.ReferencesSearch;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.Query;
import java.awt.Window;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.concurrency.Promise;

public class IDEAUtil {

  /**
   * @param element
   * @param parentClass
   * @return array of PsiReference[] usages
   */
  public static PsiReference[] getUsagesOf(PsiElement element,
      Class<? extends PsiElement> parentClass) {
    PsiElement parentOfType = PsiTreeUtil.getParentOfType(element, parentClass);
    SearchScope useScope;
    if (parentOfType != null) {
      useScope = PsiSearchHelper.getInstance(element.getProject()).getUseScope(parentOfType);
      Query<PsiReference> search = ReferencesSearch.search(element, useScope);
      return search.toArray(new PsiReference[0]);
    }
    return new PsiReference[0];
  }

  public static PsiElement[] collectElementsFromOf(PsiElement element, Class classToSearch) {
    //        PsiSearchHelper.getInstance(element.getProject());
    //         ReferencesSearch.search(variable).findAll()
    return PsiTreeUtil.collectElements(element, e -> e.getClass().isInstance(classToSearch));
  }

  public static PsiFile getActivePsiFile() {
    return getDataContextFromFocus().getData(CommonDataKeys.PSI_FILE);
    //        PsiFile[] result = new PsiFile[1];
    //        getPromiseAsync().onSuccess(dataContext -> {
    //            result[0] = CommonDataKeys.PSI_FILE.getData(dataContext);
    //        });
    //        if (result[0] == null) {
    //            return getFileFromActiveEditor();
    //        }
    //        return result[0];
  }

  public static PsiFile getFileFromActiveEditor() {
    Editor activeEditor = getActiveEditor();
    if (activeEditor == null) {
      return null;
    }
    Project project = activeEditor.getProject();
    if (project == null) {
      return null;
    }
    return PsiManager.getInstance(project)
        .findFile(Objects.requireNonNull(getVirtualFileFromActiveEditor()));
  }

  public static VirtualFile getVirtualFileFromActiveEditor() {
    Editor activeEditor = getActiveEditor();
    if (activeEditor == null) {
      return null;
    }
    return FileDocumentManager.getInstance().getFile(activeEditor.getDocument());
  }

  @Nullable
  public static Editor getActiveEditor() {
    return getDataContextFromFocus().getData(CommonDataKeys.EDITOR);
    //        Editor[] result = new Editor[1];
    //        getPromiseAsync().onSuccess(dataContext -> {
    //            result[0] = CommonDataKeys.EDITOR.getData(dataContext);
    //        });
    //        return result[0];
  }

  public static ToolWindow getProjectToolWindow() {
    return ToolWindowManager.getInstance(Objects.requireNonNull(getActiveProject()))
        .getToolWindow(ToolWindowId.PROJECT_VIEW);
  }

  public static AbstractProjectViewPane getCurrentProjectViewPane() {
    return ProjectView.getInstance(Objects.requireNonNull(getActiveProject()))
        .getCurrentProjectViewPane();
  }

  @Nullable
  public static Caret getCurrentCaret()
      throws TimeoutException, ExecutionException {
    return getDataContextFromFocus().getData(CommonDataKeys.CARET);
    //        Caret[] result = new Caret[1];
    //        getPromiseAsync().onSuccess(dataContext -> {
    //            result[0] = CommonDataKeys.CARET.getData(dataContext);
    //        });
    //        return result[0];
  }

  @Nullable
  public static PsiElement getCurrentPsiElement() {
    return getDataContextFromFocus().getData(CommonDataKeys.PSI_ELEMENT);
    //        PsiElement[] result = new PsiElement[1];
    //        getPromiseAsync().onSuccess(dataContext -> {
    //            result[0] = CommonDataKeys.PSI_ELEMENT.getData(dataContext);
    //        });
    //        return result[0];
  }

  public static Promise<DataContext> getPromiseAsync() {
    return DataManager.getInstance().getDataContextFromFocusAsync();
  }

  @Nullable
  public static Project getCurrentProject() {
    DataContext promiseContext = getDataContextFromFocus();
    return (Project) promiseContext.getData(CommonDataKeys.PROJECT.getName());
  }

  public static Object getDataAsync(String onSuccessKey) {
    AtomicReference<Object> result = new AtomicReference<>();
    IDEAUtil.getPromiseAsync()
        .onSuccess(dataContext -> result.set(dataContext.getData(onSuccessKey)))
        .onError(throwable -> TestUppLogger.getLogger().error(throwable));
    return result.get();
  }

  @NotNull
  private static DataContext getDataContextFromFocus() {
    return DataManager.getInstance().getDataContext();
  }

  /**
   * An alternative way to get current project
   *
   * @return an active project or null if not found
   */
  public static Project getActiveProject() {
    for (Project project : ProjectManager.getInstance().getOpenProjects()) {
      Window window = WindowManager.getInstance().suggestParentWindow(project);
      if ((window != null) && window.isActive()) {
        return project;
      }
    }
    return null;
  }

  @Nullable
  public static Module getModule(VirtualFile virtualFile) {
    Module moduleForFile;
    Project[] openProjects = ProjectManager.getInstance().getOpenProjects();
    for (Project openProject : openProjects) {
      moduleForFile = FileIndexFacade.getInstance(openProject).getModuleForFile(virtualFile);
      if (moduleForFile != null) {
        return moduleForFile;
      }
    }
    return null;
  }

  public static String applicationVersion() {
    ApplicationInfo instance = ApplicationInfo.getInstance();
    return instance.getFullVersion();
  }

}
