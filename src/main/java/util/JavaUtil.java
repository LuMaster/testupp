package util;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

public class JavaUtil {

  public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
    Set<Object> seen = ConcurrentHashMap.newKeySet();
    return t -> seen.add(keyExtractor.apply(t));
  }

  public static boolean isAnyNull(Object... objects) {
    boolean result = false;
    for (Object object : objects) {
      if (object == null) {
        result = true;
      }
    }
    return result;
  }

}
