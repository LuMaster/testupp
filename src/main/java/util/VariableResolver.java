package util;

import com.intellij.psi.*;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import com.intellij.psi.codeStyle.SuggestedNameInfo;
import com.intellij.psi.codeStyle.VariableKind;
import com.intellij.psi.util.PsiTypesUtil;
import com.intellij.psi.util.TypeConversionUtil;
import com.intellij.refactoring.util.RefactoringUtil;
import com.siyeh.ig.psiutils.CollectionUtils;
import gnu.trove.THashMap;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * Will find mock for needed variable from StubbedTestMethod
 * TODO this is java version - make provider for other languages
 *
 * @see RefactoringUtil#suggestUniqueVariableName(String, PsiElement, PsiField)
 */
public class VariableResolver {

    /**
     * PsiTypesUtil check for more
     */
    @NonNls
    private static final Map<String, String> ourUnboxedTypes = new THashMap<>();
    @NonNls
    private static final Map<String, String> ourBoxedTypes = new THashMap<>();

    @NonNls
    private static final Map<String, String> s_interfaceForCollection = new HashMap<>();

    /**
     * Taken from PsiKeyword.class
     */
    static String ABSTRACT = "abstract";
    static String ASSERT = "assert";
    static String BOOLEAN = "boolean";
    static String BREAK = "break";
    static String BYTE = "byte";
    static String CASE = "case";
    static String CATCH = "catch";
    static String CHAR = "char";
    static String CLASS = "class";
    static String CONST = "const";
    static String CONTINUE = "continue";
    static String DEFAULT = "default";
    static String DO = "do";
    static String DOUBLE = "double";
    static String ELSE = "else";
    static String ENUM = "enum";
    static String EXTENDS = "extends";
    static String FINAL = "final";
    static String FINALLY = "finally";
    static String FLOAT = "float";
    static String FOR = "for";
    static String GOTO = "goto";
    static String IF = "if";
    static String IMPLEMENTS = "implements";
    static String IMPORT = "import";
    static String INSTANCEOF = "instanceof";
    static String INT = "int";
    static String INTERFACE = "interface";
    static String LONG = "long";
    static String NATIVE = "native";
    static String NEW = "new";
    static String PACKAGE = "package";
    static String PRIVATE = "private";
    static String PROTECTED = "protected";
    static String PUBLIC = "public";
    static String RETURN = "return";
    static String SHORT = "short";
    static String STATIC = "static";
    static String STRICTFP = "strictfp";
    static String SUPER = "super";
    static String SWITCH = "switch";
    static String SYNCHRONIZED = "synchronized";
    static String THIS = "this";
    static String THROW = "throw";
    static String THROWS = "throws";
    static String TRANSIENT = "transient";
    static String TRY = "try";
    static String VOID = "void";
    static String VOLATILE = "volatile";
    static String WHILE = "while";

    static String TRUE = "true";
    static String FALSE = "false";
    static String NULL = "null";

    static String OPEN = "open";
    static String MODULE = "module";
    static String REQUIRES = "requires";
    static String EXPORTS = "exports";
    static String OPENS = "opens";
    static String USES = "uses";
    static String PROVIDES = "provides";
    static String TRANSITIVE = "transitive";
    static String TO = "to";
    static String WITH = "with";

    static String VAR = "var";

    static {
        ourUnboxedTypes.put(CommonClassNames.JAVA_LANG_BOOLEAN, "boolean");
        ourUnboxedTypes.put(CommonClassNames.JAVA_LANG_BYTE, "byte");
        ourUnboxedTypes.put(CommonClassNames.JAVA_LANG_SHORT, "short");
        ourUnboxedTypes.put(CommonClassNames.JAVA_LANG_INTEGER, "int");
        ourUnboxedTypes.put(CommonClassNames.JAVA_LANG_LONG, "long");
        ourUnboxedTypes.put(CommonClassNames.JAVA_LANG_FLOAT, "float");
        ourUnboxedTypes.put(CommonClassNames.JAVA_LANG_DOUBLE, "double");
        ourUnboxedTypes.put(CommonClassNames.JAVA_LANG_CHARACTER, "char");
        ourUnboxedTypes.put(CommonClassNames.JAVA_LANG_STRING, "String");

        ourBoxedTypes.put("boolean", CommonClassNames.JAVA_LANG_BOOLEAN);
        ourBoxedTypes.put("byte", CommonClassNames.JAVA_LANG_BYTE);
        ourBoxedTypes.put("short", CommonClassNames.JAVA_LANG_SHORT);
        ourBoxedTypes.put("int", CommonClassNames.JAVA_LANG_INTEGER);
        ourBoxedTypes.put("long", CommonClassNames.JAVA_LANG_LONG);
        ourBoxedTypes.put("float", CommonClassNames.JAVA_LANG_FLOAT);
        ourBoxedTypes.put("double", CommonClassNames.JAVA_LANG_DOUBLE);
        ourBoxedTypes.put("char", CommonClassNames.JAVA_LANG_CHARACTER);

        s_interfaceForCollection.put("ArrayList", "List");
        s_interfaceForCollection.put("EnumMap", "Map");
        s_interfaceForCollection.put("EnumSet", "Set");
        s_interfaceForCollection.put("HashMap", "Map");
        s_interfaceForCollection.put("HashSet", "Set");
        s_interfaceForCollection.put("Hashtable", "Map");
        s_interfaceForCollection.put("IdentityHashMap", "Map");
        s_interfaceForCollection.put("LinkedHashMap", "Map");
        s_interfaceForCollection.put("LinkedHashSet", "Set");
        s_interfaceForCollection.put("LinkedList", "List");
        s_interfaceForCollection.put("PriorityQueue", "Queue");
        s_interfaceForCollection.put("TreeMap", "Map");
        s_interfaceForCollection.put("TreeSet", "SortedSet");
        s_interfaceForCollection.put("Vector", "List");
        s_interfaceForCollection.put("WeakHashMap", "Map");
        s_interfaceForCollection.put("java.util.ArrayList", CommonClassNames.JAVA_UTIL_LIST);
        s_interfaceForCollection.put("java.util.EnumMap", CommonClassNames.JAVA_UTIL_MAP);
        s_interfaceForCollection.put("java.util.EnumSet", CommonClassNames.JAVA_UTIL_SET);
        s_interfaceForCollection.put("java.util.HashMap", CommonClassNames.JAVA_UTIL_MAP);
        s_interfaceForCollection.put("java.util.HashSet", CommonClassNames.JAVA_UTIL_SET);
        s_interfaceForCollection.put("java.util.Hashtable", CommonClassNames.JAVA_UTIL_MAP);
        s_interfaceForCollection.put("java.util.IdentityHashMap", CommonClassNames.JAVA_UTIL_MAP);
        s_interfaceForCollection.put("java.util.LinkedHashMap", CommonClassNames.JAVA_UTIL_MAP);
        s_interfaceForCollection.put("java.util.LinkedHashSet", CommonClassNames.JAVA_UTIL_SET);
        s_interfaceForCollection.put("java.util.LinkedList", CommonClassNames.JAVA_UTIL_LIST);
        s_interfaceForCollection.put("java.util.PriorityQueue", "java.util.Queue");
        s_interfaceForCollection.put("java.util.TreeMap", CommonClassNames.JAVA_UTIL_MAP);
        s_interfaceForCollection.put("java.util.TreeSet", CommonClassNames.JAVA_UTIL_SET);
        s_interfaceForCollection.put("java.util.Vector", CommonClassNames.JAVA_UTIL_LIST);
        s_interfaceForCollection.put("java.util.WeakHashMap", CommonClassNames.JAVA_UTIL_MAP);
        s_interfaceForCollection.put("com.sun.java.util.collections.HashSet", "com.sun.java.util.collections.Set");
        s_interfaceForCollection.put("com.sun.java.util.collections.TreeSet", "com.sun.java.util.collections.Set");
        s_interfaceForCollection.put("com.sun.java.util.collections.Vector", "com.sun.java.util.collections.List");
        s_interfaceForCollection.put("com.sun.java.util.collections.ArrayList", "com.sun.java.util.collections.List");
        s_interfaceForCollection.put("com.sun.java.util.collections.LinkedList", "com.sun.java.util.collections.List");
        s_interfaceForCollection.put("com.sun.java.util.collections.TreeMap", "com.sun.java.util.collections.Map");
        s_interfaceForCollection.put("com.sun.java.util.collections.HashMap", "com.sun.java.util.collections.Map");
        s_interfaceForCollection.put("com.sun.java.util.collections.Hashtable", "com.sun.java.util.collections.Map");
    }

    public static Object getDefaultValue(PsiType type) {
        if (isNullType(type)) {
            return null;
        }
        switch (type.getCanonicalText()) {
            case "boolean":
                return false;
            case "byte":
                return (byte) 0;
            case "char":
                return '\0';
            case "short":
                return (short) 0;
            case "int":
                return 0;
            case "long":
                return 0L;
            case "float":
                return 0F;
            case "double":
                return 0D;
            case CommonClassNames.JAVA_LANG_BOOLEAN:
                return false;
            case CommonClassNames.JAVA_LANG_BYTE:
                return (byte) 0;
            case CommonClassNames.JAVA_LANG_SHORT:
                return (short) 0;
            case CommonClassNames.JAVA_LANG_INTEGER:
                return 0;
            case CommonClassNames.JAVA_LANG_LONG:
                return 0L;
            case CommonClassNames.JAVA_LANG_FLOAT:
                return 0F;
            case CommonClassNames.JAVA_LANG_DOUBLE:
                return 0D;
            case CommonClassNames.JAVA_LANG_CHARACTER:
                return '\0';
            case CommonClassNames.JAVA_LANG_STRING:
                return "\"\"";
        }
        if (type instanceof PsiArrayType) {
            int count = type.getArrayDimensions() - 1;
            PsiType componentType = type.getDeepComponentType();

            if (componentType instanceof PsiClassType) {
                final PsiClassType classType = (PsiClassType) componentType;
                if (classType.resolve() instanceof PsiTypeParameter) {
                    return PsiKeyword.NULL;
                }
            }

            PsiType erasedComponentType = TypeConversionUtil.erasure(componentType);
            StringBuilder buffer = new StringBuilder();
            buffer.append(PsiKeyword.NEW);
            buffer.append(" ");
            buffer.append(erasedComponentType.getCanonicalText());
            buffer.append("[0]");
            for (int i = 0; i < count; i++) {
                buffer.append("[]");
            }
            return buffer.toString();
        }
        PsiType rawType = type instanceof PsiClassType ? ((PsiClassType) type).rawType() : null;
        if (rawType != null && rawType.equalsToText(CommonClassNames.JAVA_UTIL_OPTIONAL)) {
            return CommonClassNames.JAVA_UTIL_OPTIONAL + ".empty()";
        }
        if (CollectionUtils.isCollectionClassOrInterface(type)) {
            String fqName = type.getCanonicalText().replaceAll("<.*", "");
            Optional<Map.Entry<String, String>> first = s_interfaceForCollection.entrySet().stream().filter(t -> t.getValue().equals(fqName)).findFirst();
            String collectionName;
            if (first.isPresent()) {
                Map.Entry<String, String> stringStringEntry = first.get();
                collectionName = stringStringEntry.getKey();
            } else {
                collectionName = fqName;
            }
            return "new " + collectionName + "<>()";
        }

        return PsiKeyword.NULL;
    }

    @Contract(value = "null -> false", pure = true)
    public static boolean isNullType(PsiType type) {
        return PsiType.NULL.equals(type);
    }

    public static Set<String> getRestrictedNamesForVariables() {
        HashSet<String> restrictedNames = new HashSet<>(ourUnboxedTypes.values());
        restrictedNames.add(ABSTRACT);
        restrictedNames.add(ASSERT);
        restrictedNames.add(BOOLEAN);
        restrictedNames.add(BREAK);
        restrictedNames.add(BYTE);
        restrictedNames.add(CASE);
        restrictedNames.add(CATCH);
        restrictedNames.add(CHAR);
        restrictedNames.add(CLASS);
        restrictedNames.add(CONST);
        restrictedNames.add(CONTINUE);
        restrictedNames.add(DEFAULT);
        restrictedNames.add(DO);
        restrictedNames.add(DOUBLE);
        restrictedNames.add(ELSE);
        restrictedNames.add(ENUM);
        restrictedNames.add(EXTENDS);
        restrictedNames.add(FINAL);
        restrictedNames.add(FINALLY);
        restrictedNames.add(FLOAT);
        restrictedNames.add(FOR);
        restrictedNames.add(GOTO);
        restrictedNames.add(IF);
        restrictedNames.add(IMPLEMENTS);
        restrictedNames.add(IMPORT);
        restrictedNames.add(INSTANCEOF);
        restrictedNames.add(INT);
        restrictedNames.add(INTERFACE);
        restrictedNames.add(LONG);
        restrictedNames.add(NATIVE);
        restrictedNames.add(NEW);
        restrictedNames.add(PACKAGE);
        restrictedNames.add(PRIVATE);
        restrictedNames.add(PROTECTED);
        restrictedNames.add(PUBLIC);
        restrictedNames.add(RETURN);
        restrictedNames.add(SHORT);
        restrictedNames.add(STATIC);
        restrictedNames.add(STRICTFP);
        restrictedNames.add(SUPER);
        restrictedNames.add(SWITCH);
        restrictedNames.add(SYNCHRONIZED);
        restrictedNames.add(THIS);
        restrictedNames.add(THROW);
        restrictedNames.add(THROWS);
        restrictedNames.add(TRANSIENT);
        restrictedNames.add(TRY);
        restrictedNames.add(VOID);
        restrictedNames.add(VOLATILE);
        restrictedNames.add(WHILE);

        restrictedNames.add(TRUE);
        restrictedNames.add(FALSE);
        restrictedNames.add(NULL);

        restrictedNames.add(OPEN);
        restrictedNames.add(MODULE);
        restrictedNames.add(REQUIRES);
        restrictedNames.add(EXPORTS);
        restrictedNames.add(OPENS);
        restrictedNames.add(USES);
        restrictedNames.add(PROVIDES);
        restrictedNames.add(TRANSITIVE);
        restrictedNames.add(TO);
        restrictedNames.add(WITH);

        restrictedNames.add(VAR);

        return restrictedNames;
    }


}
