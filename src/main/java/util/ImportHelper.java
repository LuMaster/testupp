package util;

import com.intellij.psi.*;
import com.intellij.psi.impl.source.PsiImmediateClassType;
import com.intellij.psi.util.PsiTypesUtil;
import com.intellij.psi.util.PsiUtil;
import com.siyeh.ig.psiutils.CollectionUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class ImportHelper {

    /**
     * //TODO replace getting simple import class with advanced resolver even for array generics!!!
     *
     * @param element
     * @return
     */
    public static Set<PsiClass> allImportsForElement(PsiElement element) {
        Set<PsiClass> classList = new HashSet<>();
        PsiType typeByPsiElement = PsiUtil.getTypeByPsiElement(element);
        // TODO get generic from list type and resolve to class (may be some more examples not only variable!!)
        addWildcard(classList, GenericsUtil.eliminateWildcards(typeByPsiElement));
        addMethodCall(classList, element);
        addClass(classList, typeByPsiElement);
        return classList;
    }

    private static void addMethodCall(Set<PsiClass> classList,PsiElement element) {
        if (element instanceof PsiMethodCallExpression) {
            PsiMethodCallExpression psiElement = (PsiMethodCallExpression) element;
            PsiReferenceExpression methodExpression = psiElement.getMethodExpression();
            PsiElement qualifier = methodExpression.getQualifier();
            if (qualifier instanceof PsiClassObjectAccessExpression) {
                PsiClassObjectAccessExpression methodExpressionQualifier = (PsiClassObjectAccessExpression) qualifier;
                PsiType psiType = methodExpressionQualifier.getType();
                addWildcard(classList,GenericsUtil.eliminateWildcards(psiType));
                addClass(classList,psiType);
            }
            addWildcard(classList,GenericsUtil.eliminateWildcards(methodExpression.getType()));
            addClass(classList,methodExpression.getType());
        }
    }

    private static void addClass(Set<PsiClass> classList, PsiType typeByPsiElement) {
        PsiClass psiClass = PsiTypesUtil.getPsiClass(typeByPsiElement);
        classList.add(psiClass);
    }

    private static void addWildcard(Set<PsiClass> classList, PsiType eliminateWildcards) {
        if (eliminateWildcards instanceof PsiImmediateClassType) {
            PsiImmediateClassType psiType = (PsiImmediateClassType) eliminateWildcards;
            Collection<PsiType> values = psiType.resolveGenerics().getSubstitutor().getSubstitutionMap().values();
            for (PsiType value : values) {
                classList.add(PsiTypesUtil.getPsiClass(value));
            }
        }
    }

}
