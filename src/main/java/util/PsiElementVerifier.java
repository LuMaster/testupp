package util;

import com.intellij.psi.*;
import com.siyeh.ig.psiutils.CollectionUtils;

public class PsiElementVerifier {

    public static boolean canBeMocked(PsiElement element) {
        if (element instanceof PsiVariable) {
            return canVariableBeMocked((PsiVariable) element);
        }
        return false;
    }

    private static boolean canVariableBeMocked(PsiVariable variable) {
        PsiExpression initializer = variable.getInitializer();
        if (initializer == null) {
            return false;
        }
        PsiType initializerType = initializer.getType();
        if (initializerType == null || initializerType.equals(PsiType.NULL)) {
            return false;
        }
        if (initializer instanceof PsiNewExpression || initializer instanceof PsiReferenceExpression) {
            return false;
        }
        if (initializer instanceof PsiMethodCallExpression) {
            PsiReferenceExpression methodExpression = ((PsiMethodCallExpression) initializer).getMethodExpression();
            PsiElement methodExpressionQualifier = methodExpression.getQualifier();
            if (methodExpressionQualifier instanceof PsiReferenceExpression) {
                PsiType type = ((PsiReferenceExpression) methodExpressionQualifier).getType();
                if (CollectionUtils.isCollectionClassOrInterface(type)) {
                    return false;
                }
            }
        }
        return true;
    }


}
