package util;

import application.TestUppLogger;
import com.intellij.psi.PsiIdentifier;
import org.jetbrains.plugins.groovy.lang.psi.GroovyPsiElement;
import org.jetbrains.plugins.groovy.lang.psi.api.statements.GrVariable;
import org.jetbrains.plugins.groovy.lang.psi.api.statements.GrVariableDeclaration;

public class GroovyUtil {

  public static PsiIdentifier getIdentifierFromGr(GroovyPsiElement element) {
    if (element instanceof GrVariableDeclaration) {
      GrVariable[] variables = ((GrVariableDeclaration) element).getVariables();
      if (variables.length > 1) {
        TestUppLogger.getLogger()
            .warn("More than one variable in initialization : " + element.getText());
      }
      if (variables.length > 0) {
        return variables[0].getNameIdentifier();
      }
    }
    return null;
  }


}
