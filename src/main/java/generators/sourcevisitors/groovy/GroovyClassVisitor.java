package generators.sourcevisitors.groovy;

import api.ISourceClassVisitor;
import com.intellij.psi.PsiClass;
import data.structure.FlowCoverContainer;
import java.util.Set;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.plugins.groovy.lang.psi.GroovyRecursiveElementVisitor;
import org.jetbrains.plugins.groovy.lang.psi.api.statements.typedef.GrClassDefinition;
import org.jetbrains.plugins.groovy.lang.psi.api.statements.typedef.members.GrMethod;

public class GroovyClassVisitor extends GroovyRecursiveElementVisitor implements ISourceClassVisitor
{

    @Override
    public void visitMethod(@NotNull GrMethod method)
    {
        if (!method.isConstructor()) {
            return;
        }

        super.visitMethod(method);
    }

    @Override
    public void visitSourceClass(PsiClass psiClass)
    {
        visitClassDefinition((GrClassDefinition)psiClass);
    }

    @Override
    public FlowCoverContainer getFlowCoverContainer()
    {
        return null;
    }

    @Override
    public void updateExistingNames(Set<String> existingNames) {
        // TODO
    }

    @Override
    public Set<String> getRestrictedNames() {
        return null;
    }
}
