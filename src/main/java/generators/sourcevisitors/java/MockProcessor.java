package generators.sourcevisitors.java;

import api.ILanguageGenerator;
import api.ITestConfiguration;
import application.ApplicationKeys;
import application.TestUppLogger;
import application.TriConsumer;
import com.intellij.psi.GenericsUtil;
import com.intellij.psi.PsiCapturedWildcardType;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiExpression;
import com.intellij.psi.PsiIdentifier;
import com.intellij.psi.PsiKeyword;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiMethodCallExpression;
import com.intellij.psi.PsiModifier;
import com.intellij.psi.PsiReference;
import com.intellij.psi.PsiReferenceExpression;
import com.intellij.psi.PsiType;
import com.intellij.psi.PsiVariable;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.util.PsiTypesUtil;
import com.siyeh.ig.psiutils.CollectionUtils;
import data.structure.FlowCoverSection;
import data.structure.FunctionInCode;
import data.structure.elements.UnworkableElement;
import generators.TestSectionLabel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.plugins.groovy.lang.psi.GroovyPsiElement;
import util.GroovyUtil;
import util.VariableResolver;

public class MockProcessor {

  private final ILanguageGenerator generator;
  private final ITestConfiguration configuration;

  public MockProcessor(ILanguageGenerator generator, ITestConfiguration config) {
    this.generator = generator;
    this.configuration = config;
  }

  public void mockStaticMethodCall(FlowCoverSection coverSection,
      PsiMethodCallExpression callExpression, PsiMethod resolveMethod) {
    // TODO Fix statically imported methods
    PsiReferenceExpression methodExpression = callExpression.getMethodExpression();
    PsiClass containingClass = resolveMethod.getContainingClass();
    if (containingClass == null) {
      TestUppLogger.getLogger().error("No class found for : " + resolveMethod.getName());
    } else {
      PsiClassType classType = PsiTypesUtil.getClassType(containingClass);
      PsiElement mock = getOrCreateMock(coverSection, methodExpression);
      PsiElement staticClass = generator.staticClass(classType);
      coverSection.addStaticInit(staticClass, classType, TestSectionLabel.GIVEN);
      if (!(mock instanceof UnworkableElement)) {
        PsiMethodCallExpression copy = (PsiMethodCallExpression) callExpression.copy();
        replaceArgumentsInCallWithAny(copy);
        if (copy.getMethodExpression().getQualifier() == null) {
          PsiElement psiElement =  generator
              .staticMethodCall(containingClass.getQualifiedName(), copy.getText());
          addMockCall(coverSection,psiElement,mock,TestSectionLabel.AND, null);
        } else {
          addMockCall(coverSection, copy, mock, TestSectionLabel.AND, null);
        }
      }
    }
  }

  /**
   * !!! Fixme we generate multiple return mocks for one method call which is not possible to
   * execute (the true returned value is always last mocked) example : {
   * <p> element.multiply(2) >> iMoney1 </p>
   * <p> element.multiply(2) >> iMoney2 </p>
   * <p>
   * }
   */
  public void mockQualifiedMethodInvocation(FlowCoverSection coverSection,
      PsiElement mockToBeReturned, PsiMethodCallExpression callExpression, PsiElement qualifier) {
    if (qualifier != null) {
      processMethodCallWithQualifier(coverSection, mockToBeReturned, callExpression, qualifier);
    } else {
      if (configuration.useSpy()) {
        PsiMethod psiMethod = callExpression.resolveMethod();
        PsiMethod firstParent = (PsiMethod) PsiTreeUtil
            .findFirstParent(callExpression, t -> t instanceof PsiMethod);
        if (psiMethod == null || firstParent == null) {
          return;
        }
        PsiClass callExpressionClass = psiMethod.getContainingClass();
        PsiClass callExpressionContext = firstParent.getContainingClass();
        if (callExpressionClass != null && callExpressionClass.equals(callExpressionContext)) {
          // Their source class is the same so add method call
          PsiMethodCallExpression copy = (PsiMethodCallExpression) callExpression.copy();
          replaceArgumentsInCallWithAny(copy);
          PsiElement mock = getOrCreateMock(coverSection, mockToBeReturned);
          PsiVariable mockVariable = PsiTreeUtil.getChildOfType(mock, PsiVariable.class);
          if (mockVariable != null) {
            addMockCall(coverSection, copy, mockVariable, null, FunctionInCode.SPY_METHOD_CALL);
          } else {
            addMockCall(coverSection, copy, mock, null, FunctionInCode.SPY_METHOD_CALL);
          }
        }
      }
    }
  }


  private void replaceArgumentsInCallWithAny(PsiMethodCallExpression callExpression) {
    for (PsiExpression expression : callExpression.getArgumentList().getExpressions()) {
      PsiElement anyTypeArgument = generator.createAnyTypeArgument();
      if (anyTypeArgument == null) {
        return;
      }
      expression.replace(anyTypeArgument);
    }
  }

  private void processMethodCallWithQualifier(FlowCoverSection coverSection,
      PsiElement returnMockForIt, PsiMethodCallExpression callExpression,
      @NonNull PsiElement qualifier) {
    PsiReference reference = qualifier.getReference();
    if (reference != null) {
      processQualifierWithReference(coverSection, returnMockForIt, callExpression, reference);
    } else {
      processNoReferenceMethodCall(coverSection, callExpression, qualifier);
    }
  }

  private void processQualifierWithReference(FlowCoverSection coverSection,
      PsiElement returnMockForIt, PsiMethodCallExpression callExpression, PsiReference reference) {
    PsiElement resolve = reference.resolve();
    PsiElement mockFor = coverSection.getMock(resolve);
    if (mockFor != null) {
      processMethodCall(coverSection, returnMockForIt, callExpression, mockFor);
    } else {
      TestUppLogger.getLogger().debug("No mock for " + reference.getElement().getText());
    }
  }

  /**
   * @param coverSection
   * @param callExpression
   * @param qualifier
   */
  private void processNoReferenceMethodCall(FlowCoverSection coverSection,
      PsiMethodCallExpression callExpression, @NonNull PsiElement qualifier) {
        /* Chain call or ??
        Reference is pointing at non existing thing like : method call (there cant be an instance of method call) */

    List<PsiMethodCallExpression> chainedCallMethods = new ArrayList<>();
    chainedCallMethods.add(callExpression);
    fillChainedMethodCalls(qualifier, chainedCallMethods);
    Collections.reverse(chainedCallMethods);
    for (PsiMethodCallExpression chainedCallMethod : new LinkedHashSet<>(chainedCallMethods)) {
      PsiElement initializeMock = mockType(chainedCallMethod.getType());
      coverSection.addMock(chainedCallMethod, initializeMock, TestSectionLabel.GIVEN);
      PsiExpression qualifierExpression = chainedCallMethod.getMethodExpression()
          .getQualifierExpression();
      if (qualifierExpression != null) {
        PsiReference reference = qualifierExpression.getReference();
        if (reference != null) {
          processQualifierWithReference(coverSection, chainedCallMethod, chainedCallMethod,
              reference);
        } else {
          PsiElement qualifierMock = mockType(qualifierExpression.getType());
          coverSection.addMock(qualifierExpression, qualifierMock, TestSectionLabel.GIVEN);
        }
      }
      processMethodCall(coverSection, chainedCallMethod, chainedCallMethod,
          coverSection.getMock(qualifierExpression));
    }
    // TODO go no qualifier mocking
  }

  private void processMethodCall(FlowCoverSection coverSection, PsiElement mockToBeReturned,
      PsiMethodCallExpression callExpression, PsiElement mockForQualifier) {
    PsiElement mock = null;
    PsiVariable childOfType = PsiTreeUtil.getChildOfType(mockForQualifier, PsiVariable.class);
    if (childOfType != null && !CollectionUtils
        .isCollectionClassOrInterface(childOfType.getType())) {
      PsiIdentifier nameIdentifier = childOfType.getNameIdentifier();
      PsiMethodCallExpression copy = (PsiMethodCallExpression) callExpression.copy();
      PsiElement psiElement = copy.getMethodExpression().getQualifier();
      if (psiElement != null && nameIdentifier != null) {

        psiElement.replace(nameIdentifier);

        // replace argument list from original names to mocked names
        // TODO maybe in the future we can add option to decide if we want any objects or specific mocks
        for (PsiExpression expression : copy.getArgumentList().getExpressions()) {
          expression.replace(generator.createAnyTypeArgument());
        }
        mock = getOrCreateMock(coverSection, mockToBeReturned);
        PsiVariable mockVariable = PsiTreeUtil.getChildOfType(mock, PsiVariable.class);
        if (mockVariable != null) {
          addMockCall(coverSection, copy, mockVariable, TestSectionLabel.AND, null);
        } else {
          addMockCall(coverSection, copy, mock, TestSectionLabel.AND, null);
        }
      }
    }
  }

  private void addMockCall(FlowCoverSection coverSection, PsiElement copy,
      PsiElement mock, TestSectionLabel sectionLabel, FunctionInCode function) {
    PsiIdentifier identifier;
    if (mock instanceof GroovyPsiElement) {
      identifier = GroovyUtil.getIdentifierFromGr((GroovyPsiElement) mock);
    } else {
      identifier = PsiTreeUtil.getChildOfType(mock, PsiIdentifier.class);
    }
    if (identifier != null) {
      PsiElement mockOutputCall = generator.createMockOutputCall(copy.getText(),
          identifier.getText());
      mockOutputCall.putUserData(ApplicationKeys.FUNCTION_IN_CODE, function);
      coverSection.addCall(mockOutputCall, sectionLabel);
    }
  }

  private void addMockCall(FlowCoverSection coverSection, PsiMethodCallExpression copy,
      PsiElement mock, TestSectionLabel sectionLabel, FunctionInCode function) {
    PsiIdentifier identifier;
    if (mock instanceof GroovyPsiElement) {
      identifier = GroovyUtil.getIdentifierFromGr((GroovyPsiElement) mock);
    } else {
      identifier = PsiTreeUtil.getChildOfType(mock, PsiIdentifier.class);
    }
    if (identifier != null) {
      PsiElement mockOutputCall = generator.createMockOutputCall(copy.getText(),
          identifier.getText());
      mockOutputCall.putUserData(ApplicationKeys.FUNCTION_IN_CODE, function);
      coverSection.addCall(mockOutputCall, sectionLabel);
    }
  }

  private void addMockCall(FlowCoverSection coverSection, PsiMethodCallExpression copy,
      PsiVariable mockVariable, TestSectionLabel sectionLabel, FunctionInCode function) {
    PsiIdentifier mockVariableNameIdentifier = mockVariable.getNameIdentifier();
    if (mockVariableNameIdentifier != null) {
      PsiElement mockOutputCall = generator.createMockOutputCall(copy.getText(),
          mockVariableNameIdentifier.getText());
      mockOutputCall.putUserData(ApplicationKeys.FUNCTION_IN_CODE, function);
      coverSection.addCall(mockOutputCall, sectionLabel);
    }
  }

  public PsiElement getOrCreateMock(FlowCoverSection coverSection, PsiElement mockToBeReturned) {
    PsiElement result = null;
    PsiElement sectionMockFor = coverSection.getMock(mockToBeReturned);
    if (sectionMockFor == null) {
      if (mockToBeReturned instanceof PsiVariable) {
        result = mockVariable((PsiVariable) mockToBeReturned);
        coverSection.addMock(mockToBeReturned, result, TestSectionLabel.GIVEN);
      } else if (mockToBeReturned instanceof PsiReferenceExpression) {
        PsiReferenceExpression toBeReturned = (PsiReferenceExpression) mockToBeReturned;
        result = mockType(toBeReturned.getType());
        coverSection.addMock(toBeReturned, result, TestSectionLabel.GIVEN);
      } else if (mockToBeReturned instanceof PsiMethodCallExpression) {
        result = mockType(((PsiMethodCallExpression) mockToBeReturned).getType());
        coverSection.addMock(mockToBeReturned, result, TestSectionLabel.GIVEN);
      }
    } else {
      result = sectionMockFor;
    }
    return result;
  }

  public PsiElement mockMethodCallExpression(PsiMethodCallExpression callExpression, PsiType type,
      TriConsumer<PsiElement, PsiElement, TestSectionLabel> addAction) {
    PsiElement mockedEquivalent = createSimpleMock(null, type);
    addAction.accept(callExpression, mockedEquivalent, TestSectionLabel.GIVEN);
    return mockedEquivalent;
  }


  public void mockVariableType(PsiVariable variable, PsiType type,
      TriConsumer<PsiVariable, PsiElement, TestSectionLabel> addAction) {
    PsiElement mockedEquivalent = createSimpleMock(variable, type);
    addAction.accept(variable, mockedEquivalent, TestSectionLabel.GIVEN);
  }

  public PsiElement mockType(PsiType type) {
    return createSimpleMock(null, type);
  }

  public PsiElement mockVariable(PsiVariable variable) {
    return createSimpleMock(variable, variable.getType());
  }

  public void mockVariable(PsiVariable variable,
      TriConsumer<PsiElement, PsiElement, TestSectionLabel> addAction) {
    PsiElement simpleMock = createSimpleMock(variable, variable.getType());
    addAction.accept(variable, simpleMock, TestSectionLabel.GIVEN);
  }

  public PsiElement createSimpleMock(PsiVariable variable, PsiType type) {
    if (PsiType.VOID.equals(type)) {
      return new UnworkableElement();
    }
    PsiElement mockedEquivalent;
    if (type instanceof PsiCapturedWildcardType) {
      type = GenericsUtil.getVariableTypeByExpressionType(type);
    }
    Object defaultValue = VariableResolver.getDefaultValue(type);
    if (defaultValue != null && !defaultValue.equals(PsiKeyword.NULL)) {
      if (variable == null) {
        mockedEquivalent = generator.initializeSimpleValue(type, getDefaultValue(defaultValue));
      } else {
        mockedEquivalent = generator
            .initializeSimpleValue(type, variable, getDefaultValue(defaultValue));
      }
    } else {
      PsiClass psiClass = PsiTypesUtil.getPsiClass(type);
      if (psiClass != null && !psiClass.hasModifierProperty(PsiModifier.FINAL)) {
        if (variable == null) {
          mockedEquivalent = generator.initializeMock(type);
        } else {
          mockedEquivalent = generator.initializeMock(variable);
        }
      } else {
        if (variable == null) {
          mockedEquivalent = generator.initializeMockOfFinal(type);
        } else {
          mockedEquivalent = generator.initializeMockOfFinal(variable);
        }
      }
    }
    return mockedEquivalent;
  }

  @Nullable
  private String getDefaultValue(Object defaultValue) {
    return defaultValue != null ? defaultValue.toString() : null;
  }

  private void fillChainedMethodCalls(PsiElement qualifier,
      List<PsiMethodCallExpression> chainedCallMethods) {
    boolean stopCondition = true;
    while (stopCondition) {
      if (qualifier instanceof PsiMethodCallExpression) {
        PsiMethodCallExpression methodCallExpression = (PsiMethodCallExpression) qualifier;
        chainedCallMethods.add(methodCallExpression);
        PsiReferenceExpression methodExpression = methodCallExpression.getMethodExpression();
        qualifier = methodExpression.getQualifier();
      } else {
        stopCondition = false;
      }
    }
  }
}
