package generators.sourcevisitors.java;

import api.ILanguageGenerator;
import api.ISourceMethodVisitor;
import api.ITestConfiguration;
import application.ApplicationKeys;
import application.TriConsumer;
import com.intellij.psi.JavaRecursiveElementVisitor;
import com.intellij.psi.PsiAssignmentExpression;
import com.intellij.psi.PsiDeclarationStatement;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiExpression;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiForeachStatement;
import com.intellij.psi.PsiIdentifier;
import com.intellij.psi.PsiLocalVariable;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiMethodCallExpression;
import com.intellij.psi.PsiParameter;
import com.intellij.psi.PsiReferenceExpression;
import com.intellij.psi.PsiType;
import com.intellij.psi.PsiVariable;
import com.intellij.psi.util.TypeConversionUtil;
import com.siyeh.ig.psiutils.CollectionUtils;
import com.siyeh.ig.psiutils.VariableAccessUtils;
import data.structure.FlowCoverContainer;
import data.structure.FlowCoverSection;
import data.structure.FunctionInCode;
import data.structure.elements.PsiConstructorCall;
import data.structure.elements.Spy;
import generators.TestSectionLabel;
import java.util.Set;
import org.jetbrains.annotations.NotNull;
import util.PsiElementVerifier;
import util.PsiUtil;

public class JavaMethodVisitor extends JavaRecursiveElementVisitor implements ISourceMethodVisitor {

  private FlowCoverSection flowCoverSection;

  private final ITestConfiguration config;

  private final ILanguageGenerator generator;

  private final MockProcessor processor;

  public JavaMethodVisitor(ILanguageGenerator generator, FlowCoverContainer flowCoverContainer,
      ITestConfiguration config) {
    this.generator = generator;
    flowCoverSection = new FlowCoverSection(config);
    this.config = config;
    flowCoverContainer.addMethod(flowCoverSection);
    processor = new MockProcessor(generator, config);
  }

  @Override
  public void visitMethod(@NotNull PsiMethod method) {
    PsiParameter[] parameters = method.getParameterList().getParameters();
    TriConsumer<PsiVariable, PsiElement, TestSectionLabel> addArgument = flowCoverSection::addArgument;
    for (PsiParameter parameter : parameters) {
      PsiType type = parameter.getType();
      processor.mockVariableType(parameter, type, addArgument);
    }
    super.visitMethod(method);
  }

  @Override
  public void visitDeclarationStatement(@NotNull PsiDeclarationStatement statement) {
    for (PsiElement declaredElement : statement.getDeclaredElements()) {
      if (declaredElement instanceof PsiVariable) {
        PsiVariable psiVariable = (PsiVariable) declaredElement;
        if (VariableAccessUtils.variableIsUsed(psiVariable, statement.getContext())
            || VariableAccessUtils
            .variableIsPassedAsMethodArgument(psiVariable, statement.getContext())) {
          PsiExpression initializer = psiVariable.getInitializer();
          if (initializer instanceof PsiReferenceExpression) {
            PsiElement resolve = ((PsiReferenceExpression) initializer).resolve();
            PsiElement mockFor = flowCoverSection.getMock(resolve);
            if (mockFor == null) {
              PsiElement mockVariable = processor.mockVariable(psiVariable);
              flowCoverSection.addMock(declaredElement, mockVariable, TestSectionLabel.GIVEN);
            } else {
              flowCoverSection.addMock(declaredElement, mockFor, TestSectionLabel.GIVEN);
            }
          }
        }
      }
    }
    super.visitDeclarationStatement(statement);
  }

  @Override
  public void visitLocalVariable(PsiLocalVariable variable) {
    super.visitLocalVariable(variable);
  }


  @Override
  public void visitMethodCallExpression(PsiMethodCallExpression expression) {
    PsiElement qualifier = expression.getMethodExpression().getQualifier();
    PsiMethod resolveMethod = expression.resolveMethod();
    if (resolveMethod != null && PsiUtil.isStatic(resolveMethod)) {
      if (config.mockStatic()) {
        processor.mockStaticMethodCall(flowCoverSection, expression, resolveMethod);
      }
    } else {
      processor.mockQualifiedMethodInvocation(flowCoverSection, expression, expression, qualifier);
    }
    super.visitMethodCallExpression(expression);
  }

  @Override
  public void visitAssignmentExpression(PsiAssignmentExpression expression) {
    PsiExpression lExpression = expression.getLExpression();
    PsiExpression rExpression = expression.getRExpression();

    boolean lSideHasMock = false;
    /**
     * What else can be on the left side ?
     */
    PsiVariable lSideResolved = null;

    if (lExpression instanceof PsiReferenceExpression) {
      PsiElement resolve = ((PsiReferenceExpression) lExpression).resolve();
      if (resolve instanceof PsiVariable) {
        lSideResolved = (PsiVariable) resolve;
        PsiElement mockFor = flowCoverSection.getMock(lSideResolved);
        // If null variable must have been initialized as null previously eg. Object obj = null; for which we do not create mock
        if (mockFor != null) {
          lSideHasMock = true;
        }

      }
    }

    if (rExpression instanceof PsiReferenceExpression) {
      PsiElement resolve = ((PsiReferenceExpression) rExpression).resolve();
      if (resolve instanceof PsiVariable) {
        PsiVariable variable = (PsiVariable) resolve;
        PsiElement mockFor = flowCoverSection.getMock(variable);
        if (mockFor != null) {
          if (lSideHasMock) {
            flowCoverSection.reassign(lSideResolved, variable);
          }
        }
      }
    }

    if (rExpression instanceof PsiMethodCallExpression) {
      PsiReferenceExpression methodExpression = ((PsiMethodCallExpression) rExpression)
          .getMethodExpression();
      PsiElement methodExpressionQualifier = methodExpression.getQualifier();
      if (methodExpressionQualifier instanceof PsiMethodCallExpression) {
        PsiMethodCallExpression expressionQualifier = (PsiMethodCallExpression) methodExpressionQualifier;
        PsiReferenceExpression referenceExpression = expressionQualifier.getMethodExpression();
        if (CollectionUtils.isCollectionClassOrInterface(referenceExpression.getType())) {
          super.visitAssignmentExpression(expression);
          return;
        }
      }
      PsiElement psiElement = processor
          .mockMethodCallExpression((PsiMethodCallExpression) rExpression, rExpression.getType(),
              flowCoverSection::addMock);
      flowCoverSection.addMock(lSideResolved, psiElement, TestSectionLabel.GIVEN);
      flowCoverSection.reassign(lSideResolved, rExpression);
    }

    // If something is from PsiReferenceExpression can we do something about it ?
    // TODO Suggest to use setter using PropertyUtilBase.getSetter
//                    List<PsiMethod> setters = PropertyUtilBase.getSetters(Objects.requireNonNull(PsiTreeUtil.getParentOfType(psiExpression,
//                                                                                                                             PsiClass.class)),
//                                                                          Objects.requireNonNull(psiExpression.getReferenceName()));
    super.visitAssignmentExpression(expression);
  }

  @Override
  public void visitReferenceExpression(PsiReferenceExpression expression) {
    PsiElement resolve = expression.resolve();
    if (resolve instanceof PsiField) {
      if (flowCoverSection.getParentContainer().getOriginalFields().contains(resolve)) {
        flowCoverSection.addUsedField(((PsiField) resolve));
      }
    }
    super.visitReferenceExpression(expression);
  }

  @Override
  public void visitVariable(PsiVariable variable) {
    PsiExpression initializer = variable.getInitializer();
    if (!PsiElementVerifier.canBeMocked(variable)) {
      return;
    }
    PsiElement mockedVariable = processor.mockVariable(variable);
    flowCoverSection.addMock(variable, mockedVariable, TestSectionLabel.GIVEN);
    flowCoverSection.addMock(initializer, mockedVariable, TestSectionLabel.GIVEN);
    super.visitVariable(variable);
  }

  @Override
  public void visitForeachStatement(PsiForeachStatement statement) {
    PsiExpression iteratedValue = statement.getIteratedValue();
    if (iteratedValue == null) {
      return;
    }
    PsiType type = iteratedValue.getType();
    if (TypeConversionUtil.isPrimitiveAndNotNull(type)) {
      //Do sth
      // String defaultValueOfType = PsiTypesUtil.getDefaultValueOfType(type);
    }
    super.visitForeachStatement(statement);
  }

  @Override
  public void collect(PsiMethod method) {
    visitMethod(method);
    // Must be at the end because only now we know about all data inside resolved method
    setMethodCall(method);
  }

  /**
   * Create better setting method call with knowledge of variable that should be injected into
   * method
   *
   * @param method
   */
  private void setMethodCall(PsiMethod method) {
    PsiElement bestConstructorMatch = flowCoverSection.getBestConstructorMatch();
    Set<PsiElement> firstArgumentMocks = flowCoverSection.getArgumentMocks();

    if (config.useSpy()) {
      Spy.Builder builder = new Spy.Builder();
      for (PsiElement spyRealCall : flowCoverSection.reduceAndGetStatements()) {
        FunctionInCode functionInCode = flowCoverSection.getFunctionInCode(spyRealCall);
        if (FunctionInCode.SPY_METHOD_CALL.equals(functionInCode)) {
          builder.addCall(spyRealCall);
        }
      }
      Spy spy = builder.addConstructor(bestConstructorMatch).addGenerator(generator).build();
      PsiElement bestSpy = spy.toElement();
      for (PsiConstructorCall constructor : flowCoverSection.getParentContainer()
          .getConstructors()) {
        if (constructor.getConstructorCall().equals(bestConstructorMatch)) {
          spy.addParameters(constructor.getParametersMocked());
          spy.addConstructorCall(bestSpy);
          flowCoverSection.getParentContainer().getConstructors().add(spy);
          break;
        }
      }
      flowCoverSection.setBestConstructorMatch(bestSpy);
    }

    if (bestConstructorMatch == null) {
      return;
    }

    PsiElement underTest = generator.initializeMethodCall(method,
        getVariableNameFrom(bestConstructorMatch),
        firstArgumentMocks.toArray(new PsiElement[0]));

    underTest.putUserData(ApplicationKeys.SECTION_TYPE_KEY, TestSectionLabel.THEN);
    flowCoverSection.setMethodCall(underTest);
    flowCoverSection.getParentContainer().updateRestrictedNames(generator.getRestrictedNames());
  }

  /**
   * TODO replace it with ValueNameResolver
   *
   * @param element
   * @return
   */
  private String getVariableNameFrom(PsiElement element) {
    for (PsiElement child : element.getChildren()) {
      if (child instanceof PsiVariable) {
        PsiIdentifier nameIdentifier = ((PsiVariable) child).getNameIdentifier();
        if (nameIdentifier != null) {
          return nameIdentifier.getText();
        }
      }
    }
    return "underTest";
  }

}
