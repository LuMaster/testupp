package generators.sourcevisitors.java;

import api.ILanguageGenerator;
import api.ISourceClassVisitor;
import api.ITestGenerator;
import application.ApplicationKeys;
import com.intellij.psi.JavaRecursiveElementVisitor;
import com.intellij.psi.PsiAssignmentExpression;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiExpression;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiMethodCallExpression;
import com.intellij.psi.PsiModifier;
import com.intellij.psi.PsiParameter;
import com.intellij.psi.PsiReferenceExpression;
import com.intellij.psi.PsiVariable;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.util.PsiTypesUtil;
import data.structure.FlowCoverContainer;
import data.structure.elements.UnworkableElement;
import generators.TestSectionLabel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import util.ImportHelper;
import util.VariableResolver;

/**
 * TODO ?? rethink approach - replace class visitor with normal processing chosen data eg. fields and methods.
 */
public class JavaClassVisitor extends JavaRecursiveElementVisitor implements ISourceClassVisitor {
    private final ITestGenerator owner;

    private final ILanguageGenerator generator;

    private final MockProcessor processor;

    FlowCoverContainer flowCoverContainer;

    private Set<String> existingNames = new HashSet<>();

    public JavaClassVisitor(ILanguageGenerator instance, ITestGenerator testGenerator) {
        owner = testGenerator;
        generator = instance;
      flowCoverContainer = new FlowCoverContainer(generator.getTestFramework());
      existingNames.addAll(VariableResolver.getRestrictedNamesForVariables());
        generator.updateExistingNames(existingNames);
        processor = new MockProcessor(generator, owner.getConfig());
    }

    /**
     * TODO Consider adding support for builder pattern approach and factory methods !
     *
     * @param psiClass
     */
    @Override
    public void visitSourceClass(PsiClass psiClass) {
        visitClass(psiClass);
        for (PsiMethod constructorMethod : psiClass.getMethods()) {
            if (!constructorMethod.isConstructor() || constructorMethod.hasModifierProperty(PsiModifier.PRIVATE)) {
                continue;
            }
            ConstructorVisitor constructorVisitor = visitConstructor(constructorMethod);
            transformConstructor(constructorVisitor, constructorMethod);
        }
        if (flowCoverContainer.isThereNoConstructor()) {
            PsiElement emptyConstructor;
            PsiClassType type = PsiTypesUtil.getClassType(psiClass);
            if (owner.getConfig().useSpy()) {
                emptyConstructor = generator.initializeSpy(type, new ArrayList<>());
            } else {
                emptyConstructor = generator.initializeEmptyConstructor(type, new String[0]);
            }
            flowCoverContainer.addConstructor(emptyConstructor, new HashMap<>());
        }
    }

    private ConstructorVisitor visitConstructor(PsiMethod constructor) {
        ConstructorVisitor visitor = new ConstructorVisitor(constructor);
        constructor.accept(visitor);
        visitThisOrSuper(visitor);
        return visitor;
    }

    private void visitThisOrSuper(ConstructorVisitor visitor) {
        List<PsiMethod> thisAndSupers = new ArrayList<>(visitor.getThisAndSupers());
        if (thisAndSupers.isEmpty()) {
            return;
        }
        visitor.clearThisAndSupers();
        for (PsiMethod thisAndSuper : thisAndSupers) {
            thisAndSuper.accept(visitor);
        }
        visitThisOrSuper(visitor);
    }

    private void transformConstructor(ConstructorVisitor visitor, PsiMethod method) {
        PsiClass containingClass = method.getContainingClass();
        if (containingClass == null) {
            return;
        }

        Map<PsiParameter, PsiElement> arguments = new LinkedHashMap<>();
        PsiParameter[] parameters = method.getParameterList().getParameters();
        for (PsiParameter parameter : parameters) {
            PsiField psiField = visitor.getBindParametersToFields().get(parameter);
            PsiElement fieldMock = flowCoverContainer.getMock(psiField);
            if (fieldMock == null) {
                fieldMock = initializeUnmockedParameter(parameter);
                if (fieldMock == null) continue;
            }
            arguments.put(parameter, fieldMock);
        }
        PsiClassType classType = PsiTypesUtil.getClassType(containingClass);
        //TODO fix this , from groovy objects we got empty set -> PsiIdentifier not found
        List<PsiElement> collect = arguments.values()
                .stream()
                .map(element -> PsiTreeUtil.getChildOfType(element, PsiVariable.class))
                .filter(Objects::nonNull)
                .map(PsiVariable::getNameIdentifier)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        PsiElement newExpression;
        if (owner.getConfig().useSpy()) {
            newExpression = generator.initializeSpy(classType, collect);
        } else {
            newExpression = generator.createNewExpression(classType, collect);
        }
        if(newExpression == null) return;
        flowCoverContainer.addConstructor(newExpression, arguments);
        bindConstructorToUsedFields(newExpression, visitor);
    }

    @Nullable
    private PsiElement initializeUnmockedParameter(PsiParameter parameter) {
        PsiElement fieldMock;
        fieldMock = processor.createSimpleMock(null, parameter.getType());
        if (fieldMock == null || fieldMock instanceof UnworkableElement) {
            return null;
        }
        fieldMock.putUserData(ApplicationKeys.SECTION_TYPE_KEY, TestSectionLabel.GIVEN);
        for (PsiClass klass : ImportHelper.allImportsForElement(parameter)) {
            flowCoverContainer.addImport(klass);
        }
        return fieldMock;
    }

    /**
     * TODO better finding of usages (deep search)
     *
     * @param expression
     * @param visitor
     */
    private void bindConstructorToUsedFields(PsiElement expression, ConstructorVisitor visitor) {
        Collection<PsiField> values = visitor.getBindParametersToFields().values();
        for (PsiElement originalField : values) {
            flowCoverContainer.bindFieldWithConstructor(expression, originalField);
        }
    }

    @Override
    public void visitField(PsiField field) {
        processor.mockVariable(field, flowCoverContainer::addMock);
        super.visitField(field);
    }

    @Override
    public FlowCoverContainer getFlowCoverContainer() {
        return flowCoverContainer;
    }

    @Override
    public void updateExistingNames(Set<String> existingNames) {
        this.existingNames.addAll(existingNames);
    }

    @Override
    public Set<String> getRestrictedNames() {
        existingNames.addAll(generator.getRestrictedNames());
        return existingNames;
    }

    @Getter
    static
    class ConstructorVisitor extends JavaRecursiveElementVisitor {

        Map<PsiParameter, PsiParameter> bindParametersToArguments = new HashMap<>();
        Map<PsiParameter, PsiField> bindParametersToFields = new HashMap<>();

        List<PsiMethod> thisAndSupers = new ArrayList<>();

        public ConstructorVisitor(PsiMethod constructor) {
            @NotNull PsiParameter[] parameters = constructor.getParameterList().getParameters();
            for (PsiParameter parameter : parameters) {
                bindParametersToArguments.put(parameter, parameter);
            }
        }

        @Override
        public void visitMethod(PsiMethod method) {
            super.visitMethod(method);
        }

        @Override
        public void visitReferenceExpression(PsiReferenceExpression expression) {
            super.visitReferenceExpression(expression);
        }

        /**
         * TODO add reassigned parameter support
         * example :
         * foo(int a) {
         * int b = a;
         * field = b;
         * }
         * TODO parameter can be passed to method and return some value
         * example :
         * foo(int a) {
         * field = divTwo(a);
         * }
         * TODO static methods initializin fields
         *
         * @param expression
         */
        @Override
        public void visitAssignmentExpression(PsiAssignmentExpression expression) {
            PsiExpression lExpression = expression.getLExpression();
            if (lExpression instanceof PsiReferenceExpression) {
                PsiExpression rExpression = expression.getRExpression();
                if (rExpression instanceof PsiReferenceExpression) {
                    PsiReferenceExpression refExpression = (PsiReferenceExpression) rExpression;
                    PsiElement rSideResolved = refExpression.resolve();
                    PsiElement lSideResolved = ((PsiReferenceExpression) lExpression).resolve();
                    if (lSideResolved instanceof PsiField && rSideResolved instanceof PsiParameter) {
                        PsiParameter bindParameter = getBindParameter((PsiParameter) rSideResolved);
                        if (bindParameter != null) {
                            bindParametersToFields.put((PsiParameter) rSideResolved, (PsiField) lSideResolved);
                        }
                    }
                }
            }
            super.visitAssignmentExpression(expression);
        }

        @Override
        public void visitExpression(PsiExpression expression) {
            if (expression instanceof PsiMethodCallExpression) {
                PsiMethod psiMethod = ((PsiMethodCallExpression) expression).resolveMethod();
                if (psiMethod != null && psiMethod.isConstructor()) {

                    @NotNull PsiExpression[] arguments = ((PsiMethodCallExpression) expression).getArgumentList().getExpressions();
                    @NotNull PsiParameter[] parameters = psiMethod.getParameterList().getParameters();

                    for (int i = 0; i < arguments.length; i++) {
                        PsiExpression argument = arguments[i];
                        if (argument instanceof PsiReferenceExpression) {
                            PsiElement resolve = ((PsiReferenceExpression) argument).resolve();
                            if (resolve instanceof PsiParameter) {
                                bindParametersToArguments.put(parameters[i], (PsiParameter) resolve);
                            }
                        }
                    }
                    thisAndSupers.add(psiMethod);

                }
            }
            super.visitExpression(expression);
        }

        private PsiParameter getBindParameter(PsiParameter psiParam) {
            PsiParameter parameter = psiParam;
            while (bindParametersToArguments.get(parameter) != null && !bindParametersToArguments.get(parameter).equals(parameter)) {
                parameter = bindParametersToArguments.get(parameter);
            }
            return parameter;
        }

        public void clearThisAndSupers() {
            thisAndSupers = new ArrayList<>();
        }
    }
}
