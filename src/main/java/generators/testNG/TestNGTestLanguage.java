package generators.testNG;

import api.ITestLanguage;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiDirectory;
import com.intellij.testIntegration.TestFramework;
import lombok.SneakyThrows;

public class TestNGTestLanguage implements ITestLanguage {

  private Project project;
  private TestFramework framework;

  public TestNGTestLanguage(Project project, TestFramework framework) {
    this.project = project;
    this.framework = framework;
  }

  @SneakyThrows
  @Override
  public String getMethodTemplate(String types, String methodName, String arguments, String body,
      PsiDirectory directory) {
//    final FileTemplateDescriptor fileTemplateDescriptor = MethodKind.TEST.getFileTemplateDescriptor(framework);
//    final String templateName = fileTemplateDescriptor.getFileName();
//    final FileTemplate fileTemplate = FileTemplateManager.getInstance(project).getCodeTemplate(templateName);
//    final Properties defaultProperties = FileTemplateManager.getInstance(project).getDefaultProperties();
//    Properties properties = new Properties(defaultProperties);
//
//    final PsiElement psiElement = FileTemplateUtil.createFromTemplate(fileTemplate, templateName, properties, directory);
    return types + "  " + methodName + "  " + "( " + arguments + " )" + "{ " + body + " }";
  }

  @Override
  public String getMethodTemplateTypes() {
    return "public void";
  }

  @Override
  public String getStaticClassInitializer(String cfqName) {
    return null;
  }

  @Override
  public String getNewObjectInitializer(String type, String args) {
    return null;
  }

  @Override
  public String getNewExpression(String type, String arguments) {
    return null;
  }

  @Override
  public String getSpyInitializerWithNewObject(String type, String arguments) {
    return null;
  }

  @Override
  public String getSimpleMockStatement(String type, String object) {
    return null;
  }

  @Override
  public String getSpecificMockStatement(String type, String object) {
    return null;
  }

  @Override
  public String getDeclarationAssignment(String type, String variable, String object) {
    return null;
  }

  @Override
  public String getCollectionInitializer(String collectionType) {
    return null;
  }

  @Override
  public String getVariableDeclarationCallByMethod(String type, String variable, String object,
      String methodCall) {
    return null;
  }

  @Override
  public String getVoidCall(String object, String method) {
    return null;
  }

  @Override
  public String joinMockWithReturn(String mock, String result) {
    return null;
  }

  @Override
  public String getThenReturn() {
    return null;
  }

  @Override
  public String getGivenSectionName() {
    return GIVEN;
  }

  @Override
  public String getAndSectionName() {
    return AND;
  }

  @Override
  public String getWhenSectionName() {
    return WHEN;
  }

  @Override
  public String getThenSectionName() {
    return THEN;
  }

  @Override
  public String getWhereSectionName() {
    return WHERE;
  }

  @Override
  public String getExpectSectionName() {
    return EXPECT;
  }

  public static final String GIVEN = "//given ";

  public static final String AND = "//and";

  public static final String WHEN = "//when ";

  public static final String THEN = "//then";

  public static final String EXPECT = "//expect ";

  public static final String WHERE = "//where ";
}
