package generators.testNG;

import api.ILanguageGenerator;
import api.ITestLanguage;
import com.intellij.openapi.project.Project;
import com.intellij.psi.JVMElementFactory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementFactory;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiModifierList;
import com.intellij.psi.PsiType;
import com.intellij.psi.PsiVariable;
import com.intellij.testIntegration.TestFramework;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TestNGLanguageGenerator implements ILanguageGenerator {

  private final PsiElementFactory elementFactory;

  private Set<String> restrictedNames = new HashSet<>();

  private final TestFramework language;

  private final ITestLanguage dictionary;

  public TestNGLanguageGenerator(Project project, TestFramework language,
      ITestLanguage dictionary) {
    this.language = language;
    this.dictionary = dictionary;
    elementFactory = PsiElementFactory.getInstance(project);
  }

  @Override
  public JVMElementFactory getJVMElementFactory() {
    return elementFactory;
  }

  @Override
  public PsiMethod addAnnotations(PsiMethod method) {
    PsiModifierList modifierList = method.getModifierList();
    modifierList.addAnnotation("org.testng.annotations.Test");
    return method;
  }

  @Override
  public PsiElement staticMethodCall(String className, String methodName) {
    return null;
  }

  @Override
  public PsiElement staticClassDeclaration(PsiType type) {
    return null;
  }

  @Override
  public PsiElement staticClass(PsiType type) {
    return null;
  }

  @Override
  public PsiElement createStatementFromPureText(String text) {
    return null;
  }

  @Override
  public PsiElement initializeSpy(PsiType type, List<PsiElement> arguments) {
    return null;
  }

  @Override
  public PsiElement initializeEmptyConstructor(PsiType type, String[] modifiers) {
    return null;
  }

  @Override
  public PsiElement initializeMock(PsiVariable varName) {
    return null;
  }

  @Override
  public PsiElement initializeMock(PsiType type) {
    return null;
  }

  @Override
  public PsiElement initializeMockOfFinal(PsiVariable varName) {
    return null;
  }

  @Override
  public PsiElement initializeMockOfFinal(PsiType type) {
    return null;
  }

  @Override
  public PsiElement initializeSimpleValue(PsiType type, PsiVariable variable, String defaultValue) {
    return null;
  }

  @Override
  public PsiElement initializeCollectionType(PsiVariable variable) {
    return null;
  }

  @Override
  public PsiElement initializeSimpleValue(PsiType type, String defaultValue) {
    return null;
  }

  @Override
  public PsiElement initializeMethodCall(PsiMethod methodToCall, String variableName,
      PsiElement... arguments) {
    return null;
  }

  @Override
  public PsiElement createMockOutputCall(String initializer, String returnValue) {
    return null;
  }

  @Override
  public PsiElement createNewExpression(PsiType type, List<PsiElement> arguments) {
    return null;
  }

  @Override
  public PsiElement createAnyTypeArgument(PsiType type) {
    return null;
  }

  @Override
  public PsiElement createAnyTypeArgument() {
    return null;
  }

  @Override
  public PsiElement createSingleAnyNonNullArgument() {
    return null;
  }

  @Override
  public void updateExistingNames(Collection<String> collection) {

  }

  @Override
  public Collection<String> getRestrictedNames() {
    return restrictedNames;
  }

  @Override
  public TestFramework getTestFramework() {
    return language;
  }
}
