package generators.testNG;

import api.ILanguageGenerator;
import api.ISourceClassVisitor;
import api.ISourceMethodVisitor;
import api.ITestConfiguration;
import api.ITestGenerator;
import application.settings.AppSettingsState;
import com.intellij.codeInsight.CodeInsightUtil;
import com.intellij.openapi.application.WriteAction;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.ex.IdeDocumentHistory;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.util.Computable;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMember;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.codeStyle.CodeStyleManager;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import com.intellij.psi.impl.source.PostprocessReformattingAspect;
import com.intellij.refactoring.util.classMembers.MemberInfo;
import com.intellij.testIntegration.TestFramework;
import com.intellij.util.IncorrectOperationException;
import data.providers.MockGeneratorProvider;
import data.providers.SourceClassVisitorProvider;
import data.structure.FlowCoverContainer;
import dialogs.CreateTestsDialog;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.jetbrains.annotations.Nullable;
import util.SettingsUtil;

public class TestNGTestGenerator implements ITestGenerator {

  private final PsiClass srcClass;
  private final TestFramework framework;
  private final ITestConfiguration config;
  private HashSet<String> existingNames;
  private final FlowCoverContainer flowCoverContainer;


  private ILanguageGenerator languageGenerator;
  private ISourceClassVisitor sourceClassVisitor;

  public TestNGTestGenerator(PsiClass srcClass, TestFramework framework, ITestConfiguration configuration){

    this.srcClass = srcClass;
    this.framework = framework;
    this.config = configuration;

    languageGenerator = MockGeneratorProvider.getInstance(getSourceClass().getProject(), getTestFramework());
    sourceClassVisitor = SourceClassVisitorProvider.getInstance(this, getSourceClass(), languageGenerator);

    flowCoverContainer = initFlowCoverContainer();

    if(sourceClassVisitor != null) {
      existingNames = new HashSet<>(sourceClassVisitor.getRestrictedNames());
    }
  }

  @Override
  public @Nullable PsiElement generateTest(Project project, CreateTestsDialog d) {
    if (getSourceClassVisitor() == null) {
      Messages.showInfoMessage(project, "Unsupported source language in this version.\nThis version of plugin supports only Java language.\nSupport me on patreon to help me develop better plugin for creating tests", "Unsupported Source Language");
      return null;
    }
    String superClassName = ServiceManager.getService(AppSettingsState.class).superClassName;
    TestFramework testFramework = SettingsUtil
        .getTestFramework(ServiceManager.getService(AppSettingsState.class).framework);

    return WriteAction.compute(() -> {
      final PsiClass test = (PsiClass) PostprocessReformattingAspect.getInstance(project)
          .postponeFormattingInside((Computable<PsiElement>) () -> {
            try {
              IdeDocumentHistory.getInstance(project).includeCurrentPlaceAsChangePlace();
              PsiClass targetClass = createTestClass(d, testFramework, project,
                  superClassName);
              if (targetClass == null) {
                return null;
              }
              Editor editor = CodeInsightUtil
                  .positionCursorAtLBrace(project, targetClass.getContainingFile(), targetClass);
              addTestMethods(editor, targetClass, d.getSelectedTestFrameworkDescriptor(),
                  d.getSelectedMethods());
              return targetClass;
            } catch (IncorrectOperationException e1) {
              showErrorLater(project, d.getClassName());
              throw e1;
            }
          });
      if (test == null) {
        return null;
      }
      JavaCodeStyleManager.getInstance(test.getProject()).shortenClassReferences(test);
      CodeStyleManager.getInstance(project).reformat(test);
      return test;
    });
  }

  @Override
  public void addTestMethods(Editor editor, PsiClass targetClass, TestFramework descriptor,
      Collection<MemberInfo> methods) {
    if (getSourceClassVisitor() == null) {
      Messages.showInfoMessage(editor.getProject(),
          "Unsupported source language in this version.\nThis version of plugin supports only Java language.\nSupport me on patreon to help me develop better plugin for creating tests",
          "Unsupported Source Language");
      return;
    }
    for (MemberInfo method : methods) {
      createMethod(method);
    }
    addToDocument(flowCoverContainer, languageGenerator, editor, targetClass);
  }

  public void createMethod(@Nullable MemberInfo info) {
    if (info == null) {
      return;
    }
    PsiMember infoMember = info.getMember();
    if (!(infoMember instanceof PsiMethod)) {
      return;
    }

    PsiMethod infoMethod = (PsiMethod) infoMember;
    PsiClass containingClass = infoMethod.getContainingClass();

    if (containingClass == null) {
      return;
    }

    ISourceMethodVisitor languageVisitor = getSourceMethodVisitor();
    languageVisitor.collect(infoMethod);
  }

  @Override
  public PsiClass getSourceClass() {
    return srcClass;
  }

  @Override
  public ILanguageGenerator getLanguageGenerator() {
    return languageGenerator;
  }

  @Override
  public ISourceClassVisitor getSourceClassVisitor() {
    return sourceClassVisitor;
  }

  @Override
  public ITestConfiguration getConfig() {
    return config;
  }

  @Override
  public Set<String> getExistingNames() {
    return existingNames;
  }

  @Override
  public TestFramework getTestFramework() {
    return framework;
  }

  @Override
  public FlowCoverContainer getFlowCoverContainer() {
    return flowCoverContainer;
  }
}
