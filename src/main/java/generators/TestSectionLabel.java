package generators;

import api.ITestLanguage;
import com.intellij.openapi.project.Project;
import com.intellij.testIntegration.TestFramework;
import data.providers.TestFrameworkLanguageProvider;

public enum TestSectionLabel {

    GIVEN(),

    AND(),

    WHEN(),

    THEN(),

    WHERE(),

    EXPECT();

    TestSectionLabel() {

    }

    public String getLabel(Project project, TestFramework language) {
        String name = this.name();
        ITestLanguage instance = TestFrameworkLanguageProvider.getInstance(project, language);
        if (instance != null) {
            return instance.getLabelBasedOnName(name);
        }
        return null;
    }
}
