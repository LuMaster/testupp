package generators.spock;

import com.intellij.codeInsight.template.Expression;
import com.intellij.codeInsight.template.Template;
import com.intellij.codeInsight.template.TemplateEditingAdapter;
import com.intellij.codeInsight.template.TemplateManager;
import com.intellij.codeInsight.template.impl.ConstantNode;
import com.intellij.ide.fileTemplates.FileTemplate;
import com.intellij.ide.fileTemplates.FileTemplateDescriptor;
import com.intellij.ide.fileTemplates.FileTemplateManager;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiMember;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiType;
import com.intellij.refactoring.util.classMembers.MemberInfo;
import com.intellij.testIntegration.TestFramework;
import com.intellij.testIntegration.TestIntegrationUtils;
import generators.spock.generators.SpockLanguageGenerator;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.plugins.groovy.lang.psi.GroovyPsiElementFactory;

public class Archive {

    private SpockLanguageGenerator _mockGenerator;
    private String name;
    private PsiClass _src;

    public void generateMethod(@NotNull TestIntegrationUtils.MethodKind methodKind,
                               TestFramework descriptor,
                               PsiClass targetClass,
                               Editor editor,
                               @Nullable MemberInfo info, Set<String> existingNames) {
        GroovyPsiElementFactory groovyPsiElementFactory = (GroovyPsiElementFactory) _mockGenerator.getJVMElementFactory();
        PsiMethod method = (PsiMethod) targetClass.add(groovyPsiElementFactory.createMethod("dummy", PsiType.VOID));
        PsiDocumentManager.getInstance(targetClass.getProject()).doPostponedOperationsAndUnblockDocument(editor.getDocument());
        if (info == null) {
            return;
        }
        PsiMember infoMember = info.getMember();
        if (infoMember instanceof PsiMethod) {
        }
        final int startOffset = method.getModifierList().getTextRange().getStartOffset();
        final TextRange range = new TextRange(startOffset, method.getTextRange().getEndOffset());
        editor.getDocument().replaceString(range.getStartOffset(), range.getEndOffset(), "");
        editor.getCaretModel().moveToOffset(range.getStartOffset());
        org.jetbrains.plugins.groovy.lang.psi.api.statements.typedef.members.GrMethod methodFromText = groovyPsiElementFactory.createMethodFromText("def \"add\" () { }");
        PsiDocumentManager.getInstance(targetClass.getProject()).commitDocument(editor.getDocument());
        boolean automatic = true;
        Template testMethodTemplate = createTestMethodTemplate(methodKind, descriptor, targetClass, _src, name, automatic, existingNames);
        final Project project = targetClass.getProject();
        TemplateEditingAdapter adapter = null;
        TemplateManager.getInstance(project).startTemplate(editor, testMethodTemplate, adapter);

    }

    public static Template createTestMethodTemplate(@NotNull TestIntegrationUtils.MethodKind methodKind,
                                                    TestFramework descriptor,
                                                    @NotNull PsiClass targetClass,
                                                    @Nullable PsiClass sourceClass,
                                                    @Nullable String name,
                                                    boolean automatic,
                                                    Set<? super String> existingNames) {
        FileTemplateDescriptor templateDesc = methodKind.getFileTemplateDescriptor(descriptor);
        String templateName = templateDesc.getFileName();
        FileTemplate fileTemplate = FileTemplateManager.getInstance(targetClass.getProject()).getCodeTemplate(templateName);
        Template template = TemplateManager.getInstance(targetClass.getProject()).createTemplate("", "");

        String templateText;
        try {
            Properties properties = new Properties();
            if (sourceClass != null && sourceClass.isValid()) {
                properties.setProperty(FileTemplate.ATTRIBUTE_CLASS_NAME, sourceClass.getQualifiedName());
            }

            templateText = fileTemplate.getText(properties);
        } catch (IOException e) {
            templateText = fileTemplate.getText();
        }

        if (name == null) name = methodKind.getDefaultName();

        if (existingNames != null && !existingNames.add(name)) {
            int idx = 1;
            while (existingNames.contains(name)) {
                final String newName = name + (idx++);
                if (existingNames.add(newName)) {
                    name = newName;
                    break;
                }
            }
        }

        templateText = StringUtil.replace(templateText, "${BODY}\n", "");

        int from = 0;
        while (true) {
            int index = templateText.indexOf("${NAME}", from);
            if (index == -1) break;

            template.addTextSegment(templateText.substring(from, index));

            if (index > 0 && !Character.isWhitespace(templateText.charAt(index - 1))) {
                name = StringUtil.capitalize(name);
            } else {
                name = StringUtil.decapitalize(name);
            }
            if (from == 0) {
                Expression nameExpr = new ConstantNode(name);
                template.addVariable("name", nameExpr, nameExpr, !automatic);
            } else {
                template.addVariableSegment("name");
            }

            from = index + "${NAME}".length();
        }
        template.addTextSegment(templateText.substring(from));

        template.setToIndent(true);
        template.setToReformat(true);
        template.setToShortenLongNames(true);

        return template;
    }
}
