package generators.spock;

import api.ILanguageGenerator;
import api.ISourceClassVisitor;
import api.ISourceMethodVisitor;
import api.ITestConfiguration;
import api.ITestGenerator;
import application.settings.AppSettingsState;
import com.intellij.codeInsight.CodeInsightUtil;
import com.intellij.codeInsight.actions.OptimizeImportsProcessor;
import com.intellij.openapi.application.WriteAction;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.ex.IdeDocumentHistory;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.util.Computable;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiMember;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.codeStyle.CodeStyleManager;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import com.intellij.psi.impl.source.PostprocessReformattingAspect;
import com.intellij.refactoring.util.classMembers.MemberInfo;
import com.intellij.testIntegration.TestFramework;
import com.intellij.util.IncorrectOperationException;
import data.providers.MockGeneratorProvider;
import data.providers.SourceClassVisitorProvider;
import data.structure.FlowCoverContainer;
import data.structure.FlowCoverSection;
import data.structure.tests.SpockTest;
import dialogs.CreateTestsDialog;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.jetbrains.annotations.Nullable;
import util.SettingsUtil;

public class SpockTestGenerator implements ITestGenerator {

  private final ISourceClassVisitor sourceClassVisitor;

  private final TestFramework framework;

  private final PsiClass srcKlass;

  private final FlowCoverContainer flowCoverContainer;

  private HashSet<String> existingNames;

  private final ITestConfiguration testConfiguration;

  private final ILanguageGenerator mockGenerator;

  public SpockTestGenerator(PsiClass srcClass, TestFramework framework,
      ITestConfiguration configuration) {

    this.srcKlass = srcClass;
    this.testConfiguration = configuration;
    this.framework = framework;

    mockGenerator = MockGeneratorProvider
        .getInstance(getSourceClass().getProject(), getTestFramework());
    sourceClassVisitor = SourceClassVisitorProvider
        .getInstance(this, getSourceClass(), mockGenerator);

    flowCoverContainer = initFlowCoverContainer();

    if (sourceClassVisitor != null) {
      existingNames = new HashSet<>(sourceClassVisitor.getRestrictedNames());
    }
  }

  @Override
  public ITestConfiguration getConfig() {
    return testConfiguration;
  }

  @Override
  public Set<String> getExistingNames() {
    return existingNames;
  }

  @Override
  public TestFramework getTestFramework() {
    return framework;
  }

  public void createMethod(@Nullable MemberInfo info) {
    if (info == null) {
      return;
    }
    PsiMember infoMember = info.getMember();
    if (!(infoMember instanceof PsiMethod)) {
      return;
    }

    PsiMethod infoMethod = (PsiMethod) infoMember;
    PsiClass containingClass = infoMethod.getContainingClass();

    if (containingClass == null) {
      return;
    }

    ISourceMethodVisitor languageVisitor = getSourceMethodVisitor();
    languageVisitor.collect(infoMethod);
  }


  public void addTestMethods(Editor editor, PsiClass targetClass, TestFramework descriptor,
      Collection<MemberInfo> methods)
      throws IncorrectOperationException {
    if (sourceClassVisitor == null) {
      Messages.showInfoMessage(editor.getProject(),
          "Unsupported source language in this version.\nThis version of plugin supports only Java language.\nSupport me on patreon to help me develop better plugin for creating tests",
          "Unsupported Source Language");
      return;
    }
    for (MemberInfo method : methods) {
      createMethod(method);
    }
    addToDocument(editor, targetClass);
  }

  @Override
  public PsiClass getSourceClass() {
    return srcKlass;
  }

  @Override
  public ILanguageGenerator getLanguageGenerator() {
    return mockGenerator;
  }

  @Override
  public ISourceClassVisitor getSourceClassVisitor() {
    return sourceClassVisitor;
  }

  private void addToDocument(Editor editor, PsiClass targetClass) {
    Project project = targetClass.getProject();
    PsiDocumentManager.getInstance(project).commitDocument(editor.getDocument());
    List<FlowCoverSection> flowCoverSections = flowCoverContainer.getFlowCoverSections();
    PsiDirectory directory = PsiManager.getInstance(project)
        .findDirectory(targetClass.getContainingFile().getVirtualFile());
    for (FlowCoverSection flowCoverSection : flowCoverSections) {
      SpockTest spockTest = flowCoverSection.convertToTest();
      PsiMethod methodFromText = mockGenerator.getJVMElementFactory()
          .createMethodFromText(spockTest.createTestFromText(directory), targetClass);
      targetClass.add(methodFromText);
    }
    for (PsiClass aClass : flowCoverContainer.getClassesToImport().stream().filter(Objects::nonNull)
        .collect(Collectors.toSet())) {
      addImport(aClass, targetClass);
    }
    OptimizeImportsProcessor processor = new OptimizeImportsProcessor(project,
        targetClass.getContainingFile());
    processor.runWithoutProgress();
  }

  @Nullable
  @Override
  public PsiElement generateTest(Project project, CreateTestsDialog d) {
    if (sourceClassVisitor == null) {
      Messages.showInfoMessage(project,
          "Unsupported source language in this version.\nThis version of plugin supports only Java language.\nSupport me on patreon to help me develop better plugin for creating tests",
          "Unsupported Source Language");
      return null;
    }
    String superClassName = ServiceManager.getService(AppSettingsState.class).superClassName;
    TestFramework testFramework = SettingsUtil
        .getTestFramework(ServiceManager.getService(AppSettingsState.class).framework);

    return WriteAction.compute(() -> {
      final PsiClass test = (PsiClass) PostprocessReformattingAspect.getInstance(project)
          .postponeFormattingInside((Computable<PsiElement>) () -> {
            try {
              IdeDocumentHistory.getInstance(project).includeCurrentPlaceAsChangePlace();
              PsiClass targetClass = createTestClass(d, testFramework, project,
                  superClassName);
              if (targetClass == null) {
                return null;
              }
              Editor editor = CodeInsightUtil
                  .positionCursorAtLBrace(project, targetClass.getContainingFile(), targetClass);
              addTestMethods(editor, targetClass, d.getSelectedTestFrameworkDescriptor(),
                  d.getSelectedMethods());
              return targetClass;
            } catch (IncorrectOperationException e1) {
              showErrorLater(project, d.getClassName());
              throw e1;
            }
          });
      if (test == null) {
        return null;
      }
      JavaCodeStyleManager.getInstance(test.getProject()).shortenClassReferences(test);
      CodeStyleManager.getInstance(project).reformat(test);
      return test;
    });
  }

  public FlowCoverContainer getFlowCoverContainer() {
    return flowCoverContainer;
  }


  @Override
  public String toString() {
    return "Create Test in Spock";
  }

}
