package generators.spock.generators;

import api.ILanguageGenerator;
import api.ITestLanguage;
import com.intellij.openapi.project.Project;
import com.intellij.psi.JVMElementFactory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiIdentifier;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiType;
import com.intellij.psi.PsiVariable;
import com.intellij.psi.codeStyle.SuggestedNameInfo;
import com.intellij.refactoring.rename.NameSuggestionProvider;
import com.intellij.testIntegration.TestFramework;
import generators.spock.SpockTestLanguage;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;
import org.jetbrains.plugins.groovy.lang.psi.GroovyPsiElementFactory;
import org.jetbrains.plugins.groovy.lang.psi.api.statements.GrStatement;
import org.jetbrains.plugins.groovy.lang.psi.api.statements.GrVariableDeclaration;
import org.jetbrains.plugins.groovy.lang.psi.api.statements.expressions.GrExpression;
import org.jetbrains.plugins.groovy.refactoring.DefaultGroovyVariableNameValidator;
import org.jetbrains.plugins.groovy.refactoring.GroovyNameSuggestionUtil;
import util.VariableNameResolver;

/**
 * TODO refactor / replace all missing places with VariableNameResolver / see NamesUtil from
 * intellij This class should contain only methods that generate particular constructions(eg.
 * Mocks)
 */
public class SpockLanguageGenerator implements ILanguageGenerator {

  private final GroovyPsiElementFactory elementFactory;

  private Set<String> restrictedNames = new HashSet<>();

  private final TestFramework language;

  private final ITestLanguage dictionary;

  public SpockLanguageGenerator(Project project, TestFramework language, ITestLanguage dictionary) {
    elementFactory = GroovyPsiElementFactory.getInstance(project);
    this.language = language;
    this.dictionary = dictionary;
  }

  public void updateExistingNames(Collection<String> collection) {
    restrictedNames.addAll(collection);
  }

  @Override
  public Collection<String> getRestrictedNames() {
    return restrictedNames;
  }

  public void updateExistingNames(String name) {
    restrictedNames.add(name);
  }

  @Override
  public JVMElementFactory getJVMElementFactory() {
    return elementFactory;
  }

  @Override
  public PsiMethod addAnnotations(PsiMethod method) {
    return method;
  }

  @Override
  public PsiElement staticMethodCall(String className, String methodName) {
    return elementFactory.createExpressionFromText(className + "." + methodName);
  }

  @Override
  public PsiElement createStatementFromPureText(String text) {
    return elementFactory.createStatementFromText(text);
  }

  @Override
  public PsiElement initializeSpy(PsiType type, List<PsiElement> arguments) {
    String args = convertRawArgsIntoParams(arguments.toArray(new PsiElement[0]));
    GrExpression expressionFromText = elementFactory.createExpressionFromText(
        dictionary.getSpyInitializerWithNewObject(type.getPresentableText(), args));
    String varName = VariableNameResolver.resolveTypeName(type, restrictedNames);
    return elementFactory
        .createVariableDeclaration(new String[0], expressionFromText, type, varName);
  }

  public PsiElement staticClassDeclaration(PsiType type) {
    GrExpression expressionFromText = elementFactory.createExpressionFromText(
        dictionary.getStaticClassInitializer(type.getPresentableText()));
    String varName = VariableNameResolver.resolveTypeName(type, restrictedNames);
    return elementFactory
        .createVariableDeclaration(new String[0], expressionFromText, type, varName);
  }

  public PsiElement staticClass(PsiType type) {
    return elementFactory.createExpressionFromText(
        dictionary.getStaticClassInitializer(type.getPresentableText()));
  }

  @Override
  public TestFramework getTestFramework() {
    return language;
  }

  @Override
  public GrVariableDeclaration initializeEmptyConstructor(PsiType type, String[] modifiers) {
    GrExpression emptyNewExpression = createEmptyNewExpression(type);
    String resolveTypeName = VariableNameResolver.resolveTypeName(type, restrictedNames);
    return elementFactory
        .createVariableDeclaration(modifiers, emptyNewExpression, type, resolveTypeName);
  }

  public GrStatement initializeMock(PsiVariable variable) {
    String varName = VariableNameResolver.resolveVariableName(variable, restrictedNames);
    return elementFactory.createStatementFromText(
        dictionary.getSimpleMockStatement(variable.getType().getPresentableText(), varName));
  }

  public GrStatement initializeMock(PsiType type) {
    String varName = VariableNameResolver.resolveTypeName(type, restrictedNames);
    return elementFactory.createStatementFromText(
        dictionary.getSimpleMockStatement(type.getPresentableText(), varName));
  }

  @Override
  public PsiElement initializeMockOfFinal(PsiVariable variable) {
    String varName = VariableNameResolver.resolveVariableName(variable, restrictedNames);
    return elementFactory.createStatementFromText(
        dictionary.getSpecificMockStatement(variable.getType().getPresentableText(), varName));
  }

  @Override
  public PsiElement initializeMockOfFinal(PsiType type) {
    String varName = VariableNameResolver.resolveTypeName(type, restrictedNames);
    return elementFactory.createStatementFromText(
        dictionary.getSpecificMockStatement(type.getPresentableText(), varName));
  }

  public GrStatement initializeSimpleValue(PsiType type, PsiVariable variable,
      String defaultValue) {
    String varName = VariableNameResolver.resolveVariableName(variable, restrictedNames);
    return elementFactory.createStatementFromText(dictionary.getDeclarationAssignment(
        type.getPresentableText(),
        varName,
        defaultValue));
  }

  public GrStatement initializeSimpleValue(PsiType type, String defaultValue) {
    String varName = VariableNameResolver.resolveTypeName(type, restrictedNames);
    return elementFactory.createStatementFromText(dictionary.getDeclarationAssignment(
        type.getPresentableText(),
        varName,
        defaultValue));
  }

  public PsiElement createMockOutputCall(String initializer, String returnValue) {
    return elementFactory
        .createStatementFromText(dictionary.joinMockWithReturn(initializer, returnValue));
  }

  private org.jetbrains.plugins.groovy.lang.psi.api.statements.expressions.GrExpression createEmptyNewExpression(
      PsiType type) {
    return elementFactory.createExpressionFromText(dictionary.getNewExpression(
        type.getPresentableText(),
        SpockTestLanguage.PARENTHESIS));
  }

  public GrVariableDeclaration createNewExpression(PsiType type, List<PsiElement> arguments) {
    String args = convertRawArgsIntoParams(arguments.toArray(new PsiElement[0]));
    GrExpression expressionFromText = elementFactory.createExpressionFromText(
        dictionary.getNewObjectInitializer(type.getPresentableText(), args));
    String[] strings = GroovyNameSuggestionUtil
        .suggestVariableNameByType(expressionFromText.getType(),
            new DefaultGroovyVariableNameValidator(expressionFromText,
                restrictedNames));
    //TODO change for VariableNameResolver
    updateExistingNames(strings[0]);
    return elementFactory
        .createVariableDeclaration(new String[0], expressionFromText, type, strings[0]);
  }

  @Override
  public PsiElement initializeCollectionType(PsiVariable variable) {
    PsiType type = variable.getType();
    String varName = VariableNameResolver.resolveTypeName(type, restrictedNames);
    return elementFactory.createStatementFromText(dictionary.getDeclarationAssignment(
        type.getPresentableText(),
        varName,
        dictionary.getCollectionInitializer(type.getCanonicalText())));
  }

  @Override
  public PsiElement createAnyTypeArgument() {
    return elementFactory.createExpressionFromText(SpockTestLanguage.ANY);
  }

  @Override
  public PsiElement createSingleAnyNonNullArgument() {
    return elementFactory.createExpressionFromText(SpockTestLanguage.ANY_NON_NULL_ARGUMENT);
  }

  @Override
  public PsiElement createAnyTypeArgument(PsiType type) {
    String presentableText = type.getPresentableText();
    return elementFactory
        .createExpressionFromText(MessageFormat.format(SpockTestLanguage.ANY_AS, presentableText));
  }

  public PsiElement initializeMethodCall(PsiMethod methodToCall, String variableName,
      PsiElement... arguments) {
    PsiType type = methodToCall.getReturnType();
    String name = methodToCall.getName();
    int parameterSize = methodToCall.getParameterList().getParameters().length;
    if (arguments.length != parameterSize) {
      return null;
    }
    StringBuilder join = new StringBuilder();
    String methodCallText = name + "(";

    String part = "";
    for (PsiElement argument : arguments) {
      for (PsiElement child : argument.getChildren()) {
        if (child instanceof PsiVariable) {
          PsiIdentifier nameIdentifier = ((PsiVariable) child).getNameIdentifier();
          if (nameIdentifier != null) {
            String text = nameIdentifier.getText();
            part = joinNotEmpty(",", part, text);
          }
        }
      }
    }

    join.append(part);
    methodCallText += join.toString();
    methodCallText += ")";

    if (type == null || type.equals(PsiType.VOID)) {
      // Return void method call
      return elementFactory
          .createStatementFromText(dictionary.getVoidCall(variableName, methodCallText));
    }

    String typeText = VariableNameResolver.resolveTypeName(type, restrictedNames);

    String varname = typeText + name;
    SuggestedNameInfo suggestedNames = null;
    for (NameSuggestionProvider nameSuggestionProvider : NameSuggestionProvider.EP_NAME
        .getExtensions()) {
      suggestedNames = nameSuggestionProvider
          .getSuggestedNames(methodToCall, methodToCall.getContainingClass(), restrictedNames);
    }
    if (suggestedNames != null) {
      varname = suggestedNames.names[0];
    }
    updateExistingNames(varname);
    return elementFactory.createStatementFromText(dictionary
        .getVariableDeclarationCallByMethod(type.getPresentableText(), varname, variableName,
            methodCallText));
  }

  private static String convertRawArgsIntoParams(PsiElement[] arguments) {
    String part = "";
    for (PsiElement argument : arguments) {
      String text = argument.getText();
      part = joinNotEmpty(",", part, text);
    }
    return part;
  }

  public static String joinNotEmpty(CharSequence delimiter, CharSequence... elements) {
    Objects.requireNonNull(delimiter);
    Objects.requireNonNull(elements);
    List<String> list = new ArrayList<>();
    for (CharSequence element : elements) {
      list.add(String.valueOf(element));
    }
    list.removeAll(Collections.singletonList(""));
    // Number of elements not likely worth Arrays.stream overhead.
    StringJoiner joiner = new StringJoiner(delimiter);
    for (CharSequence cs : list) {
      joiner.add(cs);
    }
    return joiner.toString();
  }
}
