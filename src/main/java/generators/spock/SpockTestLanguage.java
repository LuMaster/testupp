package generators.spock;

import api.ITestLanguage;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiDirectory;
import com.intellij.testIntegration.TestFramework;

public class SpockTestLanguage implements ITestLanguage {

  public static final String PARENTHESIS = "()";

  public static final String ANY = "_";

  public static final String ANY_AS = "_ as {0}";

  public static final String ANY_NON_NULL_ARGUMENT = "!null";

  private final Project project;

  public SpockTestLanguage(Project project, TestFramework framework) {

    this.project = project;
  }

  @Override
  public String getMethodTemplate(String types, String methodName, String arguments, String body,
      PsiDirectory directory) {
    return types + " \" " + methodName + " \" " + "( " + arguments + " )" + "{ " + body + " }";
  }

  @Override
  public String getMethodTemplateTypes() {
    return "def";
  }

  @Override
  public String getStaticClassInitializer(String cfqName) {
    return "GroovySpy(" + cfqName + ", global: true)";
  }

  @Override
  public String getNewObjectInitializer(String type, String args) {
    return "new " + type + "(" + args + ")";
  }

  @Override
  public String getNewExpression(String type, String arguments) {
    return "new " + type + arguments;
  }

  @Override
  public String getSpyInitializerWithNewObject(String type, String arguments) {
    String initializer = type + "(" + arguments + ")";
    return "Spy(new " + initializer + ")";
  }

  @Override
  public String getSimpleMockStatement(String type, String object) {
    return type + " " + object + " = Mock()";
  }

  @Override
  public String getSpecificMockStatement(String type, String object) {
    return type + " " + object + " = GroovyMock()";
  }

  @Override
  public String getDeclarationAssignment(String type, String variable, String object) {
    return type + " " + variable + " = " + object;
  }

  @Override
  public String getCollectionInitializer(String collectionType) {
    return "new " + collectionType + "()";
  }

  @Override
  public String getVariableDeclarationCallByMethod(String type, String variable, String object,
      String methodCall) {
    return type + " " + variable + " = " + object + "." + methodCall;
  }

  @Override
  public String getVoidCall(String object, String method) {
    return object + "." + method;
  }

  public String joinMockWithReturn(String mock, String result) {
    return mock + getThenReturn() + result;
  }

  public String getThenReturn() {
    return " >> ";
  }

  @Override
  public String getGivenSectionName() {
    return GIVEN;
  }

  @Override
  public String getAndSectionName() {
    return AND;
  }

  @Override
  public String getWhenSectionName() {
    return WHEN;
  }

  @Override
  public String getThenSectionName() {
    return THEN;
  }

  @Override
  public String getWhereSectionName() {
    return WHERE;
  }

  @Override
  public String getExpectSectionName() {
    return EXPECT;
  }

  public static final String GIVEN = "given: ";

  public static final String AND = "and: ";

  public static final String WHEN = "when: ";

  public static final String THEN = "then: ";

  public static final String EXPECT = "expect: ";

  public static final String WHERE = "where: ";
}
